#version 120

in vec3 pos;
in vec3 normal;
in vec2 uv;
in vec4 ambient;
in vec4 diffuse;
in vec4 specular;
in vec4 emission;
in float shine;

uniform mat4 projMat;
uniform mat4 viewMat;
uniform vec3 camera;
uniform vec4 ambientLight;
uniform vec3 pLightPos[100];
uniform vec4 pLightColor[100];
uniform float pLightLen[100];
uniform int pLights;


varying vec3 pv;
varying vec3 nv;
varying vec2 tv;
varying vec4 av;
varying vec4 dv;
varying vec4 sv;
varying vec4 ev;
varying float sh;

void main() {
    gl_Position = projMat * viewMat * vec4(pos, 1.0);

    pv = pos;
    nv = normal;
    tv = uv;
    av = ambient;
    dv = diffuse;
    sv = specular;
    ev = emission;
    sh = shine;
}