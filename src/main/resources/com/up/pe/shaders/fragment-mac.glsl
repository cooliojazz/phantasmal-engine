#version 120

uniform vec3 camera;
uniform vec4 ambientlight;
uniform vec3 lightpos[100];
uniform float lightlen[100];
uniform vec4 lightcolor[100];
uniform int lightc;
uniform sampler2D tex;

varying vec3 pv;
varying vec3 nv;
varying vec2 tv;
varying vec4 av;
varying vec4 dv;
varying vec4 sv;
varying float sh;

float computePixel(float x, float y) {
    float tx = 0.0;
    float ty = 0.0;
    float lx = 0.0;
    float ly = 0.0;
    int i = 0;
    int maxv = 100;
    while (tx * tx + ty * ty < 2.0 * 2.0 && i < maxv) {
        float xtemp = tx * tx - ty * ty + x;
        float ytemp = 2.0 * tx * ty + y;
        lx = tx;
        ly = ty;
        tx = xtemp;
        ty = ytemp;
        i++;
    }
    if (i == maxv || tx * tx + ty * ty - lx * lx - ly * ly == 0) {
        return min(max(0, min(i, maxv) / maxv), 1);
    }

    float temp = 1 - (4.0 - lx * lx - ly * ly) / (tx * tx + ty * ty - lx * lx - ly * ly) ;
    return min(max(0, min(i - temp, maxv) / maxv), 1);
}

void main() {
    //float x = tv.x * 3.5 - 2.5;
    //float y = tv.y * 2.0 - 1.0;
    //vec4 mbc = computePixel(x, y) * dv;

    //Ambient
    //gl_FragColor = texture2D(tex, tv) * ambientlight;
    gl_FragColor = av * ambientlight;

    for (int i = 0; i < lightc; i++) {

        vec3 L = normalize(lightpos[i] - pv);
        vec3 V = normalize(camera - pv);
        float dmod = max(0, lightlen[i] - length(lightpos[i] - pv)) / lightlen[i];

        //Diffuse
        float diff = max(0, dot(normalize(nv), L)) * dmod;

        //Specular
        float spec = max(0, pow(dot(normalize(nv), normalize(L + V)), 50)) * dmod * sh;

        //gl_FragColor += diff * texture2D(tex, tv) * lightcolor[i] + spec * sv * lightcolor[i];
        gl_FragColor += diff * dv * lightcolor[i] + spec * sv * lightcolor[i];
    }

    gl_FragColor = clamp(gl_FragColor, 0.0, 1.0);
}