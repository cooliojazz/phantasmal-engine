#version 150

varying vec4 colors;
varying vec2 sx;

in vec4 gl_FragCoord;
out vec4 colorOut;

float computePixel(float x, float y) {
    float tx = 0.0;
    float ty = 0.0;
    float lx = 0.0;
    float ly = 0.0;
    int i = 0;
    int maxv = 1000;
    while (tx * tx + ty * ty < 2.0 * 2.0 && i < maxv) {
        float xtemp = tx * tx - ty * ty + x;
        float ytemp = 2.0 * tx * ty + y;
//        if ((xtemp == tx) && (ytemp == ty)) {
//            ans[cur] = max;
//            break;
//        }
        lx = tx;
        ly = ty;
        tx = xtemp;
        ty = ytemp;
        i++;
    }
    if (i == maxv || tx * tx + ty * ty - lx * lx - ly * ly == 0) {
        return min(max(0, min(i, 1000) / 1000), 1);
    }

    float temp = 1 - (4.0 - lx * lx - ly * ly) / (tx * tx + ty * ty - lx * lx - ly * ly) ;
    return min(max(0, min(i - temp, 1000) / 1000), 1);
}

void main() {
    float x = sx.x * 3.5 - 2.5;
    float y = sx.y * 2.0 - 1.0;
    colorOut = vec4(computePixel(x, y), 0, 0, 1);
}