#version 120

attribute vec3 pos;
attribute vec3 normal;
attribute vec2 tex;
attribute vec4 ambient;
attribute vec4 diffuse;
attribute vec4 specular;
attribute float shine;

uniform mat4 projMat;
uniform mat4 viewMat;
uniform vec3 camera;
uniform vec3 lightpos[100];
uniform float lightlen[100];
uniform vec4 lightcolor[100];
uniform int lightc;


varying vec3 pv;
varying vec3 nv;
varying vec2 tv;
varying vec4 av;
varying vec4 dv;
varying vec4 sv;
varying float sh;

void main() {
    gl_Position = projMat * viewMat * vec4(pos, 1.0);

    pv = pos;
    nv = normal;
    tv = tex;
    av = ambient;
    dv = diffuse;
    sv = specular;
    sh = shine;
}