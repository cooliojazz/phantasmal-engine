#version 120

uniform vec3 camera;
uniform vec4 ambientLight;
uniform vec3 dLightDir[100];
uniform vec4 dLightColor[100];
uniform int dLights;
uniform vec3 pLightPos[100];
uniform vec4 pLightColor[100];
uniform float pLightLen[100];
uniform int pLights;
uniform sampler2D tex;

varying vec3 pv;
varying vec3 nv;
varying vec2 tv;
varying vec4 av;
varying vec4 dv;
varying vec4 sv;
varying vec4 ev;
varying float sh;

out vec4 colorOut;

void main() {
    vec4 tc = texture2D(tex, tv);

    //Ambient
    colorOut = (tc + av) * ambientLight;

	//Direction
    for (int i = 0; i < dLights; i++) {
        vec3 L = -dLightDir[i];
        vec3 V = normalize(camera - pv);
        //float dmod = max(0, dLightLen[i] - length(pLightPos[i] - pv)) / pLightLen[i];

        //Diffuse
        float diff = max(0, dot(normalize(nv), L))/* * dmod*/;

        //Specular
        float spec = max(0, pow(dot(normalize(nv), normalize(L + V)), 50))/* * dmod*/ * sh;

        colorOut += ev + (diff * (tc + dv) + spec * sv) * dLightColor[i];
    }

	//Point
    for (int i = 0; i < pLights; i++) {
        vec3 L = normalize(pLightPos[i] - pv);
        vec3 V = normalize(camera - pv);
        float dmod = max(0, pLightLen[i] - length(pLightPos[i] - pv)) / pLightLen[i];

        //Diffuse
        float diff = max(0, dot(normalize(nv), L)) * dmod;

        //Specular
        float spec = max(0, pow(dot(normalize(nv), normalize(L + V)), 50)) * dmod * sh;

        colorOut += ev + (diff * (tc + dv) + spec * sv) * pLightColor[i];
    }

    colorOut = clamp(colorOut, 0.0, 1.0);
}