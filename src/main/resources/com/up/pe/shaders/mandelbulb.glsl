#version 150

varying vec2 sx;

out vec4 colorOut;

float PI = 3.14159265358979;
float HALF_PI = 0.5 * PI;
int MAXITER = 10;

uniform float scale = 1;
vec3 eye = vec3(0.5, 0.5, -5);
uniform vec3 offset = vec3(-0.78, 0.07, -1);

vec3 ln;
bool inSet(vec3 pos) {
    //vec3 start = pos / scale + vec3(1, 1, -1) * offset;
    vec3 start = pos;
    vec3 t = vec3(0.0, 0.0, 0.0);
    int i = 0;
    int maxv = MAXITER;
    vec3 temp;
    while (length(t) < 4.0 && i < maxv) {
        float r = length(t);
        float yang = atan(length(t.xy), t.z) * 2 + HALF_PI;
        float zang = atan(t.y, t.x) * 2 + PI;
        temp = t;
        t = r * r * vec3(sin(yang) * cos(zang), sin(yang) * sin(zang), cos(yang)) + start;
        i++;
    }
    ln = t - temp;
    return i == maxv;
}

vec3 light = vec3(0, 4, 0);
void main() {
    //vec3 screen = vec3(sx, 0);
    //vec3 step = 0.1 / scale * normalize(screen - eye);
    //vec3 pos = screen;
    //bool et;
    //while (length(pos - screen) < 10 * scale && !(et = inSet(pos))) {
    //    pos += step;
    //}

    

    if (et) {
        //float z = (pos.z + 1) / 2;
        float light = max(0, dot(normalize(normal(pos)), normalize(light - pos)));
        //colorOut = vec4(vec3(1, z, z) * light, 1);
        colorOut = vec4((normalize(ln) + light) * vec3(0.5, 0.5, 0.5), 1);
        return;
    }
}