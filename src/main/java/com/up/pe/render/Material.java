package com.up.pe.render;

import com.up.pe.util.TextUtils;
import java.awt.Color;

/**
 *
 * @author Ricky
 */
public class Material {
    
    public static final float[] green = {0f, 1f, 0f, 1f};
    public static final float[] blue = {0f, 0f, 1f, 1f};
    public static final float[] red = {1f, 0f, 0f, 1f};
    public static final float[] white = {1f, 1f, 1f, 1f};
    public static final float[] black = {0f, 0f, 0f, 1f};
    
    public float[] ambient;
    public float[] specular;
    public float[] emission;
    public float[] diffuse;
    public float shininess;
    public Texture texture;
    
    public Material(float[] main, float shininnes) {
        this(main, white, black, shininnes, null);
    }
    
    public Material(float[] ambient, float[] diffuse, float[] specular, float[] emission, float shininnes, Texture texture) {
        this.ambient = ambient;
        this.diffuse = diffuse;
        this.specular = specular;
        this.emission = emission;
        this.shininess = shininnes;
        this.texture = texture;
    }
    
    public Material(float[] main, float[] specular, float[] emission, float shininnes, Texture texture) {
        this.diffuse = this.ambient = main;
        this.specular = specular;
        this.emission = emission;
        this.shininess = shininnes;
        this.texture = texture;
    }
    
    public Material(Color main, float shininnes) {
        this(main, Color.white, Color.black, shininnes, null);
    }
    
    public Material(Color main, Color specular, Color emission, float shininnes, Texture texture) {
        this.diffuse = this.ambient = main.getRGBComponents(null);
        this.specular = colorToShaderColor(specular);
        this.emission = colorToShaderColor(emission);
        this.shininess = shininnes;
        this.texture = texture;
    }
    
    public Material(Color ambient, Color diffuse, Color specular, Color emission, float shininnes, Texture texture) {
        this.ambient = colorToShaderColor(ambient);
        this.diffuse = colorToShaderColor(diffuse);
        this.specular = colorToShaderColor(specular);
        this.emission = colorToShaderColor(emission);
        this.shininess = shininnes;
        this.texture = texture;
    }
    
    public Material(Texture texture) {
        this.ambient = green;
        this.diffuse = green;
        this.specular = white;
        this.emission = black;
        this.shininess = 63;
        this.texture = texture;
    }
    
    public static String floatToHex(float[] f) {
        return TextUtils.padLeft("0", 2, Integer.toHexString((int)(f[0] * 255))) + "" + TextUtils.padLeft("0", 2, Integer.toHexString((int)(f[1] * 255))) + "" + TextUtils.padLeft("0", 2, Integer.toHexString((int)(f[2] * 255))) + "" + TextUtils.padLeft("0", 2, Integer.toHexString((int)(f[3] * 255)));
    }
    
    public static float[] colorToShaderColor(Color c) {
        float[] fc = new float[4];
        fc[3] = 1;
        c.getColorComponents(fc);
        return fc;
    }
	
	// Should it support multiple modes?
	public Material blend(Material m, float amount) {
		return new Material(
				new float[] {ambient[0] + (m.ambient[0] - ambient[0]) * amount, ambient[1] + (m.ambient[1] - ambient[1]) * amount, ambient[2] + (m.ambient[2] - ambient[2]) * amount, ambient[3] + (m.ambient[3] - ambient[3]) * amount},
				new float[] {diffuse[0] + (m.diffuse[0] - diffuse[0]) * amount, diffuse[1] + (m.diffuse[1] - diffuse[1]) * amount, diffuse[2] + (m.diffuse[2] - diffuse[2]) * amount, diffuse[3] + (m.diffuse[3] - diffuse[3]) * amount},
				new float[] {specular[0] + (m.specular[0] - specular[0]) * amount, specular[1] + (m.specular[1] - specular[1]) * amount, specular[2] + (m.specular[2] - specular[2]) * amount, specular[3] + (m.specular[3] - specular[3]) * amount},
				new float[] {emission[0] + (m.emission[0] - emission[0]) * amount, emission[1] + (m.emission[1] - emission[1]) * amount, emission[2] + (m.emission[2] - emission[2]) * amount, emission[3] + (m.emission[3] - emission[3]) * amount},
				shininess + (m.shininess - shininess) * amount, amount < 0.5 ? texture : m.texture);
	}
}
