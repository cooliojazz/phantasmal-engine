package com.up.pe.render;

import com.up.jovr.structs.TrackedDevicePose_t;
import com.up.pe.math.Matrix3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Positionable;
import com.up.pe.math.Rotatable;
import com.up.pe.mesh.ModifiableMesh;
import com.up.pe.render.camera.Camera;

/**
 *
 * @author Ricky
 */
public class VRTrackedObject {
	
	private Positionable pos;
	private Rotatable rot;

	public VRTrackedObject(ModifiableMesh m) {
		this.pos = m;
		this.rot = m;
	}

	public VRTrackedObject(Positionable pos, Rotatable rot) {
		this.pos = pos;
		this.rot = rot;
	}
	
	public void update(TrackedDevicePose_t pose) {
		float[][] poseMat = pose.getDeviceToAbsoluteTracking().getMatrix();
		pos.setPosition(new Point3D(poseMat[0][3], poseMat[1][3], poseMat[2][3]));
		rot.setRotation(new Matrix3D(new double[][] {
				new double[] {poseMat[0][0], poseMat[0][1], poseMat[0][2]},
				new double[] {poseMat[1][0], poseMat[1][1], poseMat[1][2]},
				new double[] {poseMat[2][0], poseMat[2][1], poseMat[2][2]}}).getRotation());
	}
}
