package com.up.pe.render;

import com.up.pe.render.event.RenderInterceptorAdapter;
import com.up.pe.render.event.RenderInterceptor;
import com.jogamp.graph.curve.opengl.RegionRenderer;
import com.jogamp.graph.curve.opengl.RenderState;
import com.jogamp.graph.curve.opengl.TextRegionUtil;
import com.jogamp.graph.font.Font;
import com.up.pe.render.shader.Shader;
import com.up.pe.render.camera.Camera;
import com.up.pe.math.Point2D;
import com.up.pe.mesh.Mesh;
import java.awt.Color;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.*;
import com.jogamp.opengl.math.Matrix4f;
import com.jogamp.opengl.math.Vec4f;
import com.up.jovr.RenderModel;
import com.up.jovr.enums.EColorSpace;
import com.up.jovr.enums.ETextureType;
import com.up.jovr.enums.ETrackedDeviceClass;
import com.up.jovr.enums.EVRApplicationType;
import com.up.jovr.enums.EVRCompositorError;
import com.up.jovr.enums.EVREye;
import com.up.jovr.structs.Texture_t;
import com.up.jovr.structs.TrackedDevicePose_t;
import com.up.jovr.jni.IVR;
import com.up.jovr.jni.IVRCompositor;
import com.up.jovr.jni.IVRRenderModels;
import com.up.jovr.jni.IVRSystem;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Point4D;
import com.up.pe.math.Positionable;
import com.up.pe.math.Rotatable;
import com.up.pe.math.Vector2D;
import com.up.pe.math.Vector3D;
import com.up.pe.math.Vector4D;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.mesh.loaders.OVRMeshLoader;
import com.up.pe.render.camera.DefaultCamera;
import com.up.pe.render.camera.ReprojectableCamera;
import com.up.pe.render.camera.VRCamera;
import com.up.pe.render.light.DirectionLight;
import com.up.pe.render.light.Light;
import com.up.pe.render.light.PointLight;
import com.up.pe.render.shader.Program;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricky
 */


public class DisplayManager implements GLEventListener {
    
    private final static Logger LOGGER = Logger.getLogger(DisplayManager.class.getName());
	private Font font;
	private RegionRenderer textRenderer;
	private TextRegionUtil textUtil;
    private ReprojectableCamera camera = new DefaultCamera(Matrix4D.identity());
    private double fps = 0;
    private final CopyOnWriteArrayList<Mesh> meshes = new CopyOnWriteArrayList<>();
    private final ArrayList<Mesh> cleanupMeshes = new ArrayList<>();
    private Program prog;
    private int width;
    private int height;
    private GLCanvas canv = null;
    private ArrayList<Light> lights = new ArrayList<>();
    private Matrix4f projection = new Matrix4f();
    private float[] ambient = new float[] {0.1f, 0.1f, 0.1f, 1f};
    protected float[] clear = new float[] {0.05f, 0.05f, 0.05f, 1f};
    private float zNear = 0.1f;
    private float zFar = 100f;
	private final RenderInterceptor renderEvents;
	private List<Program> shaders = new ArrayList<>();
	private LinkedList<RenderPass> renders = new LinkedList<>();
	
	private boolean vr = false;
	private boolean vrReady = false;
	private int vrFBO;
	private IVRSystem vrSystem;
	private IVRCompositor vrCompositor;
	private IVRRenderModels vrModels;
	private Texture_t vrTexture;
	private int[] vrSize = new int[2];
    private VRCamera[] vrCamera = new VRCamera[2];
	private RenderPass[] vrRenders = new RenderPass[2];
	private HashMap<Integer, ETrackedDeviceClass> vrDevices = new HashMap<>();
	private HashMap<Integer, ArrayList<VRTrackedObject>> vrTrackedObjects = new HashMap<>();
	private int vrMaxDevice = 0;
    private double vrScale = 1;

	public DisplayManager() {
		this(new RenderInterceptorAdapter());
	}
	
	public DisplayManager(RenderInterceptor handler) {
		renderEvents = handler;
	}

	public DisplayManager(List<Program> shaders) {
		this(new RenderInterceptorAdapter(), shaders);
	}

	public DisplayManager(RenderInterceptor handler, List<Program> shaders) {
		this(handler);
		this.shaders = shaders;
	}
	
	// Making these static seems like a nice idea to save resources, but linking the same shader to multiple programs fails.
	// Possibly an entire program should be made static as well to be able to use that tactic?
	private Shader vShader = new Shader("defaultVertex", Shader.Type.VERTEX, DisplayManager.class.getResourceAsStream("/com/up/pe/shaders/vertex.glsl"));
	private Shader fShader = new Shader("defaultFragment", Shader.Type.FRAGMENT, DisplayManager.class.getResourceAsStream("/com/up/pe/shaders/fragment.glsl"));
    
    @Override
    public void init(GLAutoDrawable glad) {
        GL2 gl = (GL2)glad.getGL();
        LOGGER.log(Level.INFO, "Using OpenGL version {0}", gl.glGetString​(GL.GL_VERSION));
        LOGGER.log(Level.INFO, "{0} fixed function profile", GLProfile.getMaxFixedFunc(true));
        LOGGER.log(Level.INFO, "{0} program profile", GLProfile.getMaxProgrammable(true));
		
        prog = new Program();
		prog.addShader(vShader);
		prog.addShader(fShader);
		prog.setup(gl);
		prog.use(gl);
		
		for (Program p : shaders) p.setup(gl);
		
		renders.add(new RenderPass(prog, camera, renderEvents));
        
        gl.glShadeModel(GL2.GL_SMOOTH);
        gl.glClearDepth(1.0f);                      // Depth Buffer Setup
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glDepthFunc(GL.GL_LESS);
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST); // Really Nice Perspective Calculations
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA); // Normal blending? (When enabled)
		gl.glEnable(GL2.GL_CULL_FACE);
        gl.glCullFace(GL2.GL_BACK);
//        gl.glEnable(GL2.GL_NORMALIZE);
//        gl.glEnable(GL2.GL_LINE_SMOOTH);
        
//        gl.glEnable(GL2.GL_MULTISAMPLE); // For other type of multisampling not provided by the GLCapabilities sample buffers?
	
        gl.setSwapInterval(0); //Disable vsync
		
        gl.glLineWidth(2); // Why doesn't this work when set in drawAxis? I'd like to be able to set it per render and not globally
		
		try {
			font = DefaultFontSet.get().get(DefaultFontSet.FAMILY_MONOSPACED, 0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		textRenderer = RegionRenderer.create(RegionRenderer.defaultBlendEnable, RegionRenderer.defaultBlendDisable);
		textRenderer.getRenderState().setColorStatic(1, 1, 1, 1);
		textRenderer.getRenderState().setHintMask(RenderState.BITHINT_GLOBAL_DEPTH_TEST_ENABLED);
		textRenderer.init(gl);

		textUtil = new TextRegionUtil(0);
		
		if (vr) setupVR(gl);
	}
	
	private void setupVR(GL2 gl) {
		IVR.VR_Init(EVRApplicationType.VRApplication_Scene);
		vrSystem = IVR.VRSystem();
		vrCompositor = IVR.VRCompositor();
		vrModels = IVR.VRRenderModels();
		
		int[] oldFbo = new int[1];
		gl.glGetIntegerv(GL.GL_FRAMEBUFFER, oldFbo, 0);
		
		int[] ids = new int[1];
		gl.glGenFramebuffers(1, ids, 0);
		vrFBO = ids[0];
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, vrFBO);

		vrSize = new int[2];
		vrSystem.GetRecommendedRenderTargetSize(vrSize);
		
		gl.glGenRenderbuffers(1, ids, 0);
		gl.glBindRenderbuffer(GL.GL_RENDERBUFFER, ids[0]);
		gl.glRenderbufferStorage(GL.GL_RENDERBUFFER, GL2.GL_DEPTH_COMPONENT24, vrSize[0], vrSize[1]);
		gl.glFramebufferRenderbuffer(GL.GL_FRAMEBUFFER, GL.GL_DEPTH_ATTACHMENT, GL.GL_RENDERBUFFER, ids[0]);
		
		gl.glGenTextures(1, ids, 0);
		gl.glBindTexture(GL.GL_TEXTURE_2D, ids[0]);
		gl.glTexImage2D(GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, vrSize[0], vrSize[1], 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, null);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
		gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
		gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0, GL.GL_TEXTURE_2D, ids[0], 0);
		vrTexture = new Texture_t(ids[0], ETextureType.TextureType_OpenGL, EColorSpace.ColorSpace_Auto);
		
		gl.glDrawBuffers(1, new int[] {GL.GL_COLOR_ATTACHMENT0}, 0); 
		
		gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, oldFbo[0]);
		
		for (EVREye eye : EVREye.values()) {
			float[][] translate = vrSystem.GetEyeToHeadTransform(eye).getMatrix();
			float[][] proj = vrSystem.GetProjectionMatrix(eye, zNear, zFar).getMatrix();
			VRCamera c = new VRCamera(camera, new Matrix4D(proj), new Point3D(translate[0][3], translate[1][3], translate[2][3]));
			vrCamera[eye.getValue()] = c;
			trackObject(0, new VRTrackedObject(new Positionable() {
					@Override
					public Point3D getPosition() {
						return c.getHeadPosition();
					}

					@Override
					public void setPosition(Point3D position) {
						c.setHeadPosition(position);
					}
				}, new Rotatable() {
					@Override
					public Angle3D getRotation() {
						return c.getHeadAngle();
					}

					@Override
					public void setRotation(Angle3D rot) {
						c.setHeadAngle(rot);
					}
				}));
			vrRenders[eye.getValue()] = new RenderPass(prog, c, renderEvents);
		}
		
//		vrCamera = new VRCamera(camera);
		
		vrEventThread().start();
		
			int cId = 3;
			trackObject(cId, new VRTrackedObject(addMeshForDevice(cId, Point3D.zeros().addX(20), Angle3D.zeros(), Vector3D.ones(), null)));
		
		vrReady = true;
	}

    @Override
    public void dispose(GLAutoDrawable drawable) {
        
    }

	private long lTime;
    @Override
    public void display(GLAutoDrawable glad) {
        long fpstime = System.nanoTime();
        GL2 gl = (GL2)glad.getGL();
		
        //First make any textures needed
        Texture.texes.entrySet().stream().forEach(e -> {
				e.getKey().setupTexture(glad.getGL(), e.getValue()); 
				Texture.texes.remove(e.getKey());
			});

		//Cleanup any old meshes
        for (int x = 0; x < cleanupMeshes.size(); x++) {
            if (cleanupMeshes.get(x) != null) {
				cleanupMeshes.get(x).cleanupVBOs(gl);
			} else {
				int i = 9;
			}
        }
		cleanupMeshes.clear();
		
        //Update meshes
        for (int x = 0; x < meshes.size(); x++) {
            Mesh m = meshes.get(x);
            if (!m.hasVBOs()) m.createVBOs(gl);
            if (m.needsUpdate()) m.updateVBOs(gl);
        }
		
        //Lights
        FloatBuffer lddb = FloatBuffer.allocate(lights.size() * 3);
        FloatBuffer ldcb = FloatBuffer.allocate(lights.size() * 4);
        FloatBuffer lppb = FloatBuffer.allocate(lights.size() * 3);
        FloatBuffer lpcb = FloatBuffer.allocate(lights.size() * 4);
        FloatBuffer lplb = FloatBuffer.allocate(lights.size());
		for (Light l : lights) {
			if (l instanceof DirectionLight) {
				DirectionLight dl = (DirectionLight)l;
				lddb.put(dl.getDirection().toFloatArray());
				ldcb.put(dl.getColor());
			}
			if (l instanceof PointLight) {
				PointLight pl = (PointLight)l;
				lppb.put(pl.getPosition().toFloatArray());
				lpcb.put(pl.getColor());
				lplb.put(pl.getDistance());
			}
		}
        lddb.flip();
        ldcb.flip();
        lppb.flip();
        lpcb.flip();
        lplb.flip();
        gl.glUniform3fv(gl.glGetUniformLocation(prog.getId(), "dLightDir"), lddb.limit() / 3, lddb);
        gl.glUniform4fv(gl.glGetUniformLocation(prog.getId(), "dLightColor"), lddb.limit() / 3, ldcb);
        gl.glUniform1i(gl.glGetUniformLocation(prog.getId(), "dLights"), lddb.limit() / 3);
        gl.glUniform3fv(gl.glGetUniformLocation(prog.getId(), "pLightPos"), lplb.limit(), lppb);
        gl.glUniform4fv(gl.glGetUniformLocation(prog.getId(), "pLightColor"), lplb.limit(), lpcb);
        gl.glUniform1fv(gl.glGetUniformLocation(prog.getId(), "pLightLen"), lplb.limit(), lplb);
        gl.glUniform1i(gl.glGetUniformLocation(prog.getId(), "pLights"), lplb.limit());

        //Constants
        gl.glUniform4fv(gl.glGetUniformLocation(prog.getId(), "ambientLight"), 1, ambient, 0);
		gl.glUniform1i(gl.glGetUniformLocation(prog.getId(), "tex"), 0);

		for (RenderPass p : renders) p.render(this, gl);
		
		if (vr) {
			TrackedDevicePose_t[] poses = new TrackedDevicePose_t[vrMaxDevice + 1];
			vrCompositor.WaitGetPoses(poses);
			for (Map.Entry<Integer, ArrayList<VRTrackedObject>> e : vrTrackedObjects.entrySet()) {
				e.getValue().forEach(o -> o.update(poses[e.getKey()]));
			}
			
			int[] oldFbo = new int[1];
			gl.glGetIntegerv(GL.GL_FRAMEBUFFER, oldFbo, 0);
			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, vrFBO);
			gl.glViewport(0, 0, vrSize[0], vrSize[1]);
			
//			vrCompositor.ClearLastSubmittedFrame();
			
			for (EVREye eye : EVREye.values()) {
				vrRenders[eye.getValue()].render(this, gl);
				EVRCompositorError errL = vrCompositor.Submit(eye, vrTexture);
				if (errL != EVRCompositorError.VRCompositorError_None) LOGGER.log(Level.SEVERE, errL.toString());
			}

			gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, oldFbo[0]);
			gl.glViewport(0, 0, getWidth(), getHeight());
		}
        
//        fps = 1000000000d / (System.nanoTime() - fpstime);
        fps = 1000000000d / (System.nanoTime() - lTime);
        lTime = System.nanoTime();
    }
    
    public Point2D project(Camera c, Point3D p) {
		Vector4D out = c.getProjection().multiply(c.getTransform()).apply(new Vector4D(p.getX(), p.getY(), p.getZ(), 1));
		return new Point2D(out.getX() / out.getW(), out.getY() / out.getW());
    }
    
    public Point3D unproject(Camera c, Point2D p) {
        Matrix4f temp = c.getProjection().multiply(c.getTransform()).toGlMatrix(); // Can replace this when true 4x4 invert is added
		temp.invert();
		Vec4f out = new Vec4f();
		temp.mulVec4f(new Vec4f((float)p.getX(), (float)p.getY(), -1, 1), out);
		return new Point3D(out.x() / out.w(), out.y() / out.w(), out.z() / out.w());
    }
    
    public Vector3D unprojectScreenNormal(Camera c, Point2D p) {
        Matrix4f temp = c.getProjection().multiply(c.getTransform()).toGlMatrix(); // Can replace this when true 4x4 invert is added
		temp.invert();
		Vec4f out = new Vec4f();
		temp.mulVec4f(new Vec4f((float)p.getX(), (float)p.getY(), 1, 1), out);
		return new Vector3D(out.x() / out.w(), out.y() / out.w(), out.z() / out.w()).normalize();
    }
    
    public void drawAxis(GL2 gl, int len) {
        //Axis
        gl.glUseProgram(0);
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glBegin(GL2.GL_LINES);
//        gl.glLineWidth(5);
//		int[] temp = new int[] {-1, -1};
//		gl.glGetIntegerv(GL2.GL_LINE_WIDTH_RANGE, temp, 0);
//		gl.glGetIntegerv(GL2.GL_ALIASED_LINE_WIDTH_RANGE, temp, 0);
//		gl.glGetIntegerv(GL2.GL_SMOOTH_LINE_WIDTH_RANGE, temp, 0);
//		System.out.println(temp[0] + " " + temp[1]);
		
        gl.glColor3d(.5, 0, 0);
        gl.glVertex3d(-len, 0, 0);
        gl.glColor3d(1, .5, .5);
        gl.glVertex3d(len, 0, 0);
        
        gl.glColor3d(0, .5, 0);
        gl.glVertex3d(0, -len, 0);
        gl.glColor3d(.5, 1, .5);
        gl.glVertex3d(0, len, 0);
        
        gl.glColor3d(0, 0, .5);
        gl.glVertex3d(0, 0, -len);
        gl.glColor3d(.5, .5, 1);
        gl.glVertex3d(0, 0, len);
        gl.glEnd();
    }
    
	/**
	 * Renders a String in screen pixel coordinates
	 * @param gl
	 * @param s
	 * @param color
	 * @param pos
	 * @param size 
	 */
    public void renderString(GL2 gl, String s, Color color, Point2D pos, int size) {
		renderString(gl, s, color, pos.mul(new Point2D(1.0 / getWidth(), 1.0 / getHeight())).mul(2).sum(-1), new Vector2D((double)size / getHeight(), (double)size / getHeight()));
    }
    
	/**
	 * Renders a String in normalized screen coordinates [-1, 1]
	 * @param gl
	 * @param s
	 * @param color
	 * @param pos
	 * @param scale 
	 */
    public void renderString(GL2 gl, String s, Color color, Point2D pos, Vector2D scale) {
		Matrix4f inv = new Matrix4f(projection);
		inv.invert();
		
		gl.glPushMatrix();
		gl.glLoadIdentity();
		Vec4f proj = inv.mulVec4f(new Vec4f((float)pos.getX(), (float)pos.getY(), -0.99f, 1));
//		Vec4f xVec = inv.mulVec4f(new Vec4f(-1, 0, -1, 1));
//		Vec4f yVec = inv.mulVec4f(new Vec4f(0, -1, -1, 1));
//		double pXScale = new Vec3f(xVec.x() / xVec.w(), xVec.y() / xVec.w(), xVec.z() / xVec.w()).dist(new Vec3f(0, 0, -zNear));
//		double pYScale = new Vec3f(yVec.x() / yVec.w(), yVec.y() / yVec.w(), yVec.z() / yVec.w()).dist(new Vec3f(0, 0, -zNear));
		// Not sure why this doesn't work still, but this is close enough for now
		final double pScale = 0.125;
		gl.glTranslatef(proj.x() / proj.w(), proj.y() / proj.w(), proj.z() / proj.w());
		gl.glScaled(pScale * scale.getX(), pScale * scale.getY(), 1);
		gl.glColor4fv(Material.colorToShaderColor(color), 0);
		textUtil.drawString3D(gl, textRenderer, font, s, new Vec4f(), new int[] {1});
		gl.glPopMatrix();
    }
    
    
	/**
	 * Renders a String in world coordinates
	 * @param gl
	 * @param s
	 * @param color
	 * @param pos
	 * @param rot
	 * @param scale 
	 */
    public void renderString(GL2 gl, String s, Color color, Point3D pos, Angle3D rot, Vector2D scale) {
		gl.glDisable(GL2.GL_CULL_FACE);
		gl.glPushMatrix();
		gl.glTranslated(pos.getX(), pos.getY(), pos.getZ());
		gl.glMultMatrixf(rot.toMatrix().toTransformationMatrix().toRawGlFMatrix(), 0);
		gl.glScaled(scale.getX(), scale.getY(), 1);
		gl.glColor4fv(Material.colorToShaderColor(color), 0);
		textUtil.drawString3D(gl, textRenderer, font, s, new Vec4f(), new int[] {1});
		gl.glPopMatrix();
		gl.glEnable(GL2.GL_CULL_FACE);
    }
	
	public double textWidth(String s, Vector2D scale) {
		// Fast hack for monospaced fonts, needs to detect others and properly iterate entire string otherwise.
		return s.length() * font.getAdvanceWidth(font.getGlyphID(' ')) * scale.getX();
	}
	
	public double textHeight(Vector2D scale) {
		return font.getLineHeight() * scale.getY();
	}
    
    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        GL2 gl = (GL2)glad.getGL();
        GLU glu = new GLU();
		
        this.width = width;
        this.height = height;
//        if (height <= 0) height = 1;
        float h = (float)width / height;
		float fovy = 60;
        
        gl.glViewport(0, 0, width, height);
		
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(fovy, h, zNear, zFar);
        projection.loadIdentity();
        projection.setToPerspective(fovy / 180 * (float)Math.PI, h, zNear, zFar);
		camera.setProjection(Matrix4D.fromGlMatrix(projection));
		
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
		
		textRenderer.enable(gl, true);
		textRenderer.reshapeOrtho(width, height, zNear, zFar);
		textRenderer.enable(gl, false);
    }
	
    public float getRenderDistance() {
		return zFar;
	}
	
    public void setRenderDistance(float distance) {
		zFar = distance;
	}
	
    public ReprojectableCamera getCamera() {
        return camera;
    }
    
    public void setCamera(ReprojectableCamera c) {
        camera = c;
    }
    
    public double getVRScale() {
        return vrScale;
    }
    
    public void setVRScale(double s) {
        vrScale = s;
    }

    public void setAmbient(float[] ambient) {
        this.ambient = ambient;
    }

    public void setAmbient(Color ambient) {
        this.ambient = Material.colorToShaderColor(ambient);
    }
    
    public double getFPS() {
        return fps;
    }
    
    /**
     * Gets the GLCanvas associated with this DisplayManager, or creates a new one with default capabilities
     * @return The GLCanvas
     */
    public GLCanvas getCanvas() {
        if (canv == null) {
            canv = new GLCanvas();
            canv.addGLEventListener(this);
        }
        return canv;
    }
    
    /**
     * Gets the GLCanvas associated with this DisplayManager, or creates a new one with the given GLCapabilities
     * @param cap The capabilities to create the GLCanvas with in the case it does not exist.
     * @return The GLCanvas
     */
    public GLCanvas getCanvas(GLCapabilitiesImmutable cap) {
        if (canv == null) {
            canv = new GLCanvas(cap);
            canv.addGLEventListener(this);
        }
        return canv;
    }
	
	public void addRenderPass(RenderPass pass) {
		renders.add(pass);
	}
	
	public void insertRenderPass(RenderPass pass, int pos) {
		renders.add(pos, pass);
	}
    
    public void addMesh(Mesh m) {
        meshes.add(m);
    }
    
    public void addMeshes(List<? extends Mesh> m) {
        meshes.addAll(m);
    }
    
    public void removeMesh(Mesh m) {
		cleanupMeshes.add(m);
        meshes.remove(m);
    }
    
    public void clearMeshes() {
		cleanupMeshes.addAll(meshes);
        meshes.clear();
    }
    
    public double getLastFPS() {
        return fps;
    }
    
    public void addLight(Light light) {
        if (lights.size() < 100) {
            lights.add(light);
        }
    }
    
    public List<Mesh> getMeshes() {
        return (List<Mesh>)meshes.clone();
    }
    
    public void setClearColor(Color c) {
        c.getRGBColorComponents(clear);
    }
    
    /**
     * Starts and returns the loop thread
     * @return 
     */
    public Thread startDefaultRenderLoop() {
        Thread t = new Thread(() -> {
            while (true) {
                canv.repaint();
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) { }
            }
        });
        t.start();
        return t;
    }

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public void useVR() {
		vr = true;
	}
	
    private Thread vrEventThread() {
        Thread t = new Thread(() -> {
            while (true) {
				// Check new devices
				int newMax = 0;
				for (int i = 0; i < 64; i++) {
					vrDevices.put(i, vrSystem.GetTrackedDeviceClass(i));
					if (vrDevices.get(i) != ETrackedDeviceClass.TrackedDeviceClass_Invalid) newMax = i;
				}
				vrMaxDevice = newMax;
				
				// Do input?
				
				
				// anything else?
				
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) { }
            }
        }, "VR Polling");
        return t;
    }
	
	public int[] findVRControllers() {
		return vrDevices.entrySet().stream().filter(e -> e.getValue() == ETrackedDeviceClass.TrackedDeviceClass_Controller).mapToInt(e -> e.getKey()).toArray();
	}
	
	public SimpleMesh addMeshForDevice(int deviceId, Point3D pos, Angle3D rot, Vector3D scale, Material mat) {
		RenderModel model = vrModels.loadFullModelFromDevice(vrSystem, deviceId);
		SimpleMesh mesh = OVRMeshLoader.convert(model, pos, rot, scale, mat);
		addMesh(mesh);
		return mesh;
	}
	
	public void trackObject(int deviceId, VRTrackedObject o) {
		if (!vrTrackedObjects.containsKey(deviceId)) vrTrackedObjects.put(deviceId, new ArrayList<>());
		vrTrackedObjects.get(deviceId).add(o);
	}
	
	public void stopTrackingObject(int deviceId, VRTrackedObject o) {
		vrTrackedObjects.get(deviceId).remove(o);
		if (vrTrackedObjects.get(deviceId).isEmpty()) vrTrackedObjects.remove(deviceId);
	}
}
