package com.up.pe.render.event;

import com.jogamp.opengl.GL2;
import com.up.pe.render.DisplayManager;

/**
 *
 * @author Ricky
 */
public class RenderInterceptorAdapter implements RenderInterceptor {

	@Override
	public void preTransformation(DisplayManager dm, GL2 gl) { }

	@Override
	public void postRender(DisplayManager dm, GL2 gl) { }

	@Override
	public void preRender(DisplayManager dm, GL2 gl) { }
	
}
