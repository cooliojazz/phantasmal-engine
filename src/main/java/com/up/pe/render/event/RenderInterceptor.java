package com.up.pe.render.event;

import com.jogamp.opengl.GL2;
import com.up.pe.render.DisplayManager;

/**
 *
 * @author Ricky
 */
public interface RenderInterceptor {

	public void preTransformation(DisplayManager dm, GL2 gl);

	public void preRender(DisplayManager dm, GL2 gl);

	public void postRender(DisplayManager dm, GL2 gl);
	
}
