package com.up.pe.render;

import com.jogamp.opengl.*;
import com.jogamp.opengl.glu.*;
import com.up.jovr.structs.RenderModel_TextureMap_t;
import java.io.*;
import java.nio.*;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.*;
import java.util.concurrent.ConcurrentHashMap;
import javax.imageio.ImageIO;

/**
 * A class that loads and keeps track of textures
 * @author Ricky
 */
public class Texture {
    
    private int[] textures = new int[3];
    public String name = "";
    public static ConcurrentHashMap<Texture, BufferedImage> texes = new ConcurrentHashMap<>();
    
    public Texture(InputStream is) {
        loadTexture(is);
    }
    
    public Texture(BufferedImage img) {
        loadTexture(img);
    }
	
    public Texture(GL gl, InputStream is) {
        loadTexture(gl, is);
    }
    
    public Texture(GL gl, BufferedImage img) {
        setupTexture(gl, img);
    }
    
    public Texture(GL gl, String f) {
        loadTexture(gl, getClass().getResourceAsStream(f));
        name = f;
    }
    
    private BufferedImage loadImage(InputStream is) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(is);
		} catch (Exception ex) {
			img = new BufferedImage(2, 2, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D g = img.createGraphics();
			g.setPaint(Color.WHITE);
			g.fillRect(0, 0, 2, 2);
			System.out.println("Could not load tex; used 2x2 white square instead.");
		}
		return img;
    }
    
    public void loadTexture(GL gl, InputStream is) {
		setupTexture(gl, loadImage(is));
    }
    
    public void setupTexture(GL gl, BufferedImage img) {
        try {
            texes.remove(this);
            textures = new int[1];
            gl.glGenTextures(1, textures, 0);
            gl.glBindTexture(GL.GL_TEXTURE_2D, textures[0]);
            
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_LINEAR);
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_LINEAR);
            
            makeRGBTexture(gl, img, GL.GL_TEXTURE_2D);
        } catch (Exception e) {
            System.out.println("Something went wrong creating the texture!");
            e.printStackTrace();
        }
    }
    
    private void loadTexture(InputStream is) {
        texes.put(this, loadImage(is));
    }
    
    private void loadTexture(BufferedImage img) {
        texes.put(this, img);
    }
    
	// Unused now? Should textures even be an array?
//    public int getID(int type) {
//        return textures[type];
//    }
    
    public int getID() {
        return textures[0];
    }
        
    private void makeRGBTexture(GL gl, BufferedImage img, int target) {
        ByteBuffer dest = null;
        int width = img.getWidth();
        int height = img.getHeight();
        switch (img.getType()) {
            case BufferedImage.TYPE_CUSTOM: {
				if (img.getColorModel() == RenderModel_TextureMap_t.vrColorModel) {
					byte[] data = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
					dest = ByteBuffer.allocateDirect(data.length);
					dest.order(ByteOrder.nativeOrder());
					dest.put(data, 0, data.length);
					dest.rewind();
					gl.glTexImage2D(target, 0, GL.GL_RGBA, width, height, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, dest);
					
					gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
					gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
					break;
				}
			}
            case BufferedImage.TYPE_3BYTE_BGR:{
                byte[] data = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
                dest = ByteBuffer.allocateDirect(data.length);
                dest.order(ByteOrder.nativeOrder());
                dest.put(data, 0, data.length);
                dest.rewind();
				gl.glPixelStorei(GL.GL_UNPACK_ALIGNMENT, 3);
				gl.glTexImage2D(target, 0, GL.GL_RGB, width, height, 0, GL.GL_BGR, GL.GL_UNSIGNED_BYTE, dest);
                break;
            }
            case BufferedImage.TYPE_INT_RGB: {
                int[] data = ((DataBufferInt)img.getRaster().getDataBuffer()).getData();
                dest = ByteBuffer.allocateDirect(data.length * 4);
                dest.order(ByteOrder.nativeOrder());
                dest.asIntBuffer().put(data, 0, data.length);
				dest.rewind();
				gl.glTexImage2D(target, 0, GL.GL_RGB, width, height, 0, GL.GL_BGRA, GL2.GL_UNSIGNED_INT_8_8_8_8_REV, dest);
                break;
            }
            case BufferedImage.TYPE_4BYTE_ABGR: {
                byte[] data = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
                dest = ByteBuffer.allocateDirect(data.length);
                dest.order(ByteOrder.nativeOrder());
                dest.put(data, 0, data.length);
                dest.rewind();
				gl.glTexImage2D(target, 0, GL.GL_RGBA, width, height, 0, GL2.GL_ABGR_EXT, GL.GL_UNSIGNED_BYTE, dest);
                break;
            }
            default: throw new RuntimeException("Unsupported image type " + img.getType());
        }
    }
}
