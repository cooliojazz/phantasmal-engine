package com.up.pe.render;

import com.up.pe.math.Point3D;
import com.up.pe.math.Point2D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.attribute.Attributable;
import com.up.pe.mesh.attribute.Attribute;
import com.up.pe.mesh.attribute.AttributeSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Stores information about a single vertex in a mesh such as position, normal, and texture information.
 * @author Ricky
 */
public class Vertex implements Cloneable {
	
    private Point3D pos;
    private Point2D tex;
    private Vector3D normal;
    private Material mat = null;

    public Vertex(Material mat) {
        pos = new Point3D(0, 0, 0);
        normal = new Vector3D(0, 0, 0);
        this.mat = mat;
    }

    public Vertex(Point3D pos) {
        this.pos = pos;
        normal = new Vector3D(0, 0, 0);
        this.tex = new Point2D(0, 0);
    }

    public Vertex(Point3D pos, Material mat) {
        this.pos = pos;
        normal = new Vector3D(0, 0, 0);
        this.tex = new Point2D(0, 0);
        this.mat = mat;
    }

    public Vertex(Point3D pos, Vector3D normal) {
        this.pos = pos;
        this.normal = normal;
        this.tex = new Point2D(0, 0);
    }

    public Vertex(Point3D pos, Vector3D normal, Material mat) {
        this.pos = pos;
        this.normal = normal;
        this.tex = new Point2D(0, 0);
        this.mat = mat;
    }

    public Vertex(Point3D pos, Point2D tex, Material mat) {
        this.pos = pos;
        normal = new Vector3D(0, 0, 0);
        this.tex = tex;
        this.mat = mat;
    }

    public Vertex(Point3D pos, Vector3D normal, Point2D tex, Material mat) {
        this.pos = pos;
        this.normal = normal;
        this.tex = tex;
        this.mat = mat;
    }

    public Vector3D getNormal() {
        return normal;
    }

    public Point3D getPosition() {
        return pos;
    }

    public Point2D getTexPosition() {
        return tex;
    }
    
    public Material getMaterial() {
        return mat;
    }

    public void setNormal(Vector3D normal) {
        this.normal = normal;
    }

    public void setPosition(Point3D pos) {
        this.pos = pos;
    }

    public void setTexPosition(Point2D tex) {
        this.tex = tex;
    }
    
    public void setMaterial(Material mat) {
        this.mat = mat;
    }
    
    public static void addNormals(Vertex v1, Vertex v2, Vertex v3) {
        Vector3D lv1 = v1.getPosition().vectorTo(v2.getPosition());
        Vector3D lv2 = v2.getPosition().vectorTo(v3.getPosition());
        Vector3D n = lv1.crossProduct(lv2).normalize();
        v1.setNormal(n);
        v2.setNormal(n);
        v3.setNormal(n);
    }

	@Override
	public Vertex clone() {
		return new Vertex(pos, normal, tex, mat);
	}
    
    
    
    ///////////////////////
    
//    ArrayList<Attributable> attributes = new ArrayList<>();
    AttributeSet attributes;
    HashMap<Attribute, Attributable> attributeData;
    
}
