package com.up.pe.render.camera;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.math.Point3D;

/**
 *
 * @author Ricky
 */
public class DefaultCamera extends ReprojectableCamera {
	
    private Point3D pos = Point3D.zeros();
    private Angle3D rot = Angle3D.zeros();

	public DefaultCamera(Matrix4D proj) {
		super(proj);
	}

	@Override
    public Point3D getPosition() {
        return pos;
    }

    public Angle3D getAngle() {
        return rot;
    }

    public void setPosition(Point3D pos) {
        this.pos = pos;
    }

    public void setAngle(Angle3D dir) {
        this.rot = dir;
    }

	@Override
	public Matrix4D getTransform() {
		return rot.toMatrix().transpose().toTransformationMatrix().multiply(Matrix4D.offsetBy(pos.mul(-1)));
	}
    
    /**
     * Assumes no tilting of camera is wanted (Camera Y should stay perpendicular to world X-Z plane). If needed, set Z after calling this.
     * @param p Point to point the camera at
     */
    public void lookAt(Point3D p) {
        rot = Camera.lookAt(pos, p);
    }
}
