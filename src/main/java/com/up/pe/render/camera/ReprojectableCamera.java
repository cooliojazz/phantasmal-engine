package com.up.pe.render.camera;

import com.up.pe.math.Matrix4D;

/**
 *
 * @author Ricky
 */
public abstract class ReprojectableCamera implements Camera {
	
	private Matrix4D proj;

	public ReprojectableCamera(Matrix4D proj) {
		this.proj = proj;
	}

	@Override
	public Matrix4D getProjection() {
		return proj;
	}

	public void setProjection(Matrix4D proj) {
		this.proj = proj;
	}
	
}
