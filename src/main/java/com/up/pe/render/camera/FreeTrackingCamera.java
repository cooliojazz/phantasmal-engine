package com.up.pe.render.camera;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Positionable;
import java.util.function.Supplier;

/**
 *
 * @author Ricky
 */
public class FreeTrackingCamera extends ReprojectableCamera {
    
    private Supplier<Point3D> target;
    private Point3D pos = Point3D.zeros();
    private Angle3D rot = Angle3D.zeros();
    private Point3D offset = Point3D.zeros();
    private Angle3D hoverAngle = Angle3D.zeros();
    private double distance = 10;

	public FreeTrackingCamera(Supplier<Point3D> target, Matrix4D proj) {
		super(proj);
		this.target = target;
	}

	public FreeTrackingCamera(Supplier<Point3D> target) {
		super(Matrix4D.identity());
		this.target = target;
	}
	
	public FreeTrackingCamera(Matrix4D proj) {
		super(proj);
		this.target = null;
	}

    public double getDistance() {
        return distance;
    }

    public Angle3D getHoverAngle() {
        return hoverAngle;
    }

    public Point3D getOffset() {
        return offset;
    }

    public Point3D getTarget() {
        return target.get();
    }

    public void setDistance(double distance) {
        this.distance = distance;
        updateCamera();
    }

    public void setHoverAngle(Angle3D angle) {
        this.hoverAngle = angle;
        updateCamera();
    }

    public void setOffset(Point3D offset) {
        this.offset = offset;
        updateCamera();
    }

    public void setTarget(Positionable target) {
        this.target = target;
        updateCamera();
    }
    
    private void updateCamera() {
        pos = new Point3D(0, 0, -1).rotate(hoverAngle).mul(distance).sum(offset);
        rot = Camera.lookAt(getPosition(), offset);
    }

    public Point3D getPosition() {
        return pos.sum(target != null ? target.get() : Point3D.zeros());
    }

    public Angle3D getAngle() {
        return rot;
    }

	@Override
	public Matrix4D getTransform() {
		return rot.toMatrix().transpose().toTransformationMatrix().multiply(Matrix4D.offsetBy(getPosition().mul(-1)));
	}
}
