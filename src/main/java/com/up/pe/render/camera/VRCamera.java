package com.up.pe.render.camera;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.math.Point3D;

/**
 *
 * @author Ricky
 */
public class VRCamera implements Camera {
	
	private Camera baseCamera;
	private Matrix4D proj;
	private Point3D eyePos;
	private double scale = 1;
    private Point3D headPos = Point3D.zeros();
    private Angle3D headAngle = Angle3D.zeros();
	
	public VRCamera(Camera baseCamera, Matrix4D proj, Point3D eyePos) {
		this.baseCamera = baseCamera;
		this.proj = proj;
		this.eyePos = eyePos;
	}

	public VRCamera(Camera baseCamera, Matrix4D proj, Point3D eyePos, double scale) {
		this.baseCamera = baseCamera;
		this.proj = proj;
		this.eyePos = eyePos;
		this.scale = scale;
	}

    public Point3D getHeadPosition() {
        return headPos;
    }

    public void setHeadPosition(Point3D headPos) {
        this.headPos = headPos;
    }

    public Angle3D getHeadAngle() {
        return headAngle;
    }

    public void setHeadAngle(Angle3D headAngle) {
        this.headAngle = headAngle;
    }

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	@Override
	public Matrix4D getProjection() {
		return proj;
	}

	@Override
	public Matrix4D getTransform() {
		return headAngle.toMatrix().transpose().toTransformationMatrix().multiply(Matrix4D.offsetBy(headPos.sum(eyePos.mul(scale)).mul(-1))).multiply(baseCamera.getTransform());
	}
}
