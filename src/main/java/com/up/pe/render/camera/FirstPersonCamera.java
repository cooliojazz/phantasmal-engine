package com.up.pe.render.camera;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.math.Point3D;
import java.util.function.Supplier;

/**
 *
 * @author Ricky
 */
public class FirstPersonCamera extends DefaultCamera {
    
    private Supplier<Point3D> targetPos;
    private Supplier<Angle3D> targetAngle;

    public FirstPersonCamera(Supplier<Point3D> targetPos, Supplier<Angle3D> targetAngle) {
		super(Matrix4D.identity());
        this.targetPos = targetPos;
        this.targetAngle = targetAngle;
    }

	public FirstPersonCamera(Supplier<Point3D> targetPos, Supplier<Angle3D> targetAngle, Matrix4D proj) {
		super(proj);
		this.targetPos = targetPos;
		this.targetAngle = targetAngle;
	}

    public Point3D getTargetPosition() {
        return targetPos.get();
    }

    public void setTargetPosition(Supplier<Point3D> targetPos) {
        this.targetPos = targetPos;
    }

    public Angle3D getTargetAngle() {
        return targetAngle.get();
    }

    public void setTargetAngle(Supplier<Angle3D> targetAngle) {
        this.targetAngle = targetAngle;
    }

    @Override
    public Point3D getPosition() {
        return super.getPosition().sum(targetPos.get() != null ? targetPos.get() : Point3D.zeros());
    }

    @Override
    public Angle3D getAngle() {
        return super.getAngle().combine(targetAngle.get() != null ? targetAngle.get() : Angle3D.zeros());
    }

	@Override
	public Matrix4D getProjection() {
		return targetAngle.get().toMatrix().toTransformationMatrix().multiply(Matrix4D.offsetBy(targetPos.get().mul(-1))).multiply(super.getProjection());
	}
	
}
