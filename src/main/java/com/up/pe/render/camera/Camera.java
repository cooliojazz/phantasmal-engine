package com.up.pe.render.camera;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;

/**
 *
 * @author Ricky
 */
public interface Camera {

//    public Point3D getPosition();
//    public Angle3D getAngle();
    public Matrix4D getProjection();
    public Matrix4D getTransform();
	
	/**
	 * Assuming getTransform returns a proper affine transformation matrix, this extracts the translation into a Point3D
	 * @return 
	 */
	public default Point3D getPosition() {
		Matrix4D mat = getTransform();
		return new Point3D(mat.get(0, 2), mat.get(1, 2), mat.get(2, 2));
	}
    
    /**
     * Assumes no tilting of camera is wanted (Camera Y should stay perpendicular to world X-Z plane). If needed, set Z after calling this.
     * @param t Point to point the camera at
     */
    public static Angle3D lookAt(Point3D c, Point3D t) {
        Vector3D dir = c.vectorTo(t).normalize();
        return new Angle3D(Math.asin(dir.getY()), Math.atan2(-dir.getX(), -dir.getZ()), 0);
    }
	
}
