package com.up.pe.render.light;

import com.up.pe.math.Vector3D;
import com.up.pe.render.light.Light;

/**
 *
 * @author Ricky
 */
public class DirectionLight implements Light {
	
    private Vector3D dir;
    private float[] color;

    public DirectionLight(Vector3D dir, float[] color) {
        this.dir = dir;
        this.color = color;
    }

	public Vector3D getDirection() {
		return dir;
	}

	public void setDirection(Vector3D dir) {
		this.dir = dir;
	}

	public float[] getColor() {
		return color;
	}

	public void setColor(float[] color) {
		this.color = color;
	}
    
}
