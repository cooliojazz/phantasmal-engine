package com.up.pe.render.light;

import com.up.pe.math.Point3D;
import com.up.pe.render.light.Light;

/**
 *
 * @author Ricky
 */
public class PointLight implements Light {
	
    private Point3D pos;
    private float[] color;
    private float distance;

    public PointLight(Point3D pos, float[] color, float distance) {
        this.pos = pos;
        this.color = color;
        this.distance = distance;
    }

	public Point3D getPosition() {
		return pos;
	}

	public void setPosition(Point3D pos) {
		this.pos = pos;
	}

	public float[] getColor() {
		return color;
	}

	public void setColor(float[] color) {
		this.color = color;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}
    
}
