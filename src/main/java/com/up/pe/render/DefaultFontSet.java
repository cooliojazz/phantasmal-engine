package com.up.pe.render;

import com.jogamp.common.util.IntObjectHashMap;
import com.jogamp.graph.font.Font;
import com.jogamp.graph.font.FontFactory;
import com.jogamp.graph.font.FontSet;
import static com.jogamp.graph.font.FontSet.FAMILY_LIGHT;
import static com.jogamp.graph.font.FontSet.FAMILY_MEDIUM;
import static com.jogamp.graph.font.FontSet.FAMILY_REGULAR;
import static com.jogamp.graph.font.FontSet.STYLE_BOLD;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Based on jogamp.graph.font.JavaFontLoader
 *
 * @author Ricky
 */
public class DefaultFontSet implements FontSet {

	// FIXME: Add cache size to limit memory usage
	private static final IntObjectHashMap fontMap = new IntObjectHashMap();

	private static final FontSet fontLoader = new DefaultFontSet();

	public static FontSet get() {
		return fontLoader;
	}

	// Need to better map to styles
	final static String availableFontFileNames[] = {
			"FiraCode-Bold.ttf",
			"FiraCode-Light.ttf",
			"FiraCode-Medium.ttf",
			"FiraCode-Regular.ttf",
			"FiraCode-Retina.ttf",
			"FiraCode-SemiBold.ttf"};

	static boolean is(final int bits, final int bit) {
		return 0 != (bits & bit);
	}

	@Override
	public Font getDefault() throws IOException {
		return get(FAMILY_REGULAR, 0); // Sans Serif Regular
	}

	@Override
	public Font get(final int family, final int style) throws IOException {
		Font font = (Font)fontMap.get((family << 8) | style);
		if (font != null) {
			return font;
		}
		if (is(style, STYLE_BOLD)) abspath(availableFontFileNames[0], family, style);
		switch (family) {
			case FAMILY_LIGHT:
				font = abspath(availableFontFileNames[1], family, style);
				break;
			case FAMILY_MEDIUM:
				font = abspath(availableFontFileNames[2], family, style);
				break;
			case FAMILY_REGULAR:
				font = abspath(availableFontFileNames[3], family, style);
				break;
			default:
				font = abspath(availableFontFileNames[3], family, style);
		}
		return font;
	}

	Font abspath(final String fname, final int family, final int style) throws IOException {
		try {
			final Font f = abspathImpl(fname, family, style);
			if (null != f) {
				return f;
			}
			throw new IOException(String.format("Problem loading font %s", fname));
		} catch (final IOException ioe) {
			throw new IOException(String.format("Problem loading font %s", fname), ioe);
		}
	}

	private Font abspathImpl(final String fname, final int family, final int style) throws IOException {
		byte[] file = getClass().getResourceAsStream("/com/up/pe/fonts/" + fname).readAllBytes();
		final Font f = FontFactory.get(new ByteArrayInputStream(file), file.length, true);
		if (null != f) {
			fontMap.put((family << 8) | style, f);
			return f;
		}
		return null;
	}
}
