package com.up.pe.render.shader;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GL3;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Ricky
 */
public class Shader {
	
    private final static Logger LOGGER = Logger.getLogger(Shader.class.getName());
	
	private String name;
	private Type type;
	private String source;
	private boolean ready = false;
	private int id = -1;

	public Shader(String name, Type type, File sourceFile) throws FileNotFoundException {
		this(name, type, new FileInputStream(sourceFile));
	}

	public Shader(String name, Type type, InputStream sourceInput) {
		this.name = name;
		this.type = type;
		source = new BufferedReader(new InputStreamReader(sourceInput)).lines().collect(Collectors.joining("\n"));
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public String getSource() {
		return source;
	}

	public boolean isReady() {
		return ready;
	}

	public int getId() {
		return id;
	}
	
	public boolean setup(GL2 gl) {
        id = gl.glCreateShader(type.glVal);
        gl.glShaderSource(id, 1, new String[] {source}, null, 0);
        gl.glCompileShader(id);
        IntBuffer status = IntBuffer.allocate(1);
        gl.glGetShaderiv(id, GL2.GL_COMPILE_STATUS, status);
        if (status.get(0) != 1) {
            gl.glGetShaderiv(id, GL2.GL_INFO_LOG_LENGTH, status);
            int size = status.get(0);
            LOGGER.severe("Shader compilation error: ");
            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetShaderInfoLog(id, size, status, byteBuffer);
                try (FileOutputStream os = new FileOutputStream("glsl-error." + System.currentTimeMillis() + ".log")) {
                    os.write(byteBuffer.array());
                } catch (IOException ex) {
                    LOGGER.severe(ex.getMessage());
                }
                LOGGER.severe(new String(byteBuffer.array()));
            } else {
                LOGGER.severe("Error unspecified.");
            }
        } else {
            LOGGER.log(Level.INFO, "Shader {0} loaded correctly.", name);
			ready = true;
        }
		return ready;
	}
	
	public static enum Type {
		VERTEX(GL2.GL_VERTEX_SHADER), TESSELATION_CONTROL(GL3.GL_TESS_CONTROL_SHADER), TESSELATION_EVALUATION(GL3.GL_TESS_EVALUATION_SHADER),
		GEOMETRY(GL3.GL_GEOMETRY_SHADER), FRAGMENT(GL2.GL_FRAGMENT_SHADER), COMPUTE(GL3.GL_COMPUTE_SHADER);
		
		protected int glVal;

		private Type(int glVal) {
			this.glVal = glVal;
		}
		
	}
}
