package com.up.pe.render.shader;

import com.jogamp.opengl.GL2;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ricky
 */
public class Program {
	
    private final static Logger LOGGER = Logger.getLogger(Program.class.getName());
	
	private final ArrayList<Shader> shaders;
	private int id = -1;
	private boolean ready = false;

	public Program() {
		this(new ArrayList<>());
	}
	
	public Program(Shader... shaders) {
		this(new ArrayList<>(Arrays.asList(shaders)));
	}

	public Program(ArrayList<Shader> shaders) {
		this.shaders = shaders;
	}

	public boolean isReady() {
		return ready;
	}

	public int getId() {
		return id;
	}
	
	public void addShader(Shader s) {
		shaders.add(s);
	}
	
	public void setup(GL2 gl) {
		if (!ready) {
			ready = true;
			for (Shader s : shaders) {
				if (!s.isReady()) ready = ready && s.setup(gl);
			}
		}
		if (ready) {
			id = gl.glCreateProgram();
			for (Shader s : shaders) {
				gl.glAttachShader(id, s.getId());
			}
			gl.glBindAttribLocation(id, 0, "pos");
			gl.glBindAttribLocation(id, 1, "normal");
			gl.glBindAttribLocation(id, 2, "uv");
			gl.glBindAttribLocation(id, 3, "ambient");
			gl.glBindAttribLocation(id, 4, "diffuse");
			gl.glBindAttribLocation(id, 5, "specular");
			gl.glBindAttribLocation(id, 6, "emission");
			gl.glBindAttribLocation(id, 7, "shine");
			gl.glLinkProgram(id);
//            gl.glEnableVertexAttribArray(0);
//            gl.glEnableVertexAttribArray(1);
//            gl.glEnableVertexAttribArray(2);
//            gl.glEnableVertexAttribArray(3);
//            gl.glEnableVertexAttribArray(4);
//            gl.glEnableVertexAttribArray(5);
//            gl.glEnableVertexAttribArray(6);
//            gl.glEnableVertexAttribArray(7);
			gl.glValidateProgram(id);
			IntBuffer status = IntBuffer.allocate(1);
			gl.glGetProgramiv(id, GL2.GL_LINK_STATUS, status);
			if (status.get(0) != 1) {
				gl.glGetProgramiv(id, GL2.GL_INFO_LOG_LENGTH, status);
				int size = status.get(0);
				LOGGER.severe("Program link error: ");
				if (size > 0) {
					ByteBuffer byteBuffer = ByteBuffer.allocate(size);
					gl.glGetProgramInfoLog(id, size, status, byteBuffer);
					LOGGER.severe(new String(byteBuffer.array()));
				} else {
					LOGGER.severe("Error unspecified.");
				}
			} else {
				LOGGER.info("Program verified correctly.");
			}
		}
	}
	
	public void use(GL2 gl) {
		gl.glUseProgram(id);
	}
	
}
