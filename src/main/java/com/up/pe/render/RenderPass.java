package com.up.pe.render;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.up.pe.math.Matrix4D;
import com.up.pe.mesh.Mesh;
import com.up.pe.render.camera.Camera;
import com.up.pe.render.event.RenderInterceptor;
import com.up.pe.render.shader.Program;

/**
 *
 * @author Ricky
 */
public class RenderPass {
	
	private Program prog;
	private Camera camera;
	private RenderInterceptor renderEvents;
	private boolean meshes;

	public RenderPass(Program prog, Camera camera, RenderInterceptor renderEvents) {
		this.prog = prog;
		this.camera = camera;
		this.renderEvents = renderEvents;
		this.meshes = true;
	}

	public RenderPass(Program prog, Camera camera, RenderInterceptor renderEvents, boolean meshes) {
		this.prog = prog;
		this.camera = camera;
		this.renderEvents = renderEvents;
		this.meshes = meshes;
	}

	public RenderPass(Program prog, Camera camera) {
		this.prog = prog;
		this.camera = camera;
		this.renderEvents = null;
		this.meshes = true;
	}
	
	public void render(DisplayManager dm, GL2 gl) {
		prog.use(gl);
		
        //Constants
        gl.glUniform3fv(gl.glGetUniformLocation(prog.getId(), "camera"), 1, camera.getPosition().toFloatArray(), 0);
        gl.glUniformMatrix4fv(gl.glGetUniformLocation(prog.getId(), "projMat"), 1, false, camera.getProjection().toRawGlFMatrix(), 0);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMultMatrixf(camera.getProjection().toRawGlFMatrix(), 0);
		
        //Clean slate
        gl.glClearColor(dm.clear[0], dm.clear[1], dm.clear[2], dm.clear[3]);
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        gl.glUniformMatrix4fv(gl.glGetUniformLocation(prog.getId(), "viewMat"), 1, false, Matrix4D.identity().toRawGlFMatrix(), 0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();

        renderEvents.preTransformation(dm, gl);
		
        //Position and orientation
        gl.glUniformMatrix4fv(gl.glGetUniformLocation(prog.getId(), "viewMat"), 1, false, camera.getTransform().toRawGlFMatrix(), 0);
		gl.glMultMatrixf(camera.getTransform().toRawGlFMatrix(), 0);

        //Then preRender
        renderEvents.preRender(dm, gl);
		
        //Meshes
		if (meshes) {
			prog.use(gl); // In case an event reset it, make sure it's active for meshes at least
		
			gl.glActiveTexture(GL2.GL_TEXTURE0);
			for (Mesh m : dm.getMeshes()) {
				int tex = m.getTexture();
				if (tex != -1) {
					gl.glBindTexture(GL2.GL_TEXTURE_2D, tex);
				} else {
					gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);
				}
				gl.glBindVertexArray(m.getVertexArray());
				gl.glDrawArrays(GL.GL_TRIANGLES, 0, m.getVertexCount());
			}

			gl.glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
			gl.glBindVertexArray(0);
			gl.glUseProgram(0);
		}

        renderEvents.postRender(dm, gl);
        
        gl.glFinish();
	}
	
}
