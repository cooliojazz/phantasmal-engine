package com.up.pe.math;

import com.up.pe.math.Line;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;

/**
 *
 * @author ricky.t
 */
public class Triangle {
    Point3D p1;
    Point3D p2;
    Point3D p3;
    Vector3D n;

    public Triangle(Point3D p1, Point3D p2, Point3D p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        
        this.n = Point3D.normal(p1, p2, p3);
        
//        v0 = p1.vectorTo(p3);
//        v1 = p1.vectorTo(p2);
//
//        dot00 = v0.dotProduct(v0);
//        dot01 = v0.dotProduct(v1);
//        dot11 = v1.dotProduct(v1);
//        
//        invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
    }
    
//    Vector3D v0;
//    Vector3D v1;
//
//    double dot00;
//    double dot01;
//    double dot11;
//    
//    double invDenom;
    
    
    // PL1 = (0 = N1 . (P - P10))
    // N1 . P = N1 . P10, N1x * Px + N1y * Py = N1x * P10x + N1y * P10y
    // PL2 = (0 = N2 . (P - P20))
    // L = O + t * D
    // D = N1 x N2
    // IL = O + t * N1 x N2
    // (V11 - V10) = O + t * N1 x N2
    // t10 = (V11 - V10 - O) / D
    // t11 = (V12 - V11 - O) / D
    // t20 = (V21 - V20 - O) / D
    // t21 = (V22 - V21 - O) / D
    // a2 - b1 > 0 && b2 - a1 > 0
    // t11 - t20 > 0 && t21 - t10
    // V12 + V20 - v11 - V21 > 0
    
//    // t = (P - O) / N1 x N2
//    // https://web.stanford.edu/class/cs277/resources/papers/Moller1997b.pdf
//    public boolean intersects2(Triangle t) {
//        double 
//        if (Math.signum(t.n.dotProduct(p1.toVec())) == Math.signum(t.n.dotProduct(p2.toVec())) == Math.signum(t.n.dotProduct(p3.toVec()))) return false;
//    }
    
    public boolean intersects(Triangle t) {
        return t.intersects(new Line<>(p1, p2)) || t.intersects(new Line<>(p2, p3)) || t.intersects(new Line<>(p3, p1));
    }
            
    public boolean intersects(Line<Point3D> l) {
        double s = intersectsVal(l);
        if (s >= 0 && s <= 1) {
            Point3D px = ((Vector3D)l.getVector()).mul(s).sum(l.getPoint0());

            // Magic (Barycentric stuff?)
            Vector3D v0 = (Vector3D)p1.vectorTo(p3);
            Vector3D v1 = (Vector3D)p1.vectorTo(p2);
            Vector3D v2 = (Vector3D)p1.vectorTo(px);

            double dot00 = v0.dotProduct(v0);
            double dot01 = v0.dotProduct(v1);
            double dot11 = v1.dotProduct(v1);
            double dot02 = v0.dotProduct(v2);
            double dot12 = v1.dotProduct(v2);

            double invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
            double u = (dot11 * dot02 - dot01 * dot12) * invDenom;
            double v = (dot00 * dot12 - dot01 * dot02) * invDenom;

            if (u >= 0 && v >= 0 && u + v <= 1) {
                return true;
            }
        }
        return false;
    }
    
    public double intersectsVal(Line<Point3D> l) {
        Vector3D v1 = l.getPoint0().vectorTo(p1);
        Vector3D v2 = (Vector3D)l.getVector();
        return n.dotProduct(v1) / n.dotProduct(v2);
//        return (n.getX() * v1.getX() + n.getY() * v1.getY() + n.getZ() * v1.getZ()) / (n.getX() * v2.getX() + n.getY() * v2.getY() + n.getZ() * v2.getZ());
    }
    
    public Point3D intersectsAt(Line<Point3D> l) {
        double s = intersectsVal(l);
        if (s >= 0 && s <= 1) {
            return ((Vector3D)l.getVector()).mul(s).sum(l.getPoint0());
        }
        return null;
    }
    
    public Triangle move(Point3D vec) {
        return new Triangle(p1.sum(vec), p2.sum(vec), p3.sum(vec));
    }
    
    public Triangle scale(Vector3D vec) {
        return new Triangle(p1.toVec().product(vec), p2.toVec().product(vec), p3.toVec().product(vec));
    }
}
