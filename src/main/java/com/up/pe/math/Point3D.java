package com.up.pe.math;

import java.text.ParseException;

/**
 * Represents a point in 3D space with an x, y, and z coordinates.
 * @author Ricky Talbot
 */
public class Point3D implements Cloneable, Point {
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private float[] fs = new float[] {0f, 0f, 0f};
    
    public static Point3D zeros() {
        return new Point3D(0, 0, 0);
    }
    
    /**
     * Constructs a new Point3D at the given (x, y, z) coordinates.
     * @param x
     * @param y
     * @param z
     */
    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        fs = new float[] {(float)x, (float)y, (float)z};
    }
    
    /**
     * Gets the x-coordinate of this point
     * @return the x-coordinate
     */
    public double getX() {
        return x;
    }
    
    /**
     * Gets the y-coordinate of this point
     * @return the y-coordinate
     */
    public double getY() {
        return y;
    }
    
    /**
     * Gets the y-coordinate of this point.
     * @return the y-coordinate
     */
    public double getZ() {
        return z;
    }
    
    /**
     * Sets the x-coordinate of this point.
     * @param x the x-coordinate
     */
    public void setX(double x) {
        this.x = x;
        fs[0] = (float)x;
    }
    
    /**
     * Sets the y-coordinate of this point.
     * @param y the y-coordinate
     */
    public void setY(double y) {
        this.y = y;
        fs[1] = (float)y;
    }
    
    /**
     * Sets the z-coordinate of this point.
     * @param z the z-coordinate
     */
    public void setZ(double z) {
        this.z = z;
        fs[2] = (float)z;
    }
    
    /**
     * 
     * @param x the value to add to the x
     * @return a new Point3D with the added X value
     */
    public Point3D addX(double x) {
        return new Point3D(this.x + x, y, z);
    }
    
    /**
     * 
     * @param y the value to add to the y
     * @return
     */
    public Point3D addY(double y) {
        return new Point3D(x, this.y + y, z);
    }
    
    /**
     * 
     * @param z the value to add to the z
     * @return
     */
    public Point3D addZ(double z) {
        return new Point3D(x, y, this.z + z);
    }
    
    /**
     * 
     * @param x the value to multiply with x
     * @return a new Point3D with the added X value
     */
    public Point3D mulX(double x) {
        return new Point3D(this.x * x, y, z);
    }
    
    /**
     * 
     * @param y the value to multiply with y
     * @return
     */
    public Point3D mulY(double y) {
        return new Point3D(x, this.y * y, z);
    }
    
    /**
     * 
     * @param z the value to multiply with z
     * @return
     */
    public Point3D mulZ(double z) {
        return new Point3D(x, y, this.z * z);
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector3D between the points
     */
    @Override
    public Vector vectorTo(Point p2) {
        if (p2 instanceof Point3D)
            return new Vector3D(((Point3D)p2).getX() - x, ((Point3D)p2).getY() - y, ((Point3D)p2).getZ() - z);
        else
            return null;
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector3D between the points
     */
    public Vector3D vectorTo(Point3D p2) {
        return new Vector3D(p2.getX() - x, p2.getY() - y, p2.getZ() - z);
    }
    
    /**
     * Returns the point halfway between this point and the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Point3D midway between the points
     */
    public Point3D midPoint(Point3D p2) {
        return new Point3D((p2.getX() - x) / 2 + x, (p2.getY() - y) / 2 + y, (p2.getZ() - z) / 2 + z);
    }
    
    public Point3D sum(Point3D p2) {
        return new Point3D(getX() + p2.getX(), getY() + p2.getY(), getZ() + p2.getZ());
    }
    
    public Point3D sum(double d) {
        return new Point3D(getX() + d, getY() + d, getZ() + d);
    }
    
    public Point3D difference(Point3D p2) {
        return new Point3D(getX() - p2.getX(), getY() - p2.getY(), getZ() - p2.getZ());
    }
    
    public Point3D difference(double d) {
        return new Point3D(getX() - d, getY() - d, getZ() - d);
    }
    
    public Point3D mul(double d) {
        return new Point3D(getX() * d, getY() * d, getZ() * d);
    }
    
    public float[] toFloatArray() {
        return fs;
    }
    
    public void writeToArray(float[] floats, int start) {
        floats[start] = fs[0];
        floats[start + 1] = fs[1];
        floats[start + 2] = fs[2];
    }
    
    public Point3D min(Point3D p) {
        return new Point3D(Math.min(x, p.x), Math.min(y, p.y), Math.min(z, p.z));
    }
    
    public Point3D max(Point3D p) {
        return new Point3D(Math.max(x, p.x), Math.max(y, p.y), Math.max(z, p.z));
    }
    
    /**
     * Rotates this point around the origin by <tt>a</tt> and returns a new Point3D representing the rotated point
     * @param a the amount of degrees to rotate
     * @return The rotated Point3D
     */
    public Point3D rotate(Angle3D a) {
        return a.doRotation(this);
    }
    
    public Vector3D toVec() {
        return new Vector3D(this);
    }
    
    public Point3D average(Point3D p) {
        return new Point3D((p.getX() + x) / 2, (p.getY() + y) / 2, (p.getZ() + z) / 2);
    }
    
    @Override
    public Point3D clone() {
        return new Point3D(x, y, z);
    }
    
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ")";
    }
    
    static public Point3D fromString(String s) {
        try {
            String[] args = s.replace(" ", "").split(",");
            if (args.length < 3) throw new ParseException("", 0);
            return new Point3D(Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Point3D) {
            Point3D p = (Point3D)o;
            return p.getX() == x && p.getY() == y && p.getZ() == z;
        }
        return false;
    }
    
    @Deprecated
    public boolean equalish(Object o) {
        if (o instanceof Point3D) {
            Point3D p = (Point3D)o;
            return deq(p.getX(), x) && deq(p.getY(), y) && deq(p.getZ(), z);
        }
        return false;
    }
    
    public boolean deq(double a, double b) {
        return Math.abs(a - b) <= Math.abs(a) * 0.00001;
    }
    
    public static Vector3D normal(Point3D p1, Point3D p2, Point3D p3) {
        return p1.vectorTo(p2).crossProduct(p2.vectorTo(p3)).normalize();
    }
    
}
