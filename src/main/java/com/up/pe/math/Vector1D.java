package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public class Vector1D extends Point1D implements Vector {
    
    public static Vector1D zero = new Vector1D(0);
    public static Vector1D one = new Vector1D(1);

    public Vector1D(double x) {
        super(x);
    }

    public Vector1D(Point1D p) {
        super(p.getX());
    }
    
    public Vector1D diff(Vector1D v2) {
        return new Vector1D(getX() - v2.getX());
    }
    
    public Vector1D sum(Vector1D v2) {
        return new Vector1D(getX() + v2.getX());
    }
    
    public Vector1D product(Vector1D v2) {
        return new Vector1D(getX() * v2.getX());
    }
    
    public Vector1D quotient(Vector1D v2) {
        return new Vector1D(getX() / v2.getX());
    }
    
    public Point1D getPoint1D() {
        return super.clone();
    }

    @Override
    public Vector1D mul(double d) {
        return new Vector1D(getX() * d);
    }
    
    @Override
    public Vector1D sum(double d) {
        return new Vector1D(getX() + d);
    }
    
    public Vector1D abs() {
        return new Vector1D(Math.abs(getX()));
    }
    
    public Vector1D deInfinity() {
        if (Double.isInfinite(getX()) || Double.isNaN(getX())) {
            setX(0);
        }
        if (getX() != 0) {
            System.out.println("No! Bad!" + this);
        }
        return this;
    }
    
    @Override
    public Vector1D clone() {
        return new Vector1D(getX());
    }

	// No 1D Matrix
	@Override
	public Matrix toMatrix() {
		return null;
	}
    
}
