package com.up.pe.math;

import com.jogamp.opengl.math.Matrix4f;

/**
 *
 * @author Ricky
 */
public class Matrix3D implements Cloneable, Matrix {
	
	// Indexed as [y, x]
	private double[][] matrix = new double[3][3];

	public Matrix3D() {
	}
	
	public Matrix3D(double[][] matrix) {
		this.matrix = matrix;
	}

	public Matrix3D transpose() {
		return new Matrix3D(new double[][] {
			new double[] {matrix[0][0], matrix[1][0], matrix[2][0]},
			new double[] {matrix[0][1], matrix[1][1], matrix[2][1]},
			new double[] {matrix[0][2], matrix[1][2], matrix[2][2]}});
	}

	public Matrix3D multiply(double s) {
		return new Matrix3D(new double[][] {
			new double[] {matrix[0][0] * s, matrix[0][1] * s, matrix[0][2] * s},
			new double[] {matrix[1][0] * s, matrix[1][1] * s, matrix[1][2] * s},
			new double[] {matrix[2][0] * s, matrix[2][1] * s, matrix[2][2] * s}});
	}

	public Matrix3D sum(Matrix3D m) {
		return new Matrix3D(new double[][] {
			new double[] {matrix[0][0] + m.matrix[0][0], matrix[0][1] + m.matrix[0][1], matrix[0][2] + m.matrix[0][2]},
			new double[] {matrix[1][0] + m.matrix[1][0], matrix[1][1] + m.matrix[1][1], matrix[1][2] + m.matrix[1][2]},
			new double[] {matrix[2][0] + m.matrix[2][0], matrix[2][1] + m.matrix[2][1], matrix[2][2] + m.matrix[2][2]}});
	}

	public Matrix3D product(Matrix3D m) {
		return new Matrix3D(new double[][] {
			new double[] {matrix[0][0] * m.matrix[0][0], matrix[0][1] * m.matrix[0][1], matrix[0][2] * m.matrix[0][2]},
			new double[] {matrix[1][0] * m.matrix[1][0], matrix[1][1] * m.matrix[1][1], matrix[1][2] * m.matrix[1][2]},
			new double[] {matrix[2][0] * m.matrix[2][0], matrix[2][1] * m.matrix[2][1], matrix[2][2] * m.matrix[2][2]}});
	}

	public Matrix3D scale(Vector3D v) {
		return Matrix3D.this.multiply(new Matrix3D(new double[][] {
			new double[] {v.getX(), 0, 0},
			new double[] {0, v.getY(), 0},
			new double[] {0, 0, v.getZ()}}));
	}

//	public Matrix3D setScale(Vector3D v) {
//		return Matrix3D.this.product(new Matrix3D(new double[][] {
//			new double[] {v.getX(), 1, 1},
//			new double[] {1, v.getY(), 1},
//			new double[] {1, 1, v.getZ()}}));
//	}

	public Matrix3D multiply(Matrix3D m) {
		return new Matrix3D(new double[][] {
			new double[] {matrix[0][0] * m.matrix[0][0] + matrix[0][1] * m.matrix[1][0] + matrix[0][2] * m.matrix[2][0], matrix[0][0] * m.matrix[0][1] + matrix[0][1] * m.matrix[1][1] + matrix[0][2] * m.matrix[2][1], matrix[0][0] * m.matrix[0][2] + matrix[0][1] * m.matrix[1][2] + matrix[0][2] * m.matrix[2][2]},
			new double[] {matrix[1][0] * m.matrix[0][0] + matrix[1][1] * m.matrix[1][0] + matrix[1][2] * m.matrix[2][0], matrix[1][0] * m.matrix[0][1] + matrix[1][1] * m.matrix[1][1] + matrix[1][2] * m.matrix[2][1], matrix[1][0] * m.matrix[0][2] + matrix[1][1] * m.matrix[1][2] + matrix[1][2] * m.matrix[2][2]},
			new double[] {matrix[2][0] * m.matrix[0][0] + matrix[2][1] * m.matrix[1][0] + matrix[2][2] * m.matrix[2][0], matrix[2][0] * m.matrix[0][1] + matrix[2][1] * m.matrix[1][1] + matrix[2][2] * m.matrix[2][1], matrix[2][0] * m.matrix[0][2] + matrix[2][1] * m.matrix[1][2] + matrix[2][2] * m.matrix[2][2]}});
	}

	public Vector3D apply(Vector3D v) {
		return new Vector3D(matrix[0][0] * v.getX() + matrix[0][1] * v.getY() + matrix[0][2] * v.getZ(), matrix[1][0] * v.getX() + matrix[1][1] * v.getY() + matrix[1][2] * v.getZ(), matrix[2][0] * v.getX() + matrix[2][1] * v.getY() + matrix[2][2] * v.getZ());
	}

	public Vector2D apply(Vector2D v) {
		return new Vector2D(matrix[0][0] * v.getX() + matrix[0][1] * v.getY() + matrix[0][2], matrix[1][0] * v.getX() + matrix[1][1] * v.getY() + matrix[1][2]);
	}
	
	public double det() {
		return matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]) +
				matrix[0][1] * (matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2]) +
				matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
	}
	
	public double trace() {
		return matrix[0][0] + matrix[1][1] + matrix[2][2];
	}
	
	public Matrix3D invert() {
		// Cache det values for inverse
		double a = matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1];
		double b = matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2];
		double c = matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0];
		return new Matrix3D(new double[][] {
				{a, b, c},
				{matrix[0][2] * matrix[1][2] - matrix[0][1] * matrix[2][2], matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0], matrix[0][1] * matrix[2][0] - matrix[0][0] * matrix[2][1]},
				{matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1], matrix[0][2] * matrix[1][0] - matrix[0][0] * matrix[1][2], matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]},
			}).multiply(1 / matrix[0][0] * a + matrix[0][1] * b + matrix[0][2] * c);
	}
	
	public Angle3D getRotation() {
		if (matrix[2][0] == 1) {
			return new Angle3D(Math.atan2(-matrix[0][1], -matrix[0][2]), -Math.PI / 2, 0);
		} else if (matrix[2][0] == -1) {
			return new Angle3D(Math.atan2(matrix[0][1], matrix[0][2]), Math.PI / 2, 0);
		} else {
			double y = Math.asin(-matrix[2][0]);
			return new Angle3D(Math.atan2(matrix[2][1] / Math.cos(y), matrix[2][2] / Math.cos(y)), y, Math.atan2(matrix[1][0] / Math.cos(y), matrix[0][0] / Math.cos(y)));
		}
	}
	
	public Matrix4D toTransformationMatrix() {
		return new Matrix4D(new double[][] {new double[] {matrix[0][0], matrix[0][1], matrix[0][2], 0},
											new double[] {matrix[1][0], matrix[1][1], matrix[1][2], 0},
											new double[] {matrix[2][0], matrix[2][1], matrix[2][2], 0},
											new double[] {0, 0, 0, 1}});
	}
	
	public Matrix4D toTransformationMatrix(Point3D offset) {
		return new Matrix4D(new double[][] {new double[] {matrix[0][0], matrix[0][1], matrix[0][2], offset.getX()},
											new double[] {matrix[1][0], matrix[1][1], matrix[1][2], offset.getY()},
											new double[] {matrix[2][0], matrix[2][1], matrix[2][2], offset.getZ()},
											new double[] {0, 0, 0, 1}});
	}
	
	// GL prefers column-major by default
	public float[] toRawGlFMatrix() {
		return new float[] {(float)matrix[0][0], (float)matrix[1][0], (float)matrix[2][0], (float)matrix[0][1], (float)matrix[1][1], (float)matrix[2][1], (float)matrix[0][2], (float)matrix[1][2], (float)matrix[2][2]};
	}
	
	public double[] toRawGlDMatrix() {
		return new double[] {matrix[0][0], matrix[1][0], matrix[2][0], matrix[0][1], matrix[1][1], matrix[2][1], matrix[0][2], matrix[1][2], matrix[2][2]};
	}

	public static Matrix3D identity() {
		return new Matrix3D(new double[][] {new double[] {1, 0, 0}, new double[] {0, 1, 0}, new double[] {0, 0, 1}});
	}
	
}
