package com.up.pe.math;

import com.jogamp.opengl.GL2;

/**
 *
 * @author Ricky
 */
public class Vector3D extends Point3D implements Vector {
    
    public static Vector3D zeros() {
        return new Vector3D(0, 0, 0);
    }
	
    public static Vector3D ones() {
        return new Vector3D(1, 1, 1);
    }

    public Vector3D(double x, double y, double z) {
        super(x, y, z);
    }

    public Vector3D(double v) {
        super(v, v, v);
    }

    public Vector3D(Point3D p) {
        super(p.getX(), p.getY(), p.getZ());
    }
    
    public Vector3D crossProduct(Vector3D v2) {
        return new Vector3D(getY() * v2.getZ() - v2.getY() * getZ(), getZ() * v2.getX() - v2.getZ() * getX(), getX() * v2.getY() - v2.getX() * getY());
    }
    
    public Vector3D diff(Vector3D v2) {
        return new Vector3D(getX() - v2.getX(), getY() - v2.getY(), getZ() - v2.getZ());
    }
    
    public Vector3D sum(Vector3D v2) {
        return new Vector3D(getX() + v2.getX(), getY() + v2.getY(), getZ() + v2.getZ());
    }
    
    public double sum() {
        return getX() + getY() + getZ();
    }
    
    public double dotProduct(Vector3D v2) {
        Vector3D v = product(v2);
        return v.getX() + v.getY() + v.getZ();
    }
    
    public Vector3D product(Vector3D v2) {
        return new Vector3D(getX() * v2.getX(), getY() * v2.getY(), getZ() * v2.getZ());
    }
    
    public Vector3D quotient(Vector3D v2) {
        return new Vector3D(getX() / v2.getX(), getY() / v2.getY(), getZ() / v2.getZ());
    }
    
    public Point3D getPoint3D() {
        return super.clone();
    }
    
	//TODO: Should probably make this non-permutating, but need to souble check all usages as that's a very breaking change.
    public Vector3D normalize() {
        double mag = magnitude();
        if (getX() != 0) setX(getX() / mag);
        if (getY() != 0) setY(getY() / mag);
        if (getZ() != 0) setZ(getZ() / mag);
        return this;
    }
    
    public double magnitude() {
        return Math.sqrt(getX() * getX() + getY() * getY() + getZ() * getZ());
    }

    @Override
    public Vector3D mul(double d) {
        return new Vector3D(getX() * d, getY() * d, getZ() * d);
    }
    
    @Override
    public Vector3D sum(double d) {
        return new Vector3D(getX() + d, getY() + d, getZ() + d);
    }
    
    @Override
    public Vector3D rotate(Angle3D a) {
        return new Vector3D(super.rotate(a));
    }
    
    public Vector3D abs() {
        return new Vector3D(Math.abs(getX()), Math.abs(getY()), Math.abs(getZ()));
    }
    
    @Override
    public Matrix3D toMatrix() {
        return new Matrix3D(new double[][] {new double[] {getX(), 0, 0}, new double[] {getY(), 0, 0}, new double[] {getZ(), 0, 0}});
    }
	
    public Matrix3D crossMatrix() {
        return new Matrix3D(new double[][] {new double[] {0, -getZ(), getY()}, new double[] {getZ(), 0, -getX()}, new double[] {-getY(), getX(), 0}});
    }
    
    public Vector3D deInfinity() {
        if (Double.isInfinite(getX()) || Double.isNaN(getX())) {
            setX(0);
        }
        if (Double.isInfinite(getY()) || Double.isNaN(getY())) {
            setY(0);
        }
        if (Double.isInfinite(getZ()) || Double.isNaN(getZ())) {
            setZ(0);
        }
        if (getX() != 0 || getY() != 0 || getZ() != 0) {
            System.out.println("No! Bad!" + this);
        }
        return this;
    }
    
    @Override
    public String toString() {
        return "<" + getX() + ", " + getY() + ", " + getZ() + ">";
    }
    
    @Override
    public Vector3D clone() {
        return new Vector3D(getX(), getY(), getZ());
    }
    
    /**
     * Draw the Vector, e.g. for debugging
     */
    public void render(GL2 gl, Point3D p) {
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(p.getX(), p.getY(), p.getZ());
        Vector3D v = normalize().mul(5).sum(p).toVec();
//        Point3D v = multiply(10).sum(p);
        gl.glVertex3d(v.getX(), v.getY(), v.getZ());
        gl.glEnd();
    }
    
}
