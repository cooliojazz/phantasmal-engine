package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public enum Direction3D {
    POSITIVE_X(new Vector3D(1, 0, 0), new Angle3D(0, 0, -90)),
    NEGATIVE_X(new Vector3D(-1, 0, 0), new Angle3D(0, 0, 90)),
    POSITIVE_Y(new Vector3D(0, 1, 0), new Angle3D(0, 0, 0)),
    NEGATIVE_Y(new Vector3D(0, -1, 0), new Angle3D(180, 0, 0)),
    POSITIVE_Z(new Vector3D(0, 0, 1), new Angle3D(90, 0, 0)),
    NEGATIVE_Z(new Vector3D(0, 0, -1), new Angle3D(-90, 0, 0));

    Vector3D dir;
    Angle3D rot;

    private Direction3D(Vector3D dir, Angle3D rot) {
        this.dir = dir;
        this.rot = rot;
    }
    
    public Vector3D getVector() {
        return dir;
    }

    public Angle3D getRotation() {
        return rot;
    }
}
