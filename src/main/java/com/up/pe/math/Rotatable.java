package com.up.pe.math;

import java.util.function.Supplier;

/**
 *
 * @author Ricky
 */
public interface Rotatable /*extends Supplier<Angle3D>*/ {
    
    public Angle3D getRotation();
    public void setRotation(Angle3D rotation);

//    @Override
//    public default Angle3D get() {
//        return getRotation();
//    }
    
}
