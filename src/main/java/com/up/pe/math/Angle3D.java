package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public class Angle3D extends Point3D {
    
//    public static Angle3D zeros = new Angle3D(0, 0, 0);
    public static Angle3D zeros() {return new Angle3D(0, 0, 0);};

    /**
     * Constructs a new Angle3D with the given (x, y, z) rotation specified in radians.
     * @param x
     * @param y
     * @param z 
     */
    public Angle3D(double x, double y, double z) {
        super(x, y, z);
        updatePreCalc();
    }
    
    /**
     * Constructs a new Angle3D from the given Point3D p.
     * @param p 
     */
    public Angle3D(Point3D p) {
        super(p.getX(), p.getY(), p.getZ());
        updatePreCalc();
    }

    /**
     * Constructs a new Angle3D with the given (x, y, z) rotation specified in degrees.
     * @param x
     * @param y
     * @param z 
     */
    public Angle3D(int x, int y, int z) {
        super(x * Math.PI / 180, y * Math.PI / 180, z * Math.PI / 180);
        updatePreCalc();
    }

    @Override
    public void setX(double x) {
        super.setX(x);
        updatePreCalc();
    }

    @Override
    public void setY(double y) {
        super.setY(y);
        updatePreCalc();
    }

    @Override
    public void setZ(double z) {
        super.setZ(z);
        updatePreCalc();
    }
    

    /**
     * Gets the value of the x rotation in degrees instead of the default radians
     * @return Degrees representation of x
     */
    public double getXDeg() {
        return getX() * 180 / Math.PI;
    }

    /**
     * Gets the value of the y rotation in degrees instead of the default radians
     * @return Degrees representation of y
     */
    public double getYDeg() {
        return getY() * 180 / Math.PI;
    }

    /**
     * Gets the value of the z rotation in degrees instead of the default radians
     * @return Degrees representation of z
     */
    public double getZDeg() {
        return getZ() * 180 / Math.PI;
    }
    
    /**
     * 
     * @param x the value to add to the x
     * @return a new Angle3D with the added X value
     */
    @Override
    public Angle3D addX(double x) {
        return new Angle3D(getX() + x, getY(), getZ());
    }
    
    /**
     * 
     * @param y the value to add to the y
     * @return a new Angle3D with the added Y value
     */
    @Override
    public Angle3D addY(double y) {
        return new Angle3D(getX(), getY() + y, getZ());
    }
    
    /**
     * 
     * @param z the value to add to the z
     * @return a new Angle3D with the added Z value
     */
    @Override
    public Angle3D addZ(double z) {
        return new Angle3D(getX(), getY(), getZ() + z);
    }

    @Override
    public Angle3D sum(double d) {
        return new Angle3D(super.sum(d));
    }
    
    @Override
    public Angle3D sum(Point3D p2) {
        return new Angle3D(getX() + p2.getX(), getY() + p2.getY(), getZ() + p2.getZ());
    }
    
    public Angle3D mul(double d) {
        return new Angle3D(getX() * d, getY() * d, getZ() * d);
    }
    
    @Override
    public Angle3D clone() {
        return new Angle3D(this);
    }
    
    
    
    private void updatePreCalc() {
        precalcrot = new double[][] {
            new double[] {1,                 Math.cos(getX()), -Math.sin(getX()), Math.sin(getX()), Math.cos(getX())},
            new double[] {Math.cos(getY()),  Math.sin(getY()),  1,               -Math.sin(getY()), Math.cos(getY())},
            new double[] {Math.cos(getZ()), -Math.sin(getZ()),  Math.sin(getZ()), Math.cos(getZ()), 1}
        };
    }
    
    double[][] precalcrot;
    
    public Point3D doRotation(Point3D p) {
//        //Pre-reduction reference
//        Point3D rotated = new Point3D(p.getX(), p.getY() * precalcrot[0][1] + p.getZ() * precalcrot[0][2], p.getY() * precalcrot[0][3] + p.getZ() * precalcrot[0][4]);
//        rotated = new Point3D(rotated.getX() * precalcrot[1][0] + rotated.getZ() * precalcrot[1][1], rotated.getY(), rotated.getX() * precalcrot[1][3] + rotated.getZ() * precalcrot[1][4]);
//        return new Point3D(rotated.getX() * precalcrot[2][0] + rotated.getY() * precalcrot[2][1], rotated.getX() * precalcrot[2][2] + rotated.getY() * precalcrot[2][3], rotated.getZ());

        double x, y, z;
        y = p.getY() * precalcrot[0][1] + p.getZ() * precalcrot[0][2];
        z = p.getY() * precalcrot[0][3] + p.getZ() * precalcrot[0][4];
        x = p.getX() * precalcrot[1][0] + z * precalcrot[1][1];
        return new Point3D(x * precalcrot[2][0] + y * precalcrot[2][1], x * precalcrot[2][2] + y * precalcrot[2][3], p.getX() * precalcrot[1][3] + z * precalcrot[1][4]); 
    }
    
    public Angle3D combine(Angle3D a) {
        // Slower than a dedicated method, but works for now
		return toMatrix().multiply(a.toMatrix()).getRotation();
    }
	
	public Matrix3D toMatrix() {
		double a = Math.sin(getX()) * Math.sin(getY());
		double b = Math.cos(getX()) * Math.sin(getY());
		return new Matrix3D(new double[][] {
			new double[] {Math.cos(getY()) * Math.cos(getZ()), a * Math.cos(getZ()) - Math.cos(getX()) * Math.sin(getZ()), b * Math.cos(getZ()) + Math.sin(getX()) * Math.sin(getZ())},
			new double[] {Math.cos(getY()) * Math.sin(getZ()), a * Math.sin(getZ()) + Math.cos(getX()) * Math.cos(getZ()), b * Math.sin(getZ()) - Math.sin(getX()) * Math.cos(getZ())},
			new double[] {-Math.sin(getY()), Math.sin(getX()) * Math.cos(getY()), Math.cos(getX()) * Math.cos(getY())}});
	}
}
