package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public interface Vector {
    public Matrix toMatrix();
}
