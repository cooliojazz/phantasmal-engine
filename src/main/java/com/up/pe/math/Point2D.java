package com.up.pe.math;

/**
 * Represents a point in 2D space with an x and a y coordinate.
 * @author Ricky Talbot
 */
public class Point2D implements Cloneable, Point {
    private double x = 0;
    private double y = 0;
    
    
    public static Point2D zeros() {
        return new Point2D(0, 0);
    }
    
    /**
     * Constructs a new Point3D at the given (x, y) coordinate.
     * @param x
     * @param y
     */
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Gets the x-coordinate of this point
     * @return the x-coordinate
     */
    public double getX() {
        return x;
    }
    
    /**
     * Gets the y-coordinate of this point
     * @return the y-coordinate
     */
    public double getY() {
        return y;
    }
    
    /**
     * Sets the x-coordinate of this point.
     * @param x the x-coordinate
     */
    public void setX(double x) {
        this.x = x;
    }
    
    /**
     * Sets the y-coordinate of this point.
     * @param y the y-coordinate
     */
    public void setY(double y) {
        this.y = y;
    }
    
    /**
     * 
     * @param x
     * @return
     */
    public Point2D addX(double x) {
        return new Point2D(this.x + x, y);
    }
    
    /**
     * 
     * @param y
     * @return
     */
    public Point2D addY(double y) {
        return new Point2D(x, this.y + y);
    }
    
    /**
     * Returns the point halfway between this point and the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Point2D midway between the points
     */
    public Point2D midPoint(Point2D p2) {
        return new Point2D((p2.getX() - x) / 2 + x, (p2.getY() - y) / 2 + y);
    }

    public Point2D mul(double d) {
        return new Point2D(getX() * d, getY() * d);
    }

    public Point2D mul(Point2D p) {
        return new Point2D(x * p.x, y * p.y);
    }
    
    public Point2D sum(Point2D p2) {
        return new Point2D(getX() + p2.getX(), getY() + p2.getY());
    }
    
    public Point2D sum(double d) {
        return new Point2D(getX() + d, getY() + d);
    }
    
    public Point2D difference(Point2D p2) {
        return new Point2D(getX() - p2.getX(), getY() - p2.getY());
    }
    
    public Point2D difference(double d) {
        return new Point2D(getX() - d, getY() - d);
    }
    
    public Point2D rotate(Angle1D a) {
        return a.doRotation(this);
    }
    
    public Vector2D toVec() {
        return new Vector2D(this);
    }
    
    @Override
    public Point2D clone() {
        return new Point2D(x, y);
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector2D between the points
     */
    @Override
    public Vector vectorTo(Point p2) {
        if (p2 instanceof Point2D)
            return new Vector2D(((Point2D)p2).getX() - x, ((Point2D)p2).getY() - y);
        else
            return null;
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector2D between the points
     */
    public Vector2D vectorTo(Point2D p2) {
        return new Vector2D(p2.getX() - x, p2.getY() - y);
    }

    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }
}
