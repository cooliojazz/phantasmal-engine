package com.up.pe.math;

import java.text.ParseException;

/**
 * Represents a point in 4D space with an x, y, z and w coordinates.
 * @author Ricky Talbot
 */
public class Point4D implements Cloneable, Point {
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private double w = 0;
    private float[] fs = new float[] {0f, 0f, 0f, 0f};
    
    public static Point4D zeros() {
        return new Point4D(0, 0, 0, 0);
    }
    
    /**
     * Constructs a new Point4D at the given (x, y, z, w) coordinates.
     * @param x
     * @param y
     * @param z
     * @param w
     */
    public Point4D(double x, double y, double z, double w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        fs = new float[] {(float)x, (float)y, (float)z, (float)w};
    }
    
    /**
     * Gets the x-coordinate of this point
     * @return the x-coordinate
     */
    public double getX() {
        return x;
    }
    
    /**
     * Gets the y-coordinate of this point
     * @return the y-coordinate
     */
    public double getY() {
        return y;
    }
    
    /**
     * Gets the y-coordinate of this point.
     * @return the y-coordinate
     */
    public double getZ() {
        return z;
    }
    
    /**
     * Gets the w-coordinate of this point.
     * @return the w-coordinate
     */
    public double getW() {
        return w;
    }
    
    /**
     * Sets the x-coordinate of this point.
     * @param x the x-coordinate
     */
    public void setX(double x) {
        this.x = x;
        fs[0] = (float)x;
    }
    
    /**
     * Sets the y-coordinate of this point.
     * @param y the y-coordinate
     */
    public void setY(double y) {
        this.y = y;
        fs[1] = (float)y;
    }
    
    /**
     * Sets the z-coordinate of this point.
     * @param z the z-coordinate
     */
    public void setZ(double z) {
        this.z = z;
        fs[2] = (float)z;
    }
    
    /**
     * Sets the w-coordinate of this point.
     * @param w the w-coordinate
     */
    public void setW(double w) {
        this.w = w;
        fs[3] = (float)w;
    }
    
    /**
     * 
     * @param x the value to add to the x
     * @return a new Point4D with the added X value
     */
    public Point4D addX(double x) {
        return new Point4D(this.x + x, y, z, w);
    }
    
    /**
     * 
     * @param y the value to add to the y
     * @return
     */
    public Point4D addY(double y) {
        return new Point4D(x, this.y + y, z, w);
    }
    
    /**
     * 
     * @param z the value to add to the z
     * @return
     */
    public Point4D addZ(double z) {
        return new Point4D(x, y, this.z + z, w);
    }
    
    /**
     * 
     * @param w the value to add to the w
     * @return
     */
    public Point4D addW(double w) {
        return new Point4D(x, y, z, this.w + w);
    }
    
    /**
     * 
     * @param x the value to multiply with x
     * @return a new Point4D with the added X value
     */
    public Point4D mulX(double x) {
        return new Point4D(this.x * x, y, z, w);
    }
    
    /**
     * 
     * @param y the value to multiply with y
     * @return
     */
    public Point4D mulY(double y) {
        return new Point4D(x, this.y * y, z, w);
    }
    
    /**
     * 
     * @param z the value to multiply with z
     * @return
     */
    public Point4D mulZ(double z) {
        return new Point4D(x, y, this.z * z, w);
    }
    
    /**
     * 
     * @param w the value to multiply with w
     * @return
     */
    public Point4D mulW(double w) {
        return new Point4D(x, y, w, this.w * w);
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector4D between the points
     */
    @Override
    public Vector vectorTo(Point p2) {
        if (p2 instanceof Point4D)
            return new Vector4D(((Point4D)p2).getX() - x, ((Point4D)p2).getY() - y, ((Point4D)p2).getZ() - z, ((Point4D)p2).getW() - w);
        else
            return null;
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector4D between the points
     */
    public Vector4D vectorTo(Point4D p2) {
        return new Vector4D(p2.getX() - x, p2.getY() - y, p2.getZ() - z, p2.getW() - w);
    }
    
    /**
     * Returns the point halfway between this point and the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Point4D midway between the points
     */
    public Point4D midPoint(Point4D p2) {
        return new Point4D((p2.getX() - x) / 2 + x, (p2.getY() - y) / 2 + y, (p2.getZ() - z) / 2 + z, (p2.getW() - w) / 2 + w);
    }
    
    public Point4D sum(Point4D p2) {
        return new Point4D(getX() + p2.getX(), getY() + p2.getY(), getZ() + p2.getZ(), getZ() + p2.getZ());
    }
    
    public Point4D sum(double d) {
        return new Point4D(getX() + d, getY() + d, getZ() + d, getW() + d);
    }
    
    public Point4D difference(Point4D p2) {
        return new Point4D(getX() - p2.getX(), getY() - p2.getY(), getZ() - p2.getZ(), getW() - p2.getW());
    }
    
    public Point4D difference(double d) {
        return new Point4D(getX() - d, getY() - d, getZ() - d, getW() - d);
    }
    
    public Point4D mul(double d) {
        return new Point4D(getX() * d, getY() * d, getZ() * d, getW() * d);
    }
    
    public float[] toFloatArray() {
        return fs;
    }
    
    public void writeToArray(float[] floats, int start) {
        floats[start] = fs[0];
        floats[start + 1] = fs[1];
        floats[start + 2] = fs[2];
        floats[start + 3] = fs[3];
    }
    
    public Point4D min(Point4D p) {
        return new Point4D(Math.min(x, p.x), Math.min(y, p.y), Math.min(z, p.z), Math.min(w, p.w));
    }
    
    public Point4D max(Point4D p) {
        return new Point4D(Math.max(x, p.x), Math.max(y, p.y), Math.max(z, p.z), Math.max(w, p.w));
    }
    
//    /**
//     * Rotates this point around the origin by <tt>a</tt> and returns a new Point3D representing the rotated point
//     * @param a the amount of degrees to rotate
//     * @return The rotated Point3D
//     */
//    public Point4D rotate(Angle4D a) {
//        return a.doRotation(this);
//    }
    
    public Vector4D toVec() {
        return new Vector4D(this);
    }
    
    public Point4D average(Point4D p) {
        return new Point4D((p.getX() + x) / 2, (p.getY() + y) / 2, (p.getZ() + z) / 2, (p.getW() + w) / 2);
    }
    
    @Override
    public Point4D clone() {
        return new Point4D(x, y, z, w);
    }
    
    @Override
    public String toString() {
        return "(" + x + ", " + y + ", " + z + ", " + w + ")";
    }
    
    static public Point4D fromString(String s) {
        try {
            String[] args = s.replace(" ", "").split(",");
            if (args.length < 4) throw new ParseException("", 0);
            return new Point4D(Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof Point4D) {
            Point4D p = (Point4D)o;
            return p.getX() == x && p.getY() == y && p.getZ() == z && p.getW() == w;
        }
        return false;
    }
    
    @Deprecated
    public boolean equalish(Object o) {
        if (o instanceof Point4D) {
            Point4D p = (Point4D)o;
            return deq(p.getX(), x) && deq(p.getY(), y) && deq(p.getZ(), z) && deq(p.getW(), w);
        }
        return false;
    }
    
    public boolean deq(double a, double b) {
        return Math.abs(a - b) <= Math.abs(a) * 0.00001;
    }
    
//    public static Vector4D normal(Point4D p1, Point4D p2, Point4D p3) {
//        return p1.vectorTo(p2).crossProduct(p2.vectorTo(p3)).normalize();
//    }
    
}
