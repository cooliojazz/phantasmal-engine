package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public class Vector2D extends Point2D implements Vector  {
    
    public static Vector2D zeros() {
        return new Vector2D(0, 0);
    }
    public static Vector2D ones() {
        return new Vector2D(1, 1);
    }

    public Vector2D(double x, double y) {
        super(x, y);
    }

    public Vector2D(Point2D p) {
        super(p.getX(), p.getY());
    }
    
//    public Vector2D crossProduct(Vector2D v2) {
//        return new Vector2D(getY() * v2.getZ() - v2.getY() * getZ(), getZ() * v2.getX() - v2.getZ() * getX(), getX() * v2.getY() - v2.getX() * getY());
//    }
    
    public Vector2D diff(Vector2D v2) {
        return new Vector2D(getX() - v2.getX(), getY() - v2.getY());
    }
    
    public Vector2D sum(Vector2D v2) {
        return new Vector2D(getX() + v2.getX(), getY() + v2.getY());
    }
    
    public double sum() {
        return getX() + getY();
    }
    
    public double dotProduct(Vector2D v2) {
        Vector2D v = product(v2);
        return v.getX() + v.getY();
    }
    
    public Vector2D product(Vector2D v2) {
        return new Vector2D(getX() * v2.getX(), getY() * v2.getY());
    }
    
    public Vector2D quotient(Vector2D v2) {
        return new Vector2D(getX() / v2.getX(), getY() / v2.getY());
    }
    
    public Point2D getPoint2D() {
        return super.clone();
    }
    
    public Vector2D normalize() {
        double mag = magnitude();
        if (getX() != 0) setX(getX() / mag);
        if (getY() != 0) setY(getY() / mag);
        return this;
    }
    
    public double squareSum() {
        return getX() * getX() + getY() * getY();
    }
    
    public double magnitude() {
        return Math.sqrt(squareSum());
    }

    @Override
    public Vector2D mul(double d) {
        return new Vector2D(getX() * d, getY() * d);
    }
    
    @Override
    public Vector2D sum(double d) {
        return new Vector2D(getX() + d, getY() + d);
    }
    
    @Override
    public Vector2D rotate(Angle1D a) {
        return new Vector2D(super.rotate(a));
    }
    
    public Vector2D abs() {
        return new Vector2D(Math.abs(getX()), Math.abs(getY()));
    }

	@Override
	public Matrix2D toMatrix() {
		return new Matrix2D(new double[][] {new double[] {getX(), 0}, new double[] {getY(), 0}});
	}
    
    public Vector2D deInfinity() {
        if (Double.isInfinite(getX()) || Double.isNaN(getX())) {
            setX(0);
        }
        if (Double.isInfinite(getY()) || Double.isNaN(getY())) {
            setY(0);
        }
        if (getX() != 0 || getY() != 0) {
            System.out.println("No! Bad!" + this);
        }
        return this;
    }
    
    @Override
    public Vector2D clone() {
        return new Vector2D(getX(), getY());
    }

    @Override
    public String toString() {
        return "<" + getX() + ", " + getY() + ">";
    }
    
}
