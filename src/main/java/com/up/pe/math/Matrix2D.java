package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public class Matrix2D implements Cloneable, Matrix {
	
	// Indexed as [y, x]
	private double[][] matrix = new double[2][2];

	public Matrix2D() {
	}
	
	public Matrix2D(double[][] matrix) {
		this.matrix = matrix;
	}

	public Matrix2D transpose() {
		return new Matrix2D(new double[][] {
			new double[] {matrix[0][0], matrix[1][0]},
			new double[] {matrix[0][1], matrix[1][1]}});
	}

	public Matrix2D sum(Matrix2D m) {
		return new Matrix2D(new double[][] {
			new double[] {matrix[0][0] + m.matrix[0][0], matrix[0][1] + m.matrix[0][1]},
			new double[] {matrix[1][0] + m.matrix[1][0], matrix[1][1] + m.matrix[1][1]}});
	}

	public Matrix2D scale(double s) {
		return new Matrix2D(new double[][] {
			new double[] {matrix[0][0] * s, matrix[0][1] * s},
			new double[] {matrix[1][0] * s, matrix[1][1] * s}});
	}

	public Matrix2D multiply(Matrix2D m) {
		return new Matrix2D(new double[][] {
			new double[] {matrix[0][0] * m.matrix[0][0] + matrix[0][1] * m.matrix[1][0], matrix[0][0] * m.matrix[0][1] + matrix[0][1] * m.matrix[1][1]},
			new double[] {matrix[1][0] * m.matrix[0][0] + matrix[1][1] * m.matrix[1][0], matrix[1][0] * m.matrix[0][1] + matrix[1][1] * m.matrix[1][1]}});
	}

	public Vector2D apply(Vector2D v) {
		return new Vector2D(matrix[0][0] * v.getX() + matrix[0][1] * v.getY(), matrix[1][0] * v.getX() + matrix[1][1] * v.getY());
	}

	public Vector1D apply(Vector1D v) {
		return new Vector1D(matrix[0][0] * v.getX() + matrix[0][1]);
	}

	public static Matrix2D identity() {
		return new Matrix2D(new double[][] {new double[] {1, 0}, new double[] {0, 1}});
	}
}
