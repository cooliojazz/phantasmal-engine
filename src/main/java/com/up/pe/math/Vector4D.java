package com.up.pe.math;

import com.jogamp.opengl.GL2;

/**
 *
 * @author Ricky
 */
public class Vector4D extends Point4D implements Vector {
    
    public static Vector4D zeros() {
        return new Vector4D(0, 0, 0, 0);
    }
	
    public static Vector4D ones() {
        return new Vector4D(1, 1, 1, 1);
    }

    public Vector4D(double x, double y, double z, double w) {
        super(x, y, z, w);
    }

    public Vector4D(double v) {
        super(v, v, v, v);
    }

    public Vector4D(Point4D p) {
        super(p.getX(), p.getY(), p.getZ(), p.getW());
    }
    
//    public Vector4D crossProduct(Vector4D v2) {
//        return new Vector4D(getY() * v2.getZ() - v2.getY() * getZ(), getZ() * v2.getX() - v2.getZ() * getX(), getX() * v2.getY() - v2.getX() * getY());
//    }
    
    public Vector4D diff(Vector4D v2) {
        return new Vector4D(getX() - v2.getX(), getY() - v2.getY(), getZ() - v2.getZ(), getW() - v2.getW());
    }
    
    public Vector4D sum(Vector4D v2) {
        return new Vector4D(getX() + v2.getX(), getY() + v2.getY(), getZ() + v2.getZ(), getW() + v2.getW());
    }
    
    public double sum() {
        return getX() + getY() + getZ() + getW();
    }
    
    public double dotProduct(Vector4D v2) {
        Vector4D v = product(v2);
        return v.getX() + v.getY() + v.getZ() + v.getW();
    }
    
    public Vector4D product(Vector4D v2) {
        return new Vector4D(getX() * v2.getX(), getY() * v2.getY(), getZ() * v2.getZ(), getW() * v2.getW());
    }
    
    public Vector4D quotient(Vector4D v2) {
        return new Vector4D(getX() / v2.getX(), getY() / v2.getY(), getZ() / v2.getZ(), getW() / v2.getW());
    }
    
    public Point4D getPoint4D() {
        return super.clone();
    }
    
    public Vector4D normalize() {
        double mag = magnitude();
        if (mag != 0) return new Vector4D(getX() / mag, getY() / mag, getZ() / mag, getW() / mag);
        return Vector4D.zeros();
    }
    
    public double magnitude() {
        return Math.sqrt(getX() * getX() + getY() * getY() + getZ() * getZ() + getW() * getW());
    }

    @Override
    public Vector4D mul(double d) {
        return new Vector4D(getX() * d, getY() * d, getZ() * d, getW() * d);
    }
    
    @Override
    public Vector4D sum(double d) {
        return new Vector4D(getX() + d, getY() + d, getZ() + d, getW() + d);
    }
    
//    @Override
//    public Vector4D rotate(Angle3D a) {
//        return new Vector4D(super.rotate(a));
//    }
    
    public Vector4D abs() {
        return new Vector4D(Math.abs(getX()), Math.abs(getY()), Math.abs(getZ()), Math.abs(getW()));
    }
    
    @Override
    public Matrix4D toMatrix() {
        return new Matrix4D(new double[][] {new double[] {getX(), 0, 0, 0}, new double[] {getY(), 0, 0, 0}, new double[] {getZ(), 0, 0, 0}, new double[] {getW(), 0, 0, 0}});
    }
	
//    public Matrix4D crossMatrix() {
//        return new Matrix3D(new double[][] {new double[] {0, -getZ(), getY()}, new double[] {getZ(), 0, -getX()}, new double[] {-getY(), getX(), 0}});
//    }
    
    public Vector4D deInfinity() {
        if (Double.isInfinite(getX()) || Double.isNaN(getX())) {
            setX(0);
        }
        if (Double.isInfinite(getY()) || Double.isNaN(getY())) {
            setY(0);
        }
        if (Double.isInfinite(getZ()) || Double.isNaN(getZ())) {
            setZ(0);
        }
        if (Double.isInfinite(getW()) || Double.isNaN(getW())) {
            setW(0);
        }
        if (getX() != 0 || getY() != 0 || getZ() != 0 || getW() != 0) {
            System.out.println("No! Bad!" + this);
        }
        return this;
    }
    
    @Override
    public String toString() {
        return "<" + getX() + ", " + getY() + ", " + getZ() + ", " + getW() + ">";
    }
    
    @Override
    public Vector4D clone() {
        return new Vector4D(getX(), getY(), getZ(), getW());
    }
    
//    /**
//     * Draw the Vector, e.g. for debugging
//     */
//    public void render(GL2 gl, Point3D p) {
//        gl.glBegin(GL2.GL_LINES);
//        gl.glVertex3d(p.getX(), p.getY(), p.getZ());
//        Vector4D v = normalize().mul(5).sum(p).toVec();
////        Point3D v = multiply(10).sum(p);
//        gl.glVertex3d(v.getX(), v.getY(), v.getZ());
//        gl.glEnd();
//    }
    
}
