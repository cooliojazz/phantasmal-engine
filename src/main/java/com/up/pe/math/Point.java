package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public interface Point {
    
    public Vector vectorTo(Point p);
    
}
