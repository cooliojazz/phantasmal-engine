package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public class Angle1D extends Point1D {
    
    public static Angle1D zeros = new Angle1D(0);

    /**
     * Constructs a new Angle1D with the given (x) rotation specified in radians.
     * @param x
     */
    public Angle1D(double x) {
        super(x);
        updatePreCalc();
    }
    
    /**
     * Constructs a new Angle1D from the given Point1D p.
     * @param p 
     */
    public Angle1D(Point1D p) {
        super(p.getX());
        updatePreCalc();
    }

    /**
     * Constructs a new Angle1D with the given (x) rotation specified in degrees.
     * @param x
     */
    public Angle1D(int x) {
        super(x * Math.PI / 180);
        updatePreCalc();
    }

    @Override
    public void setX(double x) {
        super.setX(x);
        updatePreCalc();
    }
    

    /**
     * Gets the value of the x rotation in degrees instead of the default radians
     * @return Degrees representation of x
     */
    public double getXDeg() {
        return getX() * 180 / Math.PI;
    }
    
    /**
     * 
     * @param x the value to add to the x
     * @return a new Angle1D with the added X value
     */
    @Override
    public Angle1D addX(double x) {
        return new Angle1D(getX() + x);
    }
    
    @Override
    public Angle1D sum(Point1D p2) {
        return new Angle1D(getX() + p2.getX());
    }
    
    private void updatePreCalc() {
        precalcrot = new double[][] {
            new double[] {Math.cos(getX()), -Math.sin(getX())},
            new double[] {Math.sin(getX()), Math.cos(getX())}};
    }
    
    double[][] precalcrot;
    
    public Point2D doRotation(Point2D p) {
        return new Point2D(p.getX() * precalcrot[0][0] + p.getY() * precalcrot[0][1], p.getX() * precalcrot[1][0] + p.getY() * precalcrot[1][1]);
    }
	
	public Matrix2D toMatrix() {
		return new Matrix2D(new double[][] {
			new double[] {Math.cos(getX()), -Math.sin(getX())},
			new double[] {Math.sin(getX()), Math.cos(getX())}});
	}
}
