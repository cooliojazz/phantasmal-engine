package com.up.pe.math;

import java.util.function.Supplier;

/**
 *
 * @author Ricky
 */
public interface Positionable extends Supplier<Point3D> {
    
    public Point3D getPosition();
    public void setPosition(Point3D position);

    @Override
    public default Point3D get() {
        return getPosition();
    }
    
}
