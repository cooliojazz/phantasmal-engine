package com.up.pe.math;

import com.jogamp.opengl.math.Matrix4f;

/**
 *
 * @author Ricky
 */
public class Matrix4D implements Cloneable, Matrix {
	
	// Indexed as [y, x]
	private double[][] matrix = new double[4][4];

	public Matrix4D() {
	}
	
	public Matrix4D(double[][] matrix) {
		this.matrix = matrix;
	}
	
	public Matrix4D(float[][] matrix) {
		this.matrix = new double[][] {
			new double[] {matrix[0][0], matrix[0][1], matrix[0][2], matrix[0][3]},
			new double[] {matrix[1][0], matrix[1][1], matrix[1][2], matrix[1][3]},
			new double[] {matrix[2][0], matrix[2][1], matrix[2][2], matrix[2][3]},
			new double[] {matrix[3][0], matrix[3][1], matrix[3][2], matrix[3][3]}};
	}
	
	public double get(int y, int x) {
		return matrix[y][x];
	}

	public Matrix4D transpose() {
		return new Matrix4D(new double[][] {
			new double[] {matrix[0][0], matrix[1][0], matrix[2][0], matrix[3][0]},
			new double[] {matrix[0][1], matrix[1][1], matrix[2][1], matrix[3][1]},
			new double[] {matrix[0][2], matrix[1][2], matrix[2][2], matrix[3][2]},
			new double[] {matrix[0][3], matrix[1][3], matrix[2][3], matrix[3][3]}});
	}

	public Matrix4D multiply(double s) {
		return new Matrix4D(new double[][] {
			new double[] {matrix[0][0] * s, matrix[0][1] * s, matrix[0][2] * s, matrix[0][3] * s},
			new double[] {matrix[1][0] * s, matrix[1][1] * s, matrix[1][2] * s, matrix[1][3] * s},
			new double[] {matrix[2][0] * s, matrix[2][1] * s, matrix[2][2] * s, matrix[2][3] * s},
			new double[] {matrix[3][0] * s, matrix[3][1] * s, matrix[3][2] * s, matrix[3][3] * s}});
	}

	public Matrix4D sum(Matrix4D m) {
		return new Matrix4D(new double[][] {
			new double[] {matrix[0][0] + m.matrix[0][0], matrix[0][1] + m.matrix[0][1], matrix[0][2] + m.matrix[0][2], matrix[0][3] + m.matrix[0][3]},
			new double[] {matrix[1][0] + m.matrix[1][0], matrix[1][1] + m.matrix[1][1], matrix[1][2] + m.matrix[1][2], matrix[1][3] + m.matrix[1][3]},
			new double[] {matrix[2][0] + m.matrix[2][0], matrix[2][1] + m.matrix[2][1], matrix[2][2] + m.matrix[2][2], matrix[2][3] + m.matrix[2][3]},
			new double[] {matrix[3][0] + m.matrix[3][0], matrix[3][1] + m.matrix[3][1], matrix[3][2] + m.matrix[3][2], matrix[3][3] + m.matrix[3][3]}});
	}

	public Matrix4D product(Matrix4D m) {
		return new Matrix4D(new double[][] {
			new double[] {matrix[0][0] * m.matrix[0][0], matrix[0][1] * m.matrix[0][1], matrix[0][2] * m.matrix[0][2], matrix[0][3] * m.matrix[0][3]},
			new double[] {matrix[1][0] * m.matrix[1][0], matrix[1][1] * m.matrix[1][1], matrix[1][2] * m.matrix[1][2], matrix[1][3] * m.matrix[1][3]},
			new double[] {matrix[2][0] * m.matrix[2][0], matrix[2][1] * m.matrix[2][1], matrix[2][2] * m.matrix[2][2], matrix[2][3] * m.matrix[2][3]},
			new double[] {matrix[3][0] * m.matrix[3][0], matrix[3][1] * m.matrix[3][1], matrix[3][2] * m.matrix[3][2], matrix[3][3] * m.matrix[3][3]}});
	}

	public Matrix4D multiply(Matrix4D m) {
		return new Matrix4D(new double[][] {
				new double[] {
					matrix[0][0] * m.matrix[0][0] + matrix[0][1] * m.matrix[1][0] + matrix[0][2] * m.matrix[2][0] + matrix[0][3] * m.matrix[3][0],
					matrix[0][0] * m.matrix[0][1] + matrix[0][1] * m.matrix[1][1] + matrix[0][2] * m.matrix[2][1] + matrix[0][3] * m.matrix[3][1],
					matrix[0][0] * m.matrix[0][2] + matrix[0][1] * m.matrix[1][2] + matrix[0][2] * m.matrix[2][2] + matrix[0][3] * m.matrix[3][2],
					matrix[0][0] * m.matrix[0][3] + matrix[0][1] * m.matrix[1][3] + matrix[0][2] * m.matrix[2][3] + matrix[0][3] * m.matrix[3][3]},
				new double[] {
					matrix[1][0] * m.matrix[0][0] + matrix[1][1] * m.matrix[1][0] + matrix[1][2] * m.matrix[2][0] + matrix[1][3] * m.matrix[3][0],
					matrix[1][0] * m.matrix[0][1] + matrix[1][1] * m.matrix[1][1] + matrix[1][2] * m.matrix[2][1] + matrix[1][3] * m.matrix[3][1],
					matrix[1][0] * m.matrix[0][2] + matrix[1][1] * m.matrix[1][2] + matrix[1][2] * m.matrix[2][2] + matrix[1][3] * m.matrix[3][2],
					matrix[1][0] * m.matrix[0][3] + matrix[1][1] * m.matrix[1][3] + matrix[1][2] * m.matrix[2][3] + matrix[1][3] * m.matrix[3][3]},
				new double[] {
					matrix[2][0] * m.matrix[0][0] + matrix[2][1] * m.matrix[1][0] + matrix[2][2] * m.matrix[2][0] + matrix[2][3] * m.matrix[3][0],
					matrix[2][0] * m.matrix[0][1] + matrix[2][1] * m.matrix[1][1] + matrix[2][2] * m.matrix[2][1] + matrix[2][3] * m.matrix[3][1],
					matrix[2][0] * m.matrix[0][2] + matrix[2][1] * m.matrix[1][2] + matrix[2][2] * m.matrix[2][2] + matrix[2][3] * m.matrix[3][2],
					matrix[2][0] * m.matrix[0][3] + matrix[2][1] * m.matrix[1][3] + matrix[2][2] * m.matrix[2][3] + matrix[2][3] * m.matrix[3][3]},
				new double[] {
					matrix[3][0] * m.matrix[0][0] + matrix[3][1] * m.matrix[1][0] + matrix[3][2] * m.matrix[2][0] + matrix[3][3] * m.matrix[3][0],
					matrix[3][0] * m.matrix[0][1] + matrix[3][1] * m.matrix[1][1] + matrix[3][2] * m.matrix[2][1] + matrix[3][3] * m.matrix[3][1],
					matrix[3][0] * m.matrix[0][2] + matrix[3][1] * m.matrix[1][2] + matrix[3][2] * m.matrix[2][2] + matrix[3][3] * m.matrix[3][2],
					matrix[3][0] * m.matrix[0][3] + matrix[3][1] * m.matrix[1][3] + matrix[3][2] * m.matrix[2][3] + matrix[3][3] * m.matrix[3][3]}
			});
	}

//	public Matrix4D setScale(Vector4D v) {
//		return Matrix4D.this.multiply(new Matrix4D(new double[][] {
//			new double[] {v.getX(), 0, 0, 0},
//			new double[] {0, v.getY(), 0, 0},
//			new double[] {0, 0, v.getZ(), 0},
//			new double[] {0, 0, 0, v.getW()}}));
//	}
//
//	public Matrix4D setScale(Vector3D v) {
//		return Matrix4D.this.product(new Matrix4D(new double[][] {
//			new double[] {v.getX(), 1, 1, 1},
//			new double[] {1, v.getY(), 1, 1},
//			new double[] {1, 1, v.getZ(), 1},
//			new double[] {1, 1, 1, 1}}));
//	}
	
//	public Matrix4D setOffset(Point4D v) {
//		return Matrix4D.this.sum(new Matrix4D(new double[][] {
//			new double[] {1, 0, 0, v.getX()},
//			new double[] {0, 1, 0, v.getY()},
//			new double[] {0, 0, 1, v.getZ()},
//			new double[] {0, 0, 0, v.getW()}}));
//	}
//
//	public Matrix4D setOffset(Point3D v) {
//		return Matrix4D.this.sum(new Matrix4D(new double[][] {
//			new double[] {1, 0, 0, v.getX()},
//			new double[] {0, 1, 0, v.getY()},
//			new double[] {0, 0, 1, v.getZ()},
//			new double[] {0, 0, 0, 1}}));
//	}

	public Vector4D apply(Vector4D v) {
		return new Vector4D(matrix[0][0] * v.getX() + matrix[0][1] * v.getY() + matrix[0][2] * v.getZ() + matrix[0][3] * v.getW(),
							matrix[1][0] * v.getX() + matrix[1][1] * v.getY() + matrix[1][2] * v.getZ() + matrix[1][3] * v.getW(),
							matrix[2][0] * v.getX() + matrix[2][1] * v.getY() + matrix[2][2] * v.getZ() + matrix[2][3] * v.getW(),
							matrix[3][0] * v.getX() + matrix[3][1] * v.getY() + matrix[3][2] * v.getZ() + matrix[3][3] * v.getW());
	}

	public Vector3D apply(Vector3D v) {
		return new Vector3D(matrix[0][0] * v.getX() + matrix[0][1] * v.getY() + matrix[0][2] * v.getZ(),
							matrix[1][0] * v.getX() + matrix[1][1] * v.getY() + matrix[1][2] * v.getZ(),
							matrix[2][0] * v.getX() + matrix[2][1] * v.getY() + matrix[2][2] * v.getZ());
	}

	public Vector2D apply(Vector2D v) {
		return new Vector2D(matrix[0][0] * v.getX() + matrix[0][1] * v.getY() + matrix[0][2], matrix[1][0] * v.getX() + matrix[1][1] * v.getY() + matrix[1][2]);
	}
	
//	public double det() {
//		return matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]) +
//				matrix[0][1] * (matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2]) +
//				matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
//	}
	
	public double trace() {
		return matrix[0][0] + matrix[1][1] + matrix[2][2] + matrix[3][3];
	}
	
//	public Matrix4D invert() {
//		// Cache partial det values for inverse
//		double a = matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1];
//		double b = matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2];
//		double c = matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0];
////		double d = matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0];
//		return new Matrix4D(new double[][] {
//				{a, b, c},
//				{matrix[0][2] * matrix[1][2] - matrix[0][1] * matrix[2][2], matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0], matrix[0][1] * matrix[2][0] - matrix[0][0] * matrix[2][1]},
//				{matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1], matrix[0][2] * matrix[1][0] - matrix[0][0] * matrix[1][2], matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]},
//			}).multiply(1 / matrix[0][0] * a + matrix[0][1] * b + matrix[0][2] * c);
//	}
	
	/**
	 * A faster version of invert that only works if this matrix represents an affine transformation
	 */
	public Matrix4D invertTransform() { // From https://stackoverflow.com/questions/2624422/efficient-4x4-matrix-inverse-affine-transform
		double s0 = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
		double s1 = matrix[0][0] * matrix[1][2] - matrix[1][0] * matrix[0][2];
		double s2 = matrix[0][0] * matrix[1][3] - matrix[1][0] * matrix[0][3];
		double s3 = matrix[0][1] * matrix[1][2] - matrix[1][1] * matrix[0][2];
		double s4 = matrix[0][1] * matrix[1][3] - matrix[1][1] * matrix[0][3];
		double s5 = matrix[0][2] * matrix[1][3] - matrix[1][2] * matrix[0][3];

		double c5 = matrix[2][2] * matrix[3][3] - matrix[3][2] * matrix[2][3];
		double c4 = matrix[2][1] * matrix[3][3] - matrix[3][1] * matrix[2][3];
		double c3 = matrix[2][1] * matrix[3][2] - matrix[3][1] * matrix[2][2];
		double c2 = matrix[2][0] * matrix[3][3] - matrix[3][0] * matrix[2][3];
		double c1 = matrix[2][0] * matrix[3][2] - matrix[3][0] * matrix[2][2];
		double c0 = matrix[2][0] * matrix[3][1] - matrix[3][0] * matrix[2][1];

		// Should check for 0 determinant
		double invdet = 1.0 / (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);

		return new Matrix4D(new double[][] {
				new double[] {
					( matrix[1][1] * c5 - matrix[1][2] * c4 + matrix[1][3] * c3) * invdet,
					(-matrix[0][1] * c5 + matrix[0][2] * c4 - matrix[0][3] * c3) * invdet,
					( matrix[3][1] * s5 - matrix[3][2] * s4 + matrix[3][3] * s3) * invdet,
					(-matrix[2][1] * s5 + matrix[2][2] * s4 - matrix[2][3] * s3) * invdet},
				new double[] {
					(-matrix[1][0] * c5 + matrix[1][2] * c2 - matrix[1][3] * c1) * invdet,
					( matrix[0][0] * c5 - matrix[0][2] * c2 + matrix[0][3] * c1) * invdet,
					(-matrix[3][0] * s5 + matrix[3][2] * s2 - matrix[3][3] * s1) * invdet,
					( matrix[2][0] * s5 - matrix[2][2] * s2 + matrix[2][3] * s1) * invdet},
				new double[] {
					( matrix[1][0] * c4 - matrix[1][1] * c2 + matrix[1][3] * c0) * invdet,
					(-matrix[0][0] * c4 + matrix[0][1] * c2 - matrix[0][3] * c0) * invdet,
					( matrix[3][0] * s4 - matrix[3][1] * s2 + matrix[3][3] * s0) * invdet,
					(-matrix[2][0] * s4 + matrix[2][1] * s2 - matrix[2][3] * s0) * invdet},
				new double[] {
					(-matrix[1][0] * c3 + matrix[1][1] * c1 - matrix[1][2] * c0) * invdet,
					( matrix[0][0] * c3 - matrix[0][1] * c1 + matrix[0][2] * c0) * invdet,
					(-matrix[3][0] * s3 + matrix[3][1] * s1 - matrix[3][2] * s0) * invdet,
					( matrix[2][0] * s3 - matrix[2][1] * s1 + matrix[2][2] * s0) * invdet},
			});
	}
	
	public Angle3D getRotation() {
		if (matrix[2][0] == 1) {
			return new Angle3D(Math.atan2(-matrix[0][1], -matrix[0][2]), -Math.PI / 2, 0);
		} else if (matrix[2][0] == -1) {
			return new Angle3D(Math.atan2(matrix[0][1], matrix[0][2]), Math.PI / 2, 0);
		} else {
			double y = Math.asin(-matrix[2][0]);
			return new Angle3D(Math.atan2(matrix[2][1] / Math.cos(y), matrix[2][2] / Math.cos(y)), y, Math.atan2(matrix[1][0] / Math.cos(y), matrix[0][0] / Math.cos(y)));
		}
	}
	
	public Matrix4f toGlMatrix() {
		return new Matrix4f(new float[] {(float)matrix[0][0], (float)matrix[1][0], (float)matrix[2][0], (float)matrix[3][0],
										 (float)matrix[0][1], (float)matrix[1][1], (float)matrix[2][1], (float)matrix[3][1],
										 (float)matrix[0][2], (float)matrix[1][2], (float)matrix[2][2], (float)matrix[3][2],
										 (float)matrix[0][3], (float)matrix[1][3], (float)matrix[2][3], (float)matrix[3][3]});
	}
	
	// GL prefers column-major by default
	public float[] toRawGlFMatrix() {
		return new float[] {(float)matrix[0][0], (float)matrix[1][0], (float)matrix[2][0], (float)matrix[3][0],
							(float)matrix[0][1], (float)matrix[1][1], (float)matrix[2][1], (float)matrix[3][1],
							(float)matrix[0][2], (float)matrix[1][2], (float)matrix[2][2], (float)matrix[3][2],
							(float)matrix[0][3], (float)matrix[1][3], (float)matrix[2][3], (float)matrix[3][3]};
	}
	
	public double[] toRawGlDMatrix() {
		return new double[] {matrix[0][0], matrix[1][0], matrix[2][0], matrix[3][0],
						  	 matrix[0][1], matrix[1][1], matrix[2][1], matrix[3][1],
						  	 matrix[0][2], matrix[1][2], matrix[2][2], matrix[3][2],
						  	 matrix[0][3], matrix[1][3], matrix[2][3], matrix[3][3]};
	}
	
	public static Matrix4D fromGlMatrix(Matrix4f m) {
		return new Matrix4D(new double[][] {
			new double[] {m.get(0), m.get(4), m.get(8), m.get(12)},
			new double[] {m.get(1), m.get(5), m.get(9), m.get(13)},
			new double[] {m.get(2), m.get(6), m.get(10), m.get(14)},
			new double[] {m.get(3), m.get(7), m.get(11), m.get(15)}});
	}

	public static Matrix4D identity() {
		return new Matrix4D(new double[][] {new double[] {1, 0, 0, 0}, new double[] {0, 1, 0, 0}, new double[] {0, 0, 1, 0}, new double[] {0, 0, 0, 1}});
	}

	public static Matrix4D scaleBy(Vector3D v) {
		return new Matrix4D(new double[][] {
			new double[] {v.getX(), 0, 0, 0},
			new double[] {0, v.getY(), 0, 0},
			new double[] {0, 0, v.getZ(), 0},
			new double[] {0, 0, 0, 1}});
	}

	public static Matrix4D scaleBy(Vector4D v) {
		return new Matrix4D(new double[][] {
			new double[] {v.getX(), 0, 0, 0},
			new double[] {0, v.getY(), 0, 0},
			new double[] {0, 0, v.getZ(), 0},
			new double[] {0, 0, 0, v.getW()}});
	}
	

	public static Matrix4D offsetBy(Point4D v) {
		return new Matrix4D(new double[][] {
			new double[] {1, 0, 0, v.getX()},
			new double[] {0, 1, 0, v.getY()},
			new double[] {0, 0, 1, v.getZ()},
			new double[] {0, 0, 0, v.getW()}});
	}

	public static Matrix4D offsetBy(Point3D v) {
		return new Matrix4D(new double[][] {
			new double[] {1, 0, 0, v.getX()},
			new double[] {0, 1, 0, v.getY()},
			new double[] {0, 0, 1, v.getZ()},
			new double[] {0, 0, 0, 1}});
	}
}
