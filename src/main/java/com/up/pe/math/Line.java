package com.up.pe.math;

/**
 *
 * @author Ricky
 */
public class Line<T extends Point> {
    
    T p0;
    T p1;

    public Line(T p0, T p1) {
        this.p0 = p0;
        this.p1 = p1;
    }

    public T getPoint0() {
        return p0;
    }

    public T getPoint1() {
        return p1;
    }
    
    public Vector getVector() {
        return p0.vectorTo(p1);
    }
    
}
