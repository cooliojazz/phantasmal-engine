package com.up.pe.math;

/**
 * Represents a point in 1D space with an x coordinate.
 * @author Ricky Talbot
 */
public class Point1D implements Cloneable, Point {
    private double x = 0;
    
    /**
     * Constructs a new Point1D at the given (x) coordinate.
     * @param x
     */
    public Point1D(double x) {
        this.x = x;
    }
    
    /**
     * Gets the x-coordinate of this point
     * @return the x-coordinate
     */
    public double getX() {
        return x;
    }
    
    /**
     * Sets the x-coordinate of this point.
     * @param x the x-coordinate
     */
    public void setX(double x) {
        this.x = x;
    }
    
    /**
     * 
     * @param x
     * @return
     */
    public Point1D addX(double x) {
        return new Point1D(this.x + x);
    }
    
    /**
     * Returns the point halfway between this point and the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Point2D midway between the points
     */
    public Point1D midPoint(Point1D p2) {
        return new Point1D((p2.getX() - x) / 2 + x);
    }
    
    public Point1D mul(double d) {
        return new Point1D(getX() * d);
    }
    
    public Point1D sum(Point1D p2) {
        return new Point1D(getX() + p2.getX());
    }
    
    public Point1D sum(double d) {
        return new Point1D(getX() + d);
    }
    
    public Point1D clone() {
        return new Point1D(x);
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector1D between the points
     */
    @Override
    public Vector vectorTo(Point p2) {
        if (p2 instanceof Point1D)
            return new Vector1D(((Point1D)p2).getX() - x);
        else
            return null;
    }
    
    /**
     * Returns the vector from this point to the point <tt>p2</tt>
     * @param p2 the second point
     * @return The Vector2D between the points
     */
    public Vector1D vectorTo(Point1D p2) {
        return new Vector1D(p2.getX() - x);
    }
}
