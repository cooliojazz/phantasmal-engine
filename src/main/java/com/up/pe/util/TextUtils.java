package com.up.pe.util;

/**
 *
 * @author Ricky
 */
public class TextUtils {
	
	public static float[] hexToFloat(String s) {
        float[] f = new float[4];
        if (s.length() < 8) s = padRight("0", 8, s);
        f[0] = (float)Integer.parseInt(s.substring(0, 2), 16) / 255;
        f[1] = (float)Integer.parseInt(s.substring(2, 4), 16) / 255;
        f[2] = (float)Integer.parseInt(s.substring(4, 6), 16) / 255;
        f[3] = (float)Integer.parseInt(s.substring(6, 8), 16) / 255;
        return f;
    }
    
    public static String padLeft(String pad, int length, String s) {
        String ret = s;
        while (ret.length() < length) {
            ret = pad + ret;
        }
        return ret;
    }
    
    public static String padRight(String pad, int length, String s) {
        String ret = s;
        while (ret.length() < length) {
            ret += pad;
        }
        return ret;
    }
}
