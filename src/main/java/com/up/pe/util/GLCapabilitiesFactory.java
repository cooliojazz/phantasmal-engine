package com.up.pe.util;

import com.jogamp.opengl.GLCapabilities;

/**
 *
 * @author Ricky
 */
public class GLCapabilitiesFactory {
	
	private GLCapabilities caps;

	public GLCapabilitiesFactory() {
		caps = new GLCapabilities(null);
	}
	
	public GLCapabilitiesFactory withDefaultMultisampleCapabilities(int samples) {
		caps.setSampleBuffers(true);
		caps.setNumSamples(samples);
		return this;
	}
	
	public GLCapabilitiesFactory withDepthBits(int bits) {
		caps.setDepthBits(bits);
		return this;
	}
	
	public GLCapabilitiesFactory usingFBO() {
		caps.setFBO(true);
		return this;
	}
	
	public GLCapabilitiesFactory usingDoubleBuffering() {
		caps.setDoubleBuffered(true);
		return this;
	}
	
	public GLCapabilities get() {
		return caps;
	}
}
