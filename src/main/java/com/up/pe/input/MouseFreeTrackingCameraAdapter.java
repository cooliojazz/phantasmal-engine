package com.up.pe.input;

import com.up.pe.math.Angle1D;
import com.up.pe.math.Point2D;
import com.up.pe.math.Vector2D;
import com.up.pe.render.camera.FreeTrackingCamera;
import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * Simple mouse controls for a FreeTrackingCamera
 * @author Ricky
 */
public class MouseFreeTrackingCameraAdapter implements MouseListener, MouseMotionListener, MouseWheelListener {

    private FreeTrackingCamera camera;
    private int buttons = 0;
	private double dragSpeed = 1;

    public MouseFreeTrackingCameraAdapter(FreeTrackingCamera camera) {
        this.camera = camera;
    }

    public void addToComponent(Component c) {
        c.addMouseMotionListener(this);
        c.addMouseListener(this);
        c.addMouseWheelListener(this);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        buttons |= InputEvent.getMaskForButton(e.getButton());
        lx = e.getX();
        ly = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        buttons &= ~InputEvent.getMaskForButton(e.getButton());
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private int lx = 0;
    private int ly = 0;

    @Override
    public void mouseDragged(MouseEvent e) {
        if ((buttons & InputEvent.getMaskForButton(MouseEvent.BUTTON2)) > 0) {
            Vector2D dv = new Vector2D((e.getX() - lx), (e.getY() - ly));
            Point2D pv = dv.mul(-1 / 10d).rotate(new Angle1D(-camera.getAngle().getY()));
            camera.setOffset(camera.getOffset().addZ(pv.getY() * camera.getDistance() / 100 * dragSpeed));
            camera.setOffset(camera.getOffset().addX(pv.getX() * camera.getDistance() / 100 * dragSpeed));
        }
        if ((buttons & InputEvent.getMaskForButton(MouseEvent.BUTTON3)) > 0) {
            camera.setHoverAngle(camera.getHoverAngle().addY(-(e.getX() - lx) / 100d));
            camera.getHoverAngle().setX(Math.max(Math.min(camera.getHoverAngle().getX() + (e.getY() - ly) / 100d, Math.PI / 2 - .001), -Math.PI / 2 + .001));
        }
        lx = e.getX();
        ly = e.getY();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        camera.setDistance(Math.max(camera.getDistance() + e.getPreciseWheelRotation() * camera.getDistance() / 20, 1));
    }

}
