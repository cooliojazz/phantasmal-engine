package com.up.pe.input;

import com.up.pe.render.camera.FirstPersonCamera;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * Simple mouse controls for a FreeTrackingCamera
 * @author Ricky
 */
public class MouseFirstPersonCameraAdapter implements MouseListener, MouseMotionListener, MouseWheelListener {

    private FirstPersonCamera camera;

    private int lx = 0;
    private int ly = 0;

    public MouseFirstPersonCameraAdapter(FirstPersonCamera camera) {
        this.camera = camera;
    }

    public void addToComponent(Component c) {
        c.addMouseMotionListener(this);
        c.addMouseListener(this);
        c.addMouseWheelListener(this);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
		camera.setAngle(camera.getAngle().addY(-(e.getX() - lx) / 100d));
		camera.getAngle().setX(Math.max(Math.min(camera.getAngle().getX() + (e.getY() - ly) / 100d, Math.PI / 2 - .001), -Math.PI / 2 + .001));
        lx = e.getX();
        ly = e.getY();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
    }

}
