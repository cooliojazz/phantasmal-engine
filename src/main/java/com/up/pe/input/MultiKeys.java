package com.up.pe.input;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author Ricky
 */
public class MultiKeys extends KeyAdapter {
	
    private Stack<Integer> keys = new Stack<Integer>();
    private Stack<Integer> singlekeys = new Stack<Integer>();
    private ActionListener al;
    private final Thread t = new Thread(() -> {
            while (true) {
                doKeyCheck();
                try {Thread.sleep(50);} catch (Exception e) {}
            }
		}, "MK Action");
    
    private void doKeyCheck() {
        al.actionPerformed(new ActionEvent(MultiKeys.this, 3, "Re-Trigger"));
    }

	/**
	 * Use the source of the event in the listener to get the MultiKeys instance to check.
	 */
    public MultiKeys(ActionListener al) {
        this.al = al;
        t.start();
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!singlekeys.contains(e.getKeyCode()) && !keys.contains(e.getKeyCode())) singlekeys.push(e.getKeyCode());
        if (!keys.contains(e.getKeyCode())) keys.push(e.getKeyCode());
//        al.actionPerformed(new ActionEvent(this, 1, "Pressed"));
        doKeyCheck();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (keys.contains(e.getKeyCode())) keys.remove((Integer)e.getKeyCode());
        if (singlekeys.contains(e.getKeyCode())) singlekeys.remove((Integer)e.getKeyCode());
//        al.actionPerformed(new ActionEvent(this, 0, "Released"));
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            t.stop();
        } finally {
            super.finalize();
        }
    }
    
	/**
	 * Checks if the specified key is currently being pressed, but leaves its state so it can still be captured.
	 * @param key A constant from KeyEvent VKs
	 * @return 
	 */
    public boolean hasMulti(int key) {
        return keys.contains(key);
    }
    
	/**
	 * Checks if the specified key is currently being pressed, and consumes it if so.
	 * @param key A constant from KeyEvent VKs
	 * @return 
	 */
    public boolean hasSingle(int key) {
        return singlekeys.remove((Integer)key);
    }
	
	public List<Integer> getKeys() {
		return new ArrayList<>(keys);
	}
}
