package com.up.pe.mesh;

import com.up.pe.math.Point3D;
import com.up.pe.render.Material;
import com.up.pe.render.Vertex;

/**
 *
 * @author Ricky
 */
public class GroupableSimpleMesh extends SimpleMesh implements GroupableMesh {
    
    SafeMeshGroup parent = null;

    public GroupableSimpleMesh(SimpleMesh m) {
        super(m.verticies, m.mat,  m.offset, m.rot, m.scale);
    }

    @Override
    public void updateBuffers() {
//        for (int i = 0; i < verticies.length; i++) {
//			Vertex v = verticies[i];
//            Point3D s = v.getPosition().toVec().product(scale);
//            Point3D e = s.sum(v.getNormal());
//            if (parent != null) {
//				System.arraycopy(parent.transformChildPoint(s.rotate(rot).sum(offset)).toFloatArray(), 0, verts, i * 3, 3);
//				System.arraycopy(parent.transformChildPoint(e.rotate(rot)).difference(parent.transformChildPoint(s.rotate(rot))).toFloatArray(), 0, norms, i * 3, 3);
//            } else {
//				System.arraycopy(s.rotate(rot).sum(offset).toFloatArray(), 0, verts, i * 3, 3);
//				System.arraycopy(e.rotate(rot).difference(s.rotate(rot)).toFloatArray(), 0, norms, i * 3, 3);
//            }
//			System.arraycopy(new float[] {(float)v.getTexPosition().getX(), (float)v.getTexPosition().getY()}, 0, texs, i * 2, 2);
//			Material blend = parent == null || parent.getMaterial() == null ?
//					(mat == null ? v.getMaterial() : mat.blend(v.getMaterial(), 0.5f)) :
//					(mat == null ? parent.getMaterial().blend(v.getMaterial(), 0.5f) : parent.getMaterial().blend(mat.blend(v.getMaterial(), 0.5f), 0.5f));
//			System.arraycopy(blend.ambient, 0, ambs, i * 4, 4);
//			System.arraycopy(blend.diffuse, 0, difs, i * 4, 4);
//			System.arraycopy(blend.specular, 0, specs, i * 4, 4);
//			System.arraycopy(blend.emission, 0, emis, i * 4, 4);
//			shines[i] = blend.shininess;
//        }
        buffVerts.clear();
        buffNorms.clear();
        buffTexs.clear();
        buffAmbs.clear();
        buffDifs.clear();
        buffSpecs.clear();
        buffEmis.clear();
        buffShines.clear();
        for (Vertex v : verticies) {
            Point3D s = v.getPosition().toVec().product(scale);
            Point3D e = s.sum(v.getNormal());
            if (parent != null) {
                buffVerts.put(parent.transformChildPoint(s.rotate(rot).sum(offset)).toFloatArray());
                buffNorms.put(parent.transformChildPoint(e.rotate(rot)).difference(parent.transformChildPoint(s.rotate(rot))).toFloatArray());
            } else {
                buffVerts.put(s.rotate(rot).sum(offset).toFloatArray());
                buffNorms.put(e.rotate(rot).difference(s.rotate(rot)).toFloatArray());
            }
            buffTexs.put(new float[] {(float)v.getTexPosition().getX(), (float)v.getTexPosition().getY()});
			Material blend = parent == null || parent.getMaterial() == null ?
					(mat == null ? v.getMaterial() : mat.blend(v.getMaterial(), 0.5f)) :
					(mat == null ? parent.getMaterial().blend(v.getMaterial(), 0.5f) : parent.getMaterial().blend(mat.blend(v.getMaterial(), 0.5f), 0.5f));
            buffAmbs.put(blend.ambient);
            buffDifs.put(blend.diffuse);
            buffSpecs.put(blend.specular);
            buffEmis.put(blend.emission);
            buffShines.put(blend.shininess);
        }
        buffVerts.flip();
        buffNorms.flip();
        buffTexs.flip();
        buffAmbs.flip();
        buffDifs.flip();
        buffSpecs.flip();
        buffEmis.flip();
        buffShines.flip();
        swapBuffers();
        update = true;
    }

    @Override
    public Mesh getMesh() {
        return this;
    }
    
    @Override
    public void setParent(SafeMeshGroup g) {
        this.parent = g;
    }
}
