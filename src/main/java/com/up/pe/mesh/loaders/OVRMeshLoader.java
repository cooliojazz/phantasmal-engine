package com.up.pe.mesh.loaders;

import com.up.jovr.RenderModel;
import com.up.jovr.structs.HmdVector3_t;
import com.up.jovr.structs.RenderModel_Vertex_t;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.render.Material;
import com.up.pe.render.Texture;
import com.up.pe.render.Vertex;
import java.awt.Color;

/**
 *
 * @author Ricky
 */
public class OVRMeshLoader {
	
	public static SimpleMesh convert(RenderModel model, Point3D pos, Angle3D rot, Vector3D scale, Material mat) {
		int vCount = model.getModel().getIndexData().length;
		Vertex[] verticies = new Vertex[model.getModel().getIndexData().length];
		
		Material vMat = new Material(Color.black, Color.white, Color.black, 0.1f, new Texture(model.getTexture().getTextureAsImage()));
		for (int i = 0; i < vCount; i++) {
			RenderModel_Vertex_t v = model.getModel().getVertexData()[model.getModel().getIndexData()[i]];
			verticies[i] = new Vertex(pointFromVR(v.getPosition()), vectorFromVR(v.getNormal()), pointFromVR(v.getTextureCoord()), vMat);
		}
		
		return new SimpleMesh(verticies, mat, pos, rot, scale);
	}
	
	private static Point2D pointFromVR(float[] point) {
		return new Point2D(point[0], point[1]);
	}
	
	private static Point3D pointFromVR(HmdVector3_t point) {
		return new Point3D(point.getVector()[0], point.getVector()[1], point.getVector()[2]);
	}
	
	private static Vector3D vectorFromVR(HmdVector3_t vector) {
		return new Vector3D(vector.getVector()[0], vector.getVector()[1], vector.getVector()[2]);
	}
}
