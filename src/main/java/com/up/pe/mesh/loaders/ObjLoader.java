package com.up.pe.mesh.loaders;

import com.up.pe.render.Texture;
import com.up.pe.render.Vertex;
import com.up.pe.render.Material;
import com.up.pe.math.Point3D;
import com.up.pe.math.Point2D;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.mesh.Mesh;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author ricky.t
 */
public class ObjLoader {
    
    ArrayList<Group> groups = new ArrayList<>();

    public ObjLoader(File f) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(f));
        groups = new ArrayList<>();
        ArrayList<Vector3D> norms = new ArrayList<>();
        ArrayList<Point3D> points = new ArrayList<>();
        ArrayList<Point2D> texes = new ArrayList<>();
        Material defmat = new Material(Color.red, 0.25f);
        HashMap<String, Material> matlist = new HashMap<>();
        Group g = new Group(new ArrayList<>(), defmat);
        try {
            while (br.ready()) {
                String line = br.readLine();
                String[] parts = line.replaceAll("[ \t]+", " ").split(" ");
                switch (parts[0]) {
                    case "mtllib":
                        try {
                            matlist = getObjMaterials(new File(f.getParentFile(), join(parts, " ", 1)));
                        } catch (FileNotFoundException e) {
                            System.out.println("Material for model '" + f.getName() + "' is missing!");
                        }
                        break;
                    case "usemtl":
                        if (g.verts.size() > 0) groups.add(g);
                        g = new Group(new ArrayList<>(), defmat);
                        Material temp = matlist.get(parts[1]);
                        if (temp == null) temp = new Material(new float[] {(float)Math.random(), (float)Math.random(), (float)Math.random(), 1f}, Material.white, Material.black, 32, null);
                        g.mat = temp;
                        break;
                    case "g":
                        if (g.verts.size() > 0) groups.add(g);
                        g = new Group(new ArrayList<>(), defmat);
                        break;
                    case "v":
                        points.add(new Point3D(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), Double.parseDouble(parts[3])));
                        break;
                    case "vn":
                        norms.add(new Vector3D(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), Double.parseDouble(parts[3])));
                        break;
                    case "vt":
                        texes.add(new Point2D(Double.parseDouble(parts[1]), Double.parseDouble(parts[2])));
                        break;
                    case "f":
                        if (parts.length == 4) {
                            Vertex v1 = parseVertex(points, norms, texes, parts[1]);
                            Vertex v2 = parseVertex(points, norms, texes, parts[2]);
                            Vertex v3 = parseVertex(points, norms, texes, parts[3]);
                            if (v1.getNormal() == null || v2.getNormal() == null || v3.getNormal() == null) {
                                Vertex.addNormals(v1, v2, v3);
                            }
                            g.verts.add(v1);
                            g.verts.add(v2);
                            g.verts.add(v3);
                        } else {
                            int i = 1;
                            Vertex[] vs = new Vertex[parts.length - 1];
                            for (; i + 2 < parts.length; i += 3) {
                                vs[i - 1] = parseVertex(points, norms, texes, parts[i]);
                                vs[i] = parseVertex(points, norms, texes, parts[i + 1]);
                                vs[i + 1] = parseVertex(points, norms, texes, parts[i + 2]);
                                if (vs[i - 1].getNormal() == null || vs[i].getNormal() == null || vs[i + 1].getNormal() == null) {
                                    Vertex.addNormals(vs[i - 1], vs[i], vs[i + 1]);
                                }
                            }
                            if (i < parts.length + 2) {
                                i = parts.length - 2;
                                vs[i - 1] = parseVertex(points, norms, texes, parts[i]);
                                vs[i] = parseVertex(points, norms, texes, parts[i + 1]);
                                if (vs[i - 1].getNormal() == null || vs[i].getNormal() == null) {
                                    Vertex.addNormals(vs[i - 2], vs[i - 1], vs[i]);
                                }
                            }
                            g.verts.addAll(Arrays.asList(triangulate(vs)));
                        }
                        break;
                }
            }
            groups.add(g);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static Vertex parseVertex(ArrayList<Point3D> points, ArrayList<Vector3D> norms, ArrayList<Point2D> texes, String sv) {
        int pi = Integer.parseInt(sv.split("/")[0]);
        Vertex v = new Vertex(points.get(pi > 0 ? pi - 1 : points.size() + pi));
        if (sv.split("/").length > 1) {
            try {
                int ti = Integer.parseInt(sv.split("/")[1]);
                v.setTexPosition(texes.get(ti > 0 ? ti - 1 : texes.size() + ti));
            } catch (NumberFormatException ex) { }
        }
        if (sv.split("/").length > 2) {
            int ni = Integer.parseInt(sv.split("/")[2]);
            v.setNormal(new Vector3D(norms.get(ni > 0 ? ni - 1 : norms.size() + ni)).normalize());
        } else {
            v.setNormal(null);
        }
        return v;
    }
    
//    private double getDirectionOfVector(Vector3D vec, Vector3D axis) {
//        if (Math.signum(Math.atan(???)) < 0) {
//            
//        } else {
//            
//        }
//    }
    
    private static Vertex[] triangulate(Vertex[] facevs) {
        Point3D centerp = Point3D.zeros();
        Vector3D centern = Vector3D.zeros();
        Point2D centert = Point2D.zeros();
        for (Vertex v : facevs) {
            centerp = centerp.sum(v.getPosition());
            centern = centern.sum(v.getNormal());
            centert = centert.sum(v.getTexPosition());
        }
        Vertex center = new Vertex(centerp.mul(1d / facevs.length), centern.normalize(), centert.toVec().normalize(), facevs[0].getMaterial());
        Vertex[] tris = new Vertex[facevs.length * 3];
        for (int i = 0; i < facevs.length; i++) {
            tris[i * 3 + 0] = facevs[i];
            tris[i * 3 + 1] = center;
            tris[i * 3 + 2] = facevs[(i + 1) % facevs.length];
        }
        return tris;
    }
    
    public static String join(String[] s, String delim, int start) {
        if (start == s.length) return "";
        String ret = "";
        for (int i = start; i < s.length; i++) ret += s[i] + delim;
        return ret.substring(0, ret.length() - delim.length());
    }
    
    private static HashMap<String, Material> getObjMaterials(File f) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(f));
        HashMap<String, Material> matlist = new HashMap<>();
        String curn = null;
        try {
            while (br.ready()) {
                String line = br.readLine();
                String[] parts = line.replaceAll("[ \t]+", " ").split(" ");
                switch (parts[0]) {
                    case "newmtl":
                        curn = parts[1];
                        matlist.put(curn, new Material(Material.white, Material.white, Material.black, 32, null));
                        break;
                    case "Ka":
                        matlist.get(curn).diffuse = matlist.get(curn).ambient = new float[] {Float.parseFloat(parts[1]), Float.parseFloat(parts[2]), Float.parseFloat(parts[3]), 1f};
                        break;
                    case "Kd":
                        matlist.get(curn).diffuse = new float[] {Float.parseFloat(parts[1]), Float.parseFloat(parts[2]), Float.parseFloat(parts[3]), 1f};
                        break;
                    case "Ks":
                        matlist.get(curn).specular = new float[] {Float.parseFloat(parts[1]), Float.parseFloat(parts[2]), Float.parseFloat(parts[3]), 1f};
                        break;
                    case "Ke":
                        matlist.get(curn).emission = new float[] {Float.parseFloat(parts[1]), Float.parseFloat(parts[2]), Float.parseFloat(parts[3]), 1f};
                        break;
                    case "d":
                    case "Tr":
                        matlist.get(curn).ambient[3] = Float.parseFloat(parts[1]);
                        break;
                    case "map_Kd":
                        if (parts.length > 1) {
                            try {
                                matlist.get(curn).texture = new Texture(new FileInputStream(new File(f.getParentFile(), join(parts, " ", 1))));
                            } catch (FileNotFoundException e) {
                                System.out.println("Whoops! Non-existing texture wanted: '" + join(parts, " ", 1) + "'");
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matlist;
    }
    
    public List<SimpleMesh> create(Point3D pos, Angle3D rot, Vector3D scale) {
        return groups.stream().map(g -> new SimpleMesh(g.verts.toArray(new Vertex[0]), g.mat, pos, rot, scale)).collect(Collectors.toList());
    }
    
    public List<SimpleMesh> create(Point3D pos, Angle3D rot, Vector3D scale, Material m) {
        return groups.stream().map(g -> new SimpleMesh(g.verts.toArray(new Vertex[0]), m, pos, rot, scale)).collect(Collectors.toList());
    }
    
    private class Group {
        ArrayList<Vertex> verts;
        Material mat;

        public Group(ArrayList<Vertex> verts, Material mat) {
            this.verts = verts;
            this.mat = mat;
        }
        
    }
}
