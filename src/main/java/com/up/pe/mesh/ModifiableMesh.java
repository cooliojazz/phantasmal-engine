package com.up.pe.mesh;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Positionable;
import com.up.pe.math.Rotatable;
import com.up.pe.math.Vector3D;
import com.up.pe.render.Material;

/**
 *
 * @author Ricky
 */
public interface ModifiableMesh extends Mesh, Positionable, Rotatable {
    public Material getMaterial();
    public void setMaterial(Material mat);
    public Vector3D getScale();
    public void setScale(Vector3D scale);
    public Angle3D getRotation();
    public void setRotation(Angle3D rot);
	public void setSpatial(Point3D offset, Angle3D rot, Vector3D scale);
}
