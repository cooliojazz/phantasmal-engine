package com.up.pe.mesh;

/**
 *
 * @author Ricky
 */
public interface GroupableMesh {
    public Mesh getMesh();
    public void setParent(SafeMeshGroup g);
    public void updateBuffers();
}
