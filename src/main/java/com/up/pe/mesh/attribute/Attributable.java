package com.up.pe.mesh.attribute;

/**
 *
 * @author Ricky
 */
public interface Attributable {
    
    float[] getData();
    
}
