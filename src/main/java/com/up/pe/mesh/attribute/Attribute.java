package com.up.pe.mesh.attribute;

import java.util.function.Function;

/**
 *
 * @author Ricky
 */
public class Attribute<T> {
	
	int location;
	String name;
	int size;
	Function<T, Float[]> mapper;
	
}
