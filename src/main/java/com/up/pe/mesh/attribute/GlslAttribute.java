package com.up.pe.mesh.attribute;


/**
 *
 * @author Ricky
 */
public class GlslAttribute {
    
    private String name;

    public GlslAttribute(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public static enum STANDARD_SET {
        MATERIAL("material"), NORMAL("normal"), POSITION("position"), TEXTURE("texture");
        
        private GlslAttribute set;

        private STANDARD_SET(String name) {
            set = new GlslAttribute(name);
        }
        
    }
}