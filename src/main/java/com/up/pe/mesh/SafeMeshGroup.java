package com.up.pe.mesh;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL2;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.render.Material;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * MeshGroup which doesn't break with a large number of threaded updates
 * @author Ricky
 */
public class SafeMeshGroup implements Mesh, GroupableMesh, ModifiableMesh {
    
    private final Object lock = new Object();
    private MeshGroupState state = new MeshGroupState(new ArrayList<>());
    
    private int vao;
    private int vboverts;
    private int vbonorms;
    private int vbotexs;
    private int vboambs;
    private int vbodifs;
    private int vbospecs;
    private int vboemis;
    private int vboshines;
    private boolean vbos = false;
    protected Material mat;
    private Point3D offset;
    private Angle3D rot;
    private Vector3D scale;
    private SafeMeshGroup parent = null;

    public SafeMeshGroup(Material mat, Point3D offset, Angle3D rot, Vector3D scale) {
		this.mat = mat;
        this.offset = offset;
        this.rot = rot;
        this.scale = scale;
    }
    
    @Override
    public int getVertexCount() {
        return state.getVertexCount();
    }

    @Override
    public FloatBuffer getVertexBuffer() {
        return state.getVertexBuffer();
    }

    @Override
    public FloatBuffer getNormalBuffer() {
        return state.getNormalBuffer();
    }

    @Override
    public FloatBuffer getTextureBuffer() {
        return state.getTextureBuffer();
    }

    @Override
    public FloatBuffer getAmbientBuffer() {
        return state.getAmbientBuffer();
    }

    @Override
    public FloatBuffer getDiffuseBuffer() {
        return state.getDiffuseBuffer();
    }

    @Override
    public FloatBuffer getSpecularBuffer() {
        return state.getSpecularBuffer();
    }

    @Override
    public FloatBuffer getEmissionBuffer() {
        return state.getEmissionBuffer();
    }

    @Override
    public FloatBuffer getShineBuffer() {
        return state.getShineBuffer();
    }
    
    public void addMesh(GroupableMesh m) {
        synchronized (lock) {
            m.setParent(this);
            m.updateBuffers();
            List<GroupableMesh> meshes = new ArrayList<>(getMeshes());
            meshes.add(m);
            state = new MeshGroupState(meshes);
        }
    }
    
    public void removeMesh(GroupableMesh m) {
        synchronized (lock) {
            m.setParent(null);
            m.updateBuffers();
            List<GroupableMesh> meshes = new ArrayList<>(getMeshes());
            meshes.remove(m);
            state = new MeshGroupState(meshes);
        }
    }
    
    public List<GroupableMesh> getChildren() {
        return new ArrayList<>(getMeshes());
    }

    @Override
    public int getVertexArray() {
        return vao;
    }
    
	@Override
    public void createVBOs(GL2 gl) {
        IntBuffer id = IntBuffer.allocate(8);
        
        gl.glGenBuffers(8, id);
        vboverts = id.get(0);
        vbonorms = id.get(1);
        vbotexs = id.get(2);
        vboambs = id.get(3);
        vbodifs = id.get(4);
        vbospecs = id.get(5);
        vboemis = id.get(6);
        vboshines = id.get(7);
        
        gl.glGenVertexArrays(1, id);
        vao = id.get(0);
		
		gl.glBindVertexArray(vao);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboverts);
        gl.glVertexAttribPointer(0, 3, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbonorms);
        gl.glVertexAttribPointer(1, 3, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(1);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbotexs);
        gl.glVertexAttribPointer(2, 2, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(2);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboambs);
        gl.glVertexAttribPointer(3, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(3);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbodifs);
        gl.glVertexAttribPointer(4, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(4);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbospecs);
        gl.glVertexAttribPointer(5, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(5);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboemis);
        gl.glVertexAttribPointer(6, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(6);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboshines);
        gl.glVertexAttribPointer(7, 1, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(7);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
		gl.glBindVertexArray(0);
        
        vbos = true;
        updateVBOs(gl);
    }

	@Override
	public void cleanupVBOs(GL2 gl) {
		gl.glDeleteVertexArrays(1, new int[] {vao}, 0);
		gl.glDeleteBuffers(8, new int[] {vboverts, vbonorms, vbotexs, vboambs, vbodifs, vbospecs, vboemis, vboshines}, 0);
	}
    
    @Override
    public void updateVBOs(GL2 gl) {
        MeshGroupState safeState = state;
		gl.glBindVertexArray(vao);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboverts);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 3 * 4, safeState.getVertexBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbonorms);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 3 * 4, safeState.getNormalBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbotexs);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 2 * 4, safeState.getTextureBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboambs);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, safeState.getAmbientBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbodifs);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, safeState.getDiffuseBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbospecs);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, safeState.getSpecularBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboemis);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, safeState.getEmissionBuffer(), GL2.GL_STATIC_DRAW);
		gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboshines);
		gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 1 * 4, safeState.getShineBuffer(), GL2.GL_STATIC_DRAW);
    }

    @Override
    public boolean hasVBOs() {
        return vbos;
    }

    @Override
    public boolean needsUpdate() {
        return state.meshes.stream().anyMatch(m -> m.getMesh().needsUpdate());
    }
    
    public Point3D transformChildPoint(Point3D p) {
        if (parent != null) {
            return parent.transformChildPoint(p.toVec().product(scale).rotate(rot).sum(offset));
        } else {
            return p.toVec().product(scale).rotate(rot).sum(offset);
        }
    }

    @Override
    public void setParent(SafeMeshGroup g) {
        parent = g;
    }

    @Override
    public Mesh getMesh() {
        return this;
    }

	@Override
	public Material getMaterial() {
		return mat;
	}

	@Override
	public void setMaterial(Material mat) {
		this.mat = mat;
        updateBuffers();
	}
    
    @Override
    public Angle3D getRotation() {
        return rot;
    }
    
    @Override
    public void setRotation(Angle3D rot) {
        this.rot = rot;
        updateBuffers();
    }

    @Override
    public Vector3D getScale() {
        return scale;
    }

    @Override
    public void setScale(Vector3D scale) {
        this.scale = scale;
        updateBuffers();
    }

    @Override
    public Point3D getPosition() {
        return offset;
    }

    @Override
    public void setPosition(Point3D offset) {
        this.offset = offset;
        updateBuffers();
    }
   
	@Override
    public void setSpatial(Point3D offset, Angle3D rot, Vector3D scale) {
        this.offset = offset;
        this.rot = rot;
        this.scale = scale;
        updateBuffers();
    }

    @Override
    public void updateBuffers() {
        state.updateBuffers();
    }
    
    public List<GroupableMesh> getMeshes() {
        return state.meshes;
    }
    
    private static class MeshGroupState {
    
        final List<GroupableMesh> meshes;
        final int vertc;
        FloatBuffer verts;
        FloatBuffer norms;
        FloatBuffer texs;
        FloatBuffer ambs;
        FloatBuffer difs;
        FloatBuffer specs;
        FloatBuffer emis;
        FloatBuffer shines;
        
        public MeshGroupState(List<GroupableMesh> meshes) {
            this.meshes = Collections.unmodifiableList(meshes);
            vertc = this.meshes.stream().mapToInt(m -> m.getMesh().getVertexCount()).sum();
            updateBuffers();
        }
        
        public int getVertexCount() {
            return vertc;
        }
        
        private void updateBuffers() {
            for (GroupableMesh gm : meshes) gm.updateBuffers();
            verts = Buffers.newDirectFloatBuffer(vertc * 3);
            norms = Buffers.newDirectFloatBuffer(vertc * 3);
            texs = Buffers.newDirectFloatBuffer(vertc * 2);
            ambs = Buffers.newDirectFloatBuffer(vertc * 4);
            difs = Buffers.newDirectFloatBuffer(vertc * 4);
            specs = Buffers.newDirectFloatBuffer(vertc * 4);
            emis = Buffers.newDirectFloatBuffer(vertc * 4);
            shines = Buffers.newDirectFloatBuffer(vertc * 1);
            for (GroupableMesh m : meshes) {
				if (m instanceof SafeMeshGroup) {
					MeshGroupState mState = ((SafeMeshGroup)m).state;
					verts.put(mState.getVertexBuffer());
					norms.put(mState.getNormalBuffer());
					texs.put(mState.getTextureBuffer());
					ambs.put(mState.getAmbientBuffer());
					difs.put(mState.getDiffuseBuffer());
					specs.put(mState.getSpecularBuffer());
					emis.put(mState.getEmissionBuffer());
					shines.put(mState.getShineBuffer());
				} else {
					verts.put(m.getMesh().getVertexBuffer());
					norms.put(m.getMesh().getNormalBuffer());
					texs.put(m.getMesh().getTextureBuffer());
					ambs.put(m.getMesh().getAmbientBuffer());
					difs.put(m.getMesh().getDiffuseBuffer());
					specs.put(m.getMesh().getSpecularBuffer());
					emis.put(m.getMesh().getEmissionBuffer());
					shines.put(m.getMesh().getShineBuffer());
				}
            }
            verts.rewind();
            norms.rewind();
            texs.rewind();
            ambs.rewind();
            difs.rewind();
            specs.rewind();
            emis.rewind();
            shines.rewind();
        }

		public FloatBuffer getVertexBuffer() {
			verts.rewind();
			return verts;
		}

		public FloatBuffer getNormalBuffer() {
			norms.rewind();
			return norms;
		}

		public FloatBuffer getTextureBuffer() {
			texs.rewind();
			return texs;
		}

		public FloatBuffer getAmbientBuffer() {
			ambs.rewind();
			return ambs;
		}

		public FloatBuffer getDiffuseBuffer() {
			difs.rewind();
			return difs;
		}

		public FloatBuffer getSpecularBuffer() {
			specs.rewind();
			return specs;
		}

		public FloatBuffer getEmissionBuffer() {
			emis.rewind();
			return emis;
		}

		public FloatBuffer getShineBuffer() {
			shines.rewind();
			return shines;
		}
        
    }

	@Override
	public int getTexture() {
		if (mat != null && mat.texture != null) {
			return mat.texture.getID();
		}
		if (state.meshes.size() > 0) {
			return state.meshes.get(0).getMesh().getTexture();
		}
		return -1;
	}
}
