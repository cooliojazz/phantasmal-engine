package com.up.pe.mesh;

import com.up.pe.math.Angle3D;
import com.up.pe.render.Material;
import com.up.pe.render.Texture;
import com.up.pe.render.Vertex;
import java.util.stream.Stream;

/**
 *
 * @author Ricky
 */
public class AnimateMesh extends SimpleMesh {
	
    private SimpleMesh startMesh;
    private SimpleMesh endMesh;
    private int length;
    private int time = 0;
    private boolean loop;

    public AnimateMesh(SimpleMesh startMesh, SimpleMesh endMesh, int length, boolean loop) {
		super(Stream.of(startMesh.getVerticies()).map(Vertex::clone).toArray(Vertex[]::new), null /* TODO: this should be animated too */, startMesh.getPosition(), startMesh.getRotation(), startMesh.getScale());
        if (startMesh.getVerticies().length != endMesh.getVerticies().length) throw new ArrayIndexOutOfBoundsException("Meshes must be same size!");
        this.startMesh = startMesh;
        this.endMesh = endMesh;
        this.length = length;
        this.loop = loop;
    }
    
    public boolean isAnimationDone() {
        return time >= length && !loop;
    }
    
    public void reset() {
        time = 0;
    }
    
    public void frame() {
        if (!isAnimationDone()) {
            double inter = (double)(time % length) / length;
            for (int i = 0; i < startMesh.getVertexCount(); i++) {
				calculateVertex(startMesh.getVerticies()[i], endMesh.getVerticies()[i], inter, getVerticies()[i]);
			}
			setSpatial(startMesh.getPosition().sum(startMesh.getPosition().vectorTo(endMesh.getPosition()).mul(inter)),
						new Angle3D(startMesh.getRotation().sum(startMesh.getRotation().vectorTo(endMesh.getRotation()).mul(inter))),
						startMesh.getScale().sum(startMesh.getScale().vectorTo(endMesh.getScale()).mul(inter)));
			setMaterial(startMesh.getMaterial().blend(endMesh.getMaterial(), (float)inter));
        }
        time++;
    }
    
    private void calculateVertex(Vertex start, Vertex end, double inter, Vertex vf) {
        vf.setPosition(start.getPosition().sum(start.getPosition().vectorTo(end.getPosition()).mul(inter)));
		vf.setMaterial(start.getMaterial().blend(end.getMaterial(), (float)inter));
    }
}
