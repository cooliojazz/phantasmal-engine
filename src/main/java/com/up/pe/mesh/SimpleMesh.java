package com.up.pe.mesh;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL2;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Line;
import com.up.pe.render.Material;
import com.up.pe.math.Point3D;
import com.up.pe.math.Triangle;
import com.up.pe.math.Vector3D;
import com.up.pe.render.Vertex;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 *
 * @author Ricky
 */
public class SimpleMesh implements ModifiableMesh {
	
    protected Vertex[] verticies;
//    protected float[] verts;
//    protected float[] norms;
//    protected float[] texs;
//    protected float[] ambs;
//    protected float[] difs;
//    protected float[] specs;
//    protected float[] emis;
//    protected float[] shines;
    protected FloatBuffer verts;
    protected FloatBuffer norms;
    protected FloatBuffer texs;
    protected FloatBuffer ambs;
    protected FloatBuffer difs;
    protected FloatBuffer specs;
    protected FloatBuffer emis;
    protected FloatBuffer shines;
    protected FloatBuffer buffVerts;
    protected FloatBuffer buffNorms;
    protected FloatBuffer buffTexs;
    protected FloatBuffer buffAmbs;
    protected FloatBuffer buffDifs;
    protected FloatBuffer buffSpecs;
    protected FloatBuffer buffEmis;
    protected FloatBuffer buffShines;
    private int vboVerts;
    private int vboNorms;
    private int vboTexs;
    private int vboAmbs;
    private int vboDifs;
    private int vboSpecs;
    private int vboEmis;
    private int vboShines;
    protected Material mat;
    protected Point3D offset;
    protected Angle3D rot;
    protected Vector3D scale;
    private int vao;
    private boolean vbos = false;
    protected boolean update = false;
    
    private ArrayList<Triangle> tris = new ArrayList();
    
    public SimpleMesh(Vertex[] verticies, Material mat, Point3D offset, Angle3D rot, Vector3D scale) {
		this.mat = mat;
        this.verticies = verticies;
        this.offset = offset;
        this.rot = rot;
        this.scale = scale;
        updateVerticies();
    }
    
    private void updateVerticies() {
//        verts = new float[verticies.length * 3];
//        norms = new float[verticies.length * 3];
//        texs = new float[verticies.length * 2];
//        ambs = new float[verticies.length * 4];
//        difs = new float[verticies.length * 4];
//        specs = new float[verticies.length * 4];
//        emis = new float[verticies.length * 4];
//        shines = new float[verticies.length * 1];
//        buffVerts = FloatBuffer.wrap(verts);
//        buffNorms = FloatBuffer.wrap(norms);
//        buffTexs = FloatBuffer.wrap(texs);
//        buffAmbs = FloatBuffer.wrap(ambs);
//        buffDifs = FloatBuffer.wrap(difs);
//        buffSpecs = FloatBuffer.wrap(specs);
//        buffEmis = FloatBuffer.wrap(emis);
//        buffShines = FloatBuffer.wrap(shines);
        verts = Buffers.newDirectFloatBuffer(verticies.length * 3);
        norms = Buffers.newDirectFloatBuffer(verticies.length * 3);
        texs = Buffers.newDirectFloatBuffer(verticies.length * 2);
        ambs = Buffers.newDirectFloatBuffer(verticies.length * 4);
        difs = Buffers.newDirectFloatBuffer(verticies.length * 4);
        specs = Buffers.newDirectFloatBuffer(verticies.length * 4);
        emis = Buffers.newDirectFloatBuffer(verticies.length * 4);
        shines = Buffers.newDirectFloatBuffer(verticies.length * 1);
        buffVerts = Buffers.newDirectFloatBuffer(verticies.length * 3);
        buffNorms = Buffers.newDirectFloatBuffer(verticies.length * 3);
        buffTexs = Buffers.newDirectFloatBuffer(verticies.length * 2);
        buffAmbs = Buffers.newDirectFloatBuffer(verticies.length * 4);
        buffDifs = Buffers.newDirectFloatBuffer(verticies.length * 4);
        buffSpecs = Buffers.newDirectFloatBuffer(verticies.length * 4);
        buffEmis = Buffers.newDirectFloatBuffer(verticies.length * 4);
        buffShines = Buffers.newDirectFloatBuffer(verticies.length * 1);
        updateBuffers();
        tris = new ArrayList<>();
        for (int i = 0; i < verticies.length; i += 3) tris.add(new Triangle(verticies[i].getPosition(), verticies[i + 1].getPosition(), verticies[i + 2].getPosition()));
    }

	@Override
	public Material getMaterial() {
		return mat;
	}

	@Override
	public void setMaterial(Material mat) {
		this.mat = mat;
        updateBuffers();
	}

    @Override
    public Vector3D getScale() {
        return scale;
    }

    @Override
    public void setScale(Vector3D scale) {
        this.scale = scale;
        updateBuffers();
    }

    @Override
    public Point3D getPosition() {
        return offset;
    }

    @Override
    public void setPosition(Point3D offset) {
        this.offset = offset;
        updateBuffers();
    }
    
    @Override
    public Angle3D getRotation() {
        return rot;
    }
    
    @Override
    public void setRotation(Angle3D rot) {
        this.rot = rot;
        updateBuffers();
    }
    /**
     * Use instead of the individual set(Position/Rotation/Scale) when using multiple at a time to conserve buffer updates
     */
    @Override
    public void setSpatial(Point3D offset, Angle3D rot, Vector3D scale) {
        this.offset = offset;
        this.rot = rot;
        this.scale = scale;
        updateBuffers();
    }

    public void setSpatial(Point3D offset, Angle3D rot) {
        setSpatial(offset, rot, scale);
    }
    
    public Vertex[] getVerticies() {
        return verticies;
    }
    
    public void setVerticies(Vertex[] verticies) {
        this.verticies = verticies;
        updateVerticies();
    }
    
//    protected void updateBuffers() {
//        for (int i = 0; i < verticies.length; i++) {
//			Vertex v = verticies[i];
//            Point3D s = v.getPosition().toVec().product(scale);
//            Point3D e = s.sum(v.getNormal());
//			System.arraycopy(s.rotate(rot).sum(offset).toFloatArray(), 0, verts, i * 3, 3);
//			System.arraycopy(e.rotate(rot).difference(s.rotate(rot)).toFloatArray(), 0, norms, i * 3, 3);
//			System.arraycopy(new float[] {(float)v.getTexPosition().getX(), (float)v.getTexPosition().getY()}, 0, texs, i * 2, 2);
//			Material blend = mat == null ? v.getMaterial() : mat.blend(v.getMaterial(), 0.5f);
//			System.arraycopy(blend.ambient, 0, ambs, i * 4, 4);
//			System.arraycopy(blend.diffuse, 0, difs, i * 4, 4);
//			System.arraycopy(blend.specular, 0, specs, i * 4, 4);
//			System.arraycopy(blend.emission, 0, emis, i * 4, 4);
//			shines[i] = blend.shininess;
//        }
    protected synchronized void updateBuffers() {
		buffVerts.clear();
		buffNorms.clear();
		buffTexs.clear();
		buffAmbs.clear();
		buffDifs.clear();
		buffSpecs.clear();
		buffEmis.clear();
		buffShines.clear();
		for (Vertex v : verticies) {
			Point3D s = v.getPosition().toVec().product(scale);
			Point3D e = s.sum(v.getNormal());
			buffVerts.put(s.rotate(rot).sum(offset).toFloatArray());
			buffNorms.put(e.rotate(rot).difference(s.rotate(rot)).toFloatArray());
			buffTexs.put(new float[] {(float)v.getTexPosition().getX(), (float)v.getTexPosition().getY()});
			Material blend = mat == null ? v.getMaterial() : (v.getMaterial() == null ? mat : mat.blend(v.getMaterial(), 0.5f));
			buffAmbs.put(blend.ambient);
			buffDifs.put(blend.diffuse);
			buffSpecs.put(blend.specular);
			buffEmis.put(blend.emission);
			buffShines.put(blend.shininess);
		}
		buffVerts.flip();
		buffNorms.flip();
		buffTexs.flip();
		buffAmbs.flip();
		buffDifs.flip();
		buffSpecs.flip();
		buffEmis.flip();
		buffShines.flip();
		swapBuffers();
		update = true;
    }
    
    protected void swapBuffers() {
        FloatBuffer tmpverts = verts;
        FloatBuffer tmpnorms = norms;
        FloatBuffer tmptexs = texs;
        FloatBuffer tmpambs = ambs;
        FloatBuffer tmpdifs = difs;
        FloatBuffer tmpspecs = specs;
        FloatBuffer tmpemis = emis;
        FloatBuffer tmpshines = shines;
        verts = buffVerts;
        norms = buffNorms;
        texs = buffTexs;
        ambs = buffAmbs;
        difs = buffDifs;
        specs = buffSpecs;
        emis = buffEmis;
        shines = buffShines;
        buffVerts = tmpverts;
        buffNorms = tmpnorms;
        buffTexs = tmptexs;
        buffAmbs = tmpambs;
        buffDifs = tmpdifs;
        buffSpecs = tmpspecs;
		  buffEmis = tmpemis;
        buffShines = tmpshines;
    }
    
    public Stream<Vertex> stream() {
        return Stream.of(verticies);
    }

    @Override
    public FloatBuffer getVertexBuffer() {
//        buffVerts.rewind();
//        return buffVerts;
        verts.rewind();
        return verts;
    }

    @Override
    public FloatBuffer getNormalBuffer() {
//        buffNorms.rewind();
//        return buffNorms;
        norms.rewind();
        return norms;
    }

    @Override
    public FloatBuffer getTextureBuffer() {
//        buffTexs.rewind();
//        return buffTexs;
        texs.rewind();
        return texs;
    }

    @Override
    public FloatBuffer getAmbientBuffer() {
//        buffAmbs.rewind();
//        return buffAmbs;
        ambs.rewind();
        return ambs;
    }

    @Override
    public FloatBuffer getDiffuseBuffer() {
//        buffDifs.rewind();
//        return buffDifs;
        difs.rewind();
        return difs;
    }

    @Override
    public FloatBuffer getSpecularBuffer() {
//        buffSpecs.rewind();
//        return buffSpecs;
        specs.rewind();
        return specs;
    }

    @Override
    public FloatBuffer getEmissionBuffer() {
//        buffEmis.rewind();
//        return buffEmis;
        emis.rewind();
        return emis;
    }

    @Override
    public FloatBuffer getShineBuffer() {
//        buffShines.rewind();
//        return buffShines;
        shines.rewind();
        return shines;
    }
    
    @Override
    public int getVertexCount() {
        return verticies.length;
    }
    
    public boolean intersects(Function<Triangle, Boolean> intersector) {
        return tris.stream().anyMatch(tri -> intersector.apply(tri.scale(scale).move(offset)));
//        return tris.parallelStream().anyMatch(tri -> intersector.apply(tri.scale(scale).move(offset)));
    }
    
    public boolean intersects(Line<Point3D> l) {
        return intersects(tri -> tri.intersects(l));
    }
    
    public Point3D intersectsAt(Line<Point3D> l) {
        return tris.parallelStream().filter(tri -> tri.intersects(l)).findFirst().get().intersectsAt(l);
    }
    
    public boolean intersects(Triangle t) {
        return intersects(tri -> tri.intersects(t));
    }
    
    public boolean streamlessIntersects(Triangle t) {
        for (Triangle tri : tris) {
            if (t.intersects(tri.scale(scale).move(offset))) return true;
        }
        return false;
    }
    
    public boolean collides(SimpleMesh m) {
        return intersects(tri -> m.intersects(tri));
    }
    
    public boolean streamlessCollides(SimpleMesh m) {
        for (Triangle tri : tris) {
            if (m.streamlessIntersects(tri.scale(scale).move(offset))) return true;
        }
        return false;
    }
    
    @Override
    public void createVBOs(GL2 gl) {
        IntBuffer id = IntBuffer.allocate(8);
        
        gl.glGenBuffers(8, id);
        vboVerts = id.get(0);
        vboNorms = id.get(1);
        vboTexs = id.get(2);
        vboAmbs = id.get(3);
        vboDifs = id.get(4);
        vboSpecs = id.get(5);
        vboEmis = id.get(6);
        vboShines = id.get(7);
        
        gl.glGenVertexArrays(1, id);
        vao = id.get(0);
		
		gl.glBindVertexArray(vao);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboVerts);
        gl.glVertexAttribPointer(0, 3, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboNorms);
        gl.glVertexAttribPointer(1, 3, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(1);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboTexs);
        gl.glVertexAttribPointer(2, 2, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(2);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboAmbs);
        gl.glVertexAttribPointer(3, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(3);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboDifs);
        gl.glVertexAttribPointer(4, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(4);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboSpecs);
        gl.glVertexAttribPointer(5, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(5);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboEmis);
        gl.glVertexAttribPointer(6, 4, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(6);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboShines);
        gl.glVertexAttribPointer(7, 1, GL2.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(7);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, 0);
		gl.glBindVertexArray(0);
        
        vbos = true;
        updateVBOs(gl);
    }

	@Override
	public void cleanupVBOs(GL2 gl) {
		gl.glDeleteVertexArrays(1, new int[] {vao}, 0);
		gl.glDeleteBuffers(8, new int[] {vboVerts, vboNorms, vboTexs, vboAmbs, vboDifs, vboSpecs, vboEmis, vboShines}, 0);
	}
    
    @Override
    public void updateVBOs(GL2 gl) {
        gl.glBindVertexArray(vao);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboVerts);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 3 * 4, getVertexBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboNorms);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 3 * 4, getNormalBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboTexs);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 2 * 4, getTextureBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboAmbs);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getAmbientBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboDifs);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getDiffuseBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboSpecs);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getSpecularBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboEmis);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getEmissionBuffer(), GL2.GL_STATIC_DRAW);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboShines);
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 1 * 4, getShineBuffer(), GL2.GL_STATIC_DRAW);
        update = false;
    }
    
    @Override
    public boolean hasVBOs() {
        return vbos;
    }
    
    @Override
    public int getVertexArray() {
        return vao;
    }

    @Override
    public boolean needsUpdate() {
        return update;
    }

	@Override
	public int getTexture() {
		if (mat != null && mat.texture != null) {
			return mat.texture.getID();
		}
		if (verticies.length > 0 && verticies[0].getMaterial().texture != null) {
			return verticies[0].getMaterial().texture.getID();
		}
		return -1;
	}
}
