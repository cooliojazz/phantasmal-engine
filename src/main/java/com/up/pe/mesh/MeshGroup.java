package com.up.pe.mesh;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL2;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Ricky
 */
//public class MeshGroup implements Mesh, GroupableMesh, ModifiableMesh {
//    
//    private CopyOnWriteArrayList<GroupableMesh> meshes = new CopyOnWriteArrayList<>();
//    private int vcount = 0;
//    
//    private int vao;
//    private int vboverts;
//    private int vbonorms;
//    private int vbotexs;
//    private int vboambs;
//    private int vbodifs;
//    private int vbospecs;
//    private int vboshines;
//    protected FloatBuffer verts;
//    protected FloatBuffer norms;
//    protected FloatBuffer texs;
//    protected FloatBuffer ambs;
//    protected FloatBuffer difs;
//    protected FloatBuffer specs;
//    protected FloatBuffer shines;
//    private boolean vbos = false;
//    private Point3D offset;
//    private Angle3D rot;
//    private Vector3D scale;
//    private MeshGroup parent = null;
//
//    public MeshGroup(Point3D offset, Angle3D rot, Vector3D scale) {
//        this.offset = offset;
//        this.rot = rot;
//        this.scale = scale;
//    }
//    
//    @Override
//    public int getVertexCount() {
//        return meshes.stream().map(m -> m.getMesh().getVertexCount()).reduce(0, (m1, m2) -> m1 + m2);
//    }
//    
//    @Override
//    public void updateVBO(GL2 gl) {
//        gl.glBindVertexArray(vao);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboverts);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 3 * 4, getVertexBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(0, 3, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(0);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbonorms);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 3 * 4, getNormalBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(1, 3, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(1);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbotexs);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 2 * 4, getTextureBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(2, 2, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(2);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboambs);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getAmbientBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(3, 4, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(3);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbodifs);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getDiffuseBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(4, 4, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(4);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vbospecs);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 4 * 4, getSpecularBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(5, 4, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(5);
//        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, vboshines);
//        gl.glBufferData(GL2.GL_ARRAY_BUFFER, getVertexCount() * 1 * 4, getShineBuffer(), GL2.GL_STATIC_DRAW);
//        gl.glVertexAttribPointer(6, 1, GL2.GL_FLOAT, false, 0, 0);
//        gl.glEnableVertexAttribArray(6);
//    }
//
//    @Override
//    public FloatBuffer getVertexBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 3);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getVertexBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//
//    @Override
//    public FloatBuffer getNormalBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 3);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getNormalBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//
//    @Override
//    public FloatBuffer getTextureBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 2);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getTextureBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//
//    @Override
//    public FloatBuffer getAmbientBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 4);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getAmbientBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//
//    @Override
//    public FloatBuffer getDiffuseBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 4);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getDiffuseBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//
//    @Override
//    public FloatBuffer getSpecularBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 4);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getSpecularBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//
//    @Override
//    public FloatBuffer getShineBuffer() {
//        FloatBuffer fb = Buffers.newDirectFloatBuffer(vcount * 1);
//        for (GroupableMesh m : meshes) {
//            fb.put(m.getMesh().getShineBuffer());
//        }
//        fb.rewind();
//        return fb;
//    }
//    
//    public void addMesh(GroupableMesh m) {
//        m.setParent(this);
//        m.updateBuffers();
//        meshes.add(m);
//        vcount = getVertexCount();
//    }
//    
//    public void removeMesh(GroupableMesh m) {
//        m.setParent(null);
//        m.updateBuffers();
//        meshes.remove(m);
//        vcount = getVertexCount();
//    }
//
//    @Override
//    public int getVertexArray() {
//        return vao;
//    }
//    
//    public void createVBO(GL2 gl) {
//        IntBuffer id = IntBuffer.allocate(7);
//        
//        gl.glGenBuffers(7, id);
//        vboverts = id.get(0);
//        vbonorms = id.get(1);
//        vbotexs = id.get(2);
//        vboambs = id.get(3);
//        vbodifs = id.get(4);
//        vbospecs = id.get(5);
//        vboshines = id.get(6);
//        
//        gl.glGenVertexArrays(1, id);
//        vao = id.get(0);
//        
//        vbos = true;
//        updateVBO(gl);
//    }
//
//    @Override
//    public boolean hasVBOs() {
//        return vbos;
//    }
//
//    @Override
//    public boolean needsUpdate() {
//        return meshes.stream().anyMatch(m -> m.getMesh().needsUpdate());
//    }
//    
//    public Point3D transformChildPoint(Point3D p) {
//        if (parent != null) {
//            return parent.transformChildPoint(p.toVec().product(scale).rotate(rot).sum(offset));
//        } else {
//            return p.toVec().product(scale).rotate(rot).sum(offset);
//        }
//    }
//
//    @Override
//    public void setParent(MeshGroup g) {
//        parent = g;
//    }
//
//    @Override
//    public Mesh getMesh() {
//        return this;
//    }
//    
//    @Override
//    public Angle3D getRotation() {
//        return rot;
//    }
//    
//    @Override
//    public void setRotation(Angle3D rot) {
//        this.rot = rot;
//        updateBuffers();
//    }
//
//    @Override
//    public Vector3D getScale() {
//        return scale;
//    }
//
//    @Override
//    public void setScale(Vector3D scale) {
//        this.scale = scale;
//        updateBuffers();
//    }
//
//    @Override
//    public Point3D getPosition() {
//        return offset;
//    }
//
//    @Override
//    public void setPosition(Point3D offset) {
//        this.offset = offset;
//        updateBuffers();
//    }
//
//    @Override
//    public void updateBuffers() {
//        for (GroupableMesh gm : meshes) gm.updateBuffers();
//    }
//    
//    public List<GroupableMesh> getMeshes() {
//        return meshes;
//    }
//}
