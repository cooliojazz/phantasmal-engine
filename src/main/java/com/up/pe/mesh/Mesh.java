package com.up.pe.mesh;

import com.jogamp.opengl.GL2;
import java.nio.FloatBuffer;

/**
 *
 * @author Ricky
 */
public interface Mesh {
    
    public int getVertexArray();
    public int getVertexCount();
//    public Material getMaterial();
    
    public FloatBuffer getVertexBuffer();
    public FloatBuffer getNormalBuffer();
//    public FloatBuffer getTextureOffsetBuffer();
    public FloatBuffer getTextureBuffer();
    public FloatBuffer getAmbientBuffer();
    public FloatBuffer getDiffuseBuffer();
    public FloatBuffer getSpecularBuffer();
    public FloatBuffer getEmissionBuffer();
    public FloatBuffer getShineBuffer();
	
//	public AttributeBuffers getBufferSupport();
//	public FloatBuffer getBuffer(AttributeBuffers type);
    
    public void updateVBOs(GL2 gl);
    public boolean hasVBOs();
    public void createVBOs(GL2 gl);
    public void cleanupVBOs(GL2 gl);
    public boolean needsUpdate();
    
	/**
	 * -1 if not using a texture
	 */
    public int getTexture();
}
