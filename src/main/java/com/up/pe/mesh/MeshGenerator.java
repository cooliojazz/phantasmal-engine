package com.up.pe.mesh;

import com.up.pe.math.Angle3D;
import com.up.pe.render.Material;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.render.Vertex;
import java.awt.Color;
import java.util.HashMap;

/**
 *
 * @author Ricky
 */
public class MeshGenerator {
    public static SimpleMesh triangle(Material mat, Point3D p1, Point3D p2, Point3D p3) {
        Vector3D norm = p1.vectorTo(p2).crossProduct(p1.vectorTo(p3)).normalize();
        return triangle(mat, p1, p2, p3, norm, norm, norm);
    }
    
    public static SimpleMesh triangleCurve(Material mat, Point3D p1, Point3D p2, Point3D p3) {
        return triangle(mat, p1, p2, p3, p1.toVec(), p2.toVec(), p3.toVec());
    }
    
    public static SimpleMesh triangle(Material mat, Point3D p1, Point3D p2, Point3D p3, Point2D t1, Point2D t2, Point2D t3) {
        Vector3D norm = p1.vectorTo(p2).crossProduct(p1.vectorTo(p3)).normalize();
        return triangle(mat, p1, p2, p3, norm, norm, norm, t1, t2, t3);
    }
    
    public static SimpleMesh triangle(Material mat, Point3D p1, Point3D p2, Point3D p3, Vector3D n1, Vector3D n2, Vector3D n3) {
        return triangle(mat, p1, p2, p3, n1, n2, n3, new Point2D(0, 0), new Point2D(0, 0), new Point2D(0, 0));
    }
    
    public static SimpleMesh triangle(Material mat, Point3D p1, Point3D p2, Point3D p3, Vector3D n1, Vector3D n2, Vector3D n3, Point2D t1, Point2D t2, Point2D t3) {
        return new SimpleMesh(
            new Vertex[] {
                new Vertex(p1, n1, t1, mat),
                new Vertex(p2, n2, t2, mat),
                new Vertex(p3, n3, t3, mat)
            },
            mat, new Point3D(0, 0, 0), new Angle3D(0, 0, 0), new Vector3D(1, 1, 1)
        );
    }
    
    public static SimpleMesh plane(Material mat, Point3D pos, Angle3D rot, Vector3D scale) {
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(triangle(mat, new Point3D(0, 0, 0), new Point3D(1, 0, 0), new Point3D(1, 1, 0)));
        mg.addMesh(triangle(mat, new Point3D(1, 1, 0), new Point3D(0, 1, 0), new Point3D(0, 0, 0)));
        return mg.simplify();
    }
    
    public static SimpleMesh plane(Material mat, Point3D pos, Angle3D rot, Vector3D scale, Point2D t1, Point2D t2) {
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(triangle(mat, new Point3D(0, 0, 0), new Point3D(1, 0, 0), new Point3D(1, 1, 0), t1, new Point2D(t2.getX(), t1.getY()), t2));
        mg.addMesh(triangle(mat, new Point3D(1, 1, 0), new Point3D(0, 1, 0), new Point3D(0, 0, 0), t2, new Point2D(t1.getX(), t2.getY()), t1));
        return mg.simplify();
    }
    
    public static SimpleMesh curve1D(Material mat, Point3D pos, Angle3D rot, Vector3D scale, Point3D s1, Point3D s2) {
        Vector3D n1 = s1.toVec();
        Vector3D n2 = s2.toVec();
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(triangle(mat, new Point3D(0, 0, 0), new Point3D(1, 0, 0), new Point3D(1, 1, 0), n1, n2, n2));
        mg.addMesh(triangle(mat, new Point3D(1, 1, 0), new Point3D(0, 1, 0), new Point3D(0, 0, 0), n2, n1, n1));
        return mg.simplify();
    }
    
    public static SimpleMesh curve1D(Material mat, Point3D pos, Angle3D rot, Vector3D scale, Point3D s1, Point3D s2, Point2D t1, Point2D t2) {
        Vector3D n1 = s1.toVec();
        Vector3D n2 = s2.toVec();
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(triangle(mat, new Point3D(0, 0, 0), new Point3D(1, 0, 0), new Point3D(1, 1, 0), n1, n2, n2, t1, new Point2D(t2.getX(), t1.getY()), t2));
        mg.addMesh(triangle(mat, new Point3D(1, 1, 0), new Point3D(0, 1, 0), new Point3D(0, 0, 0), n2, n1, n1, t2, new Point2D(t1.getX(), t2.getY()), t1));
        return mg.simplify();
    }
    
    public static SimpleMesh cube(Material mat, Point3D pos, Angle3D rot, Vector3D scale) {
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(plane(mat, new Point3D(0, 0, 0), new Angle3D(0, 0, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        mg.addMesh(plane(mat, new Point3D(1, 0, -1), new Angle3D(0, 180, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        mg.addMesh(plane(mat, new Point3D(0, 0, -1), new Angle3D(0, -90, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        mg.addMesh(plane(mat, new Point3D(1, 0, 0), new Angle3D(0, 90, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        mg.addMesh(plane(mat, new Point3D(0, 0, -1), new Angle3D(90, 0, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        mg.addMesh(plane(mat, new Point3D(0, 1, 0), new Angle3D(-90, 0, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        return mg.simplify();
    }
	
    public static SimpleMesh pyramid(Material mat, Point3D pos, Angle3D rot, Vector3D scale) {
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(plane(mat, new Point3D(-0.5, -0.5, -0.5), new Angle3D(90, 0, 0), new Vector3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
        mg.addMesh(triangle(mat, new Point3D(-0.5, -0.5, -0.5), new Point3D(0, 0.5, 0), new Point3D(0.5, -0.5, -0.5)));
        mg.addMesh(triangle(mat, new Point3D(0.5, -0.5, -0.5), new Point3D(0, 0.5, 0), new Point3D(0.5, -0.5, 0.5)));
        mg.addMesh(triangle(mat, new Point3D(0.5, -0.5, 0.5), new Point3D(0, 0.5, 0), new Point3D(-0.5, -0.5, 0.5)));
        mg.addMesh(triangle(mat, new Point3D(-0.5, -0.5, 0.5), new Point3D(0, 0.5, 0), new Point3D(-0.5, -0.5, -0.5)));
        return mg.simplify();
    }
    
//    public static SimpleMesh twistcube(double angle, Material mat, Vector3D pos, Angle3D rot, Vector3D scale) {
//        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
////        mg.addMesh(plane(mat, new Point3D(0, 0, 0), new Point3D(0, 0, 0), new Point3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
////        mg.addMesh(plane(mat, new Point3D(1, 0, -1), new Point3D(0, 180, 0), new Point3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
////        mg.addMesh(plane(mat, new Point3D(0, 0, -1), new Point3D(0, -90, 0), new Point3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
////        mg.addMesh(plane(mat, new Point3D(1, 0, 0), new Point3D(0, 90, 0), new Point3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
////        mg.addMesh(plane(mat, new Point3D(0, 0, -1), new Point3D(90, 0, 0), new Point3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
////        mg.addMesh(plane(mat, new Point3D(0, 1, 0), new Point3D(-90, 0, 0), new Point3D(1, 1, 1), new Point2D(0, 0), new Point2D(1, 1)));
//        return mg.simplify(mat);
//    }
    
    public static SimpleMesh cylinder(int sides, double height, Material mat, Point3D pos, Angle3D rot, Vector3D scale) {
        double da = 2 * Math.PI / sides;
        double w = new Point3D(1, 0, 0).difference(new Point3D(Math.cos(da), 0, Math.sin(da))).toVec().magnitude();
        Point3D off = new Point3D(0, 0, 0);
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        Point3D s1 = new Point3D(Math.cos(da * (0 + 0.5) + Math.PI / 2), 0, Math.sin(da * (0 + 0.5) + Math.PI / 2));
        Point3D s2 = new Point3D(Math.cos(da * (0 + 1.5) + Math.PI / 2), 0, Math.sin(da * (0 + 1.5) + Math.PI / 2));
        for (int i = 0; i < sides; i++) {
            int ni = sides - i;
            Point3D p1 = new Point3D(Math.cos(da * (ni + 0.5) + Math.PI / 2), 0, Math.sin(da * (ni + 0.5) + Math.PI / 2)).sum(off);
            Point3D p2 = new Point3D(Math.cos(da * (ni + 1.5) + Math.PI / 2), 0, Math.sin(da * (ni + 1.5) + Math.PI / 2)).sum(off);
            mg.addMesh(triangle(mat, p2.sum(new Point3D(0, height, 0)), p1.sum(new Point3D(0, height, 0)), new Point3D(0, height, 0).sum(off)));
            mg.addMesh(triangle(mat, p1, p2, new Point3D(0, 0, 0).sum(off)));

            mg.addMesh(curve1D(mat, p1, new Angle3D(0, Math.PI * 2 / sides * i, 0), new Vector3D(w, height, w), s2, s1, new Point2D(1.0 / sides * i, 0), new Point2D(1.0 / sides * (i + 1), 1)));
        }
        return mg.simplify();
    }
    
    public static SimpleMesh flatCylinder(int sides, double height, Material mat, Point3D pos, Angle3D rot, Vector3D scale) {
        double da = 2 * Math.PI / sides;
        double w = new Point3D(1, 0, 0).difference(new Point3D(Math.cos(da), 0, Math.sin(da))).toVec().magnitude();
        Point3D off = new Point3D(0, 0, 0);
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        for (int i = 0; i < sides; i++) {
            int ni = sides - i;
            Point3D p1 = new Point3D(Math.cos(da * (ni + 0.5) + Math.PI / 2), 0, Math.sin(da * (ni + 0.5) + Math.PI / 2)).sum(off);
            Point3D p2 = new Point3D(Math.cos(da * (ni + 1.5) + Math.PI / 2), 0, Math.sin(da * (ni + 1.5) + Math.PI / 2)).sum(off);
            mg.addMesh(triangle(mat, p2.sum(new Point3D(0, height, 0)), p1.sum(new Point3D(0, height, 0)), new Point3D(0, height, 0).sum(off)));
            mg.addMesh(triangle(mat, p1, p2, new Point3D(0, 0, 0).sum(off)));

            mg.addMesh(plane(mat, p1, new Angle3D(0, Math.PI * 2 / sides * i, 0), new Vector3D(w, height, w), new Point2D(1.0 / sides * i, 0), new Point2D(1.0 / sides * (i + 1), 1)));
        }
        return mg.simplify();
    }
    
    private static HashMap<Integer, SimpleMesh> spheres = new HashMap<>();
    private static Material defmat = new Material(Color.white, Color.white, Color.black, 32, null);
    
    private static SimpleMesh getSphere(int tl) {
        if (spheres.containsKey(tl)) {
            return spheres.get(tl);
        } else {
            MeshGrouper mg = new MeshGrouper(Point3D.zeros(), Angle3D.zeros(), Vector3D.ones());
            double corner = 1;
            mg.addMesh(subdivideTri(defmat, new Point3D(corner, corner, -corner).toVec().normalize(), new Point3D(corner, -corner, corner).toVec().normalize(), new Point3D(-corner, -corner, -corner).toVec().normalize(), tl));
            mg.addMesh(subdivideTri(defmat, new Point3D(corner, corner, -corner).toVec().normalize(), new Point3D(-corner, corner, corner).toVec().normalize(), new Point3D(corner, -corner, corner).toVec().normalize(), tl));
            mg.addMesh(subdivideTri(defmat, new Point3D(corner, corner, -corner).toVec().normalize(), new Point3D(-corner, -corner, -corner).toVec().normalize(), new Point3D(-corner, corner, corner).toVec().normalize(), tl));
            mg.addMesh(subdivideTri(defmat, new Point3D(-corner, -corner, -corner).toVec().normalize(), new Point3D(corner, -corner, corner).toVec().normalize(), new Point3D(-corner, corner, corner).toVec().normalize(), tl));
            SimpleMesh sm = mg.simplify(defmat);
            spheres.put(tl, sm);
            return sm;
        }
    }
    
    public static SimpleMesh sphere(int tessLevel, Material mat, Point3D pos, Angle3D rot, Vector3D scale) {
        MeshGrouper mg = new MeshGrouper(pos, rot, scale);
        mg.addMesh(getSphere(tessLevel));
        return mg.simplify(mat);
    }
    
    public static SimpleMesh subdivideTri(Material mat, Point3D p1, Point3D p2, Point3D p3, int ctl) {
        if (ctl > 0) {
            MeshGrouper mg = new MeshGrouper(new Point3D(0, 0, 0), new Angle3D(0, 0, 0), Vector3D.ones());
//            Point3D mid = p1.midPoint(p2).midPoint(p2.midPoint(p3)).toVec().normalize();
//            mg.addMesh(subdivideTri(mat, p1, p2, mid, ctl - 1));
//            mg.addMesh(subdivideTri(mat, p2, p3, mid, ctl - 1));
//            mg.addMesh(subdivideTri(mat, p3, p1, mid, ctl - 1));
            Point3D m12 = p1.midPoint(p2).toVec().normalize();
            Point3D m23 = p2.midPoint(p3).toVec().normalize();
            Point3D m31 = p3.midPoint(p1).toVec().normalize();
            
            mg.addMesh(subdivideTri(mat, m12, m23, m31, ctl - 1));
            mg.addMesh(subdivideTri(mat, p1, m12, m31, ctl - 1));
            mg.addMesh(subdivideTri(mat, p2, m23, m12, ctl - 1));
            mg.addMesh(subdivideTri(mat, p3, m31, m23, ctl - 1));
            
            return mg.simplify(mat);
        } else {
            return triangleCurve(mat, p1, p2, p3);
        }
    }
        
}
