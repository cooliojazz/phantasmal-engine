package com.up.pe.mesh;

import com.jogamp.opengl.GL2;
import java.nio.FloatBuffer;

/**
 *
 * @author Ricky
 */
public class FrameAnimateMesh implements Mesh {

    private final AnimateMesh[] ams;
    private int cur = 0;
    private int length;
    private int time = 0;
	private boolean loop;
	boolean finished = false;

    public FrameAnimateMesh(SimpleMesh[] meshes, int length, boolean loop) {
        ams = new AnimateMesh[meshes.length - (loop ? 0 : 1)];
        for (int i = 0; i < meshes.length - 1; i++) {
            ams[i] = new AnimateMesh(meshes[i], meshes[i + 1], length, false);
        }
		if (loop) ams[ams.length - 1] = new AnimateMesh(meshes[meshes.length - 1], meshes[0], length, false);
        this.length = length;
		this.loop = loop;
    }
    
    @Override
    public int getVertexArray() {
        return ams[cur].getVertexArray();
    }

    @Override
    public int getVertexCount() {
        return ams[cur].getVertexCount();
    }

    @Override
    public FloatBuffer getVertexBuffer() {
        return ams[cur].getVertexBuffer();
    }

    @Override
    public FloatBuffer getNormalBuffer() {
        return ams[cur].getNormalBuffer();
    }

    @Override
    public FloatBuffer getTextureBuffer() {
        return ams[cur].getTextureBuffer();
    }

    @Override
    public FloatBuffer getAmbientBuffer() {
        return ams[cur].getAmbientBuffer();
    }

    @Override
    public FloatBuffer getDiffuseBuffer() {
        return ams[cur].getDiffuseBuffer();
    }

    @Override
    public FloatBuffer getSpecularBuffer() {
        return ams[cur].getSpecularBuffer();
    }

    @Override
    public FloatBuffer getEmissionBuffer() {
        return ams[cur].getEmissionBuffer();
    }

    @Override
    public FloatBuffer getShineBuffer() {
        return ams[cur].getShineBuffer();
    }

    @Override
    public boolean hasVBOs() {
        return ams[cur].hasVBOs();
    }

    @Override
    public void createVBOs(GL2 gl) {
        ams[cur].createVBOs(gl);
    }

	@Override
	public void cleanupVBOs(GL2 gl) {
		for (Mesh m : ams) {
			if (m.hasVBOs()) m.cleanupVBOs(gl);
		}
	}

    @Override
    public void updateVBOs(GL2 gl) {
        ams[cur].updateVBOs(gl);
    }

    @Override
    public boolean needsUpdate() {
        return ams[cur].needsUpdate();
    }
    
    public void frame() {
		if (!finished) {
			if (cur < ams.length) {
				time++;
				if (time > length) {
					if (cur >= ams.length - 1 && !loop) {
						finished = true;
						return;
					}
					time = 0;
					ams[cur].reset();
					cur++;
				} else {
					ams[cur].frame();
				}
			}
			if (cur >= ams.length) {
				cur = 0;
			}
		}
    }
    
    public boolean isAnimationDone() {
        return finished;
    }

	@Override
	public int getTexture() {
		if (ams.length > 0) {
			return ams[cur].getTexture();
		}
		return -1;
	}

}
