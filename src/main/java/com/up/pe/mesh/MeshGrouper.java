package com.up.pe.mesh;

import com.up.pe.math.Angle3D;
import com.up.pe.render.Material;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.render.Vertex;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A helper to group together SimpleMeshes with the intent of simplifying them into a single new SimpleMesh which no longer contains the individual reference SimpleMeshes.
 * Compare to MeshGroup which is a Mesh itself and maintains the list of sub-meshes, allowing for modifying the group while using it.
 * @author Ricky
 */
public class MeshGrouper {
    
    ArrayList<SimpleMesh> meshes = new ArrayList<>();
    
    private Point3D offset;
    private Angle3D rot;
    private Vector3D scale;

    public MeshGrouper(Point3D offset, Angle3D rot, Vector3D scale) {
        this.offset = offset;
        this.rot = rot;
        this.scale = scale;
    }
    
    public int getVertexCount() {
        return meshes.stream().map(m -> m.getVertexCount()).reduce(0, (m1, m2) -> m1 + m2);
    }
    
    public MeshGrouper addMesh(SimpleMesh m) {
        meshes.add(m);
        return this;
    }
    
    public MeshGrouper addMeshes(List<SimpleMesh> m) {
        meshes.addAll(m);
        return this;
    }

    /**
     * A remanent of before the engine supported per-vertex materials. Only used by the mesh generator.
     * @param mat
     * @return
     * @deprecated
     */
    @Deprecated
    public SimpleMesh simplify(Material mat) {
        return new SimpleMesh(
                meshes.stream()
                        .flatMap(m -> m.stream().map(v -> new Vertex(
                                v.getPosition().toVec().product(m.getScale()).rotate(m.getRotation()).sum(m.getPosition()),
                                v.getPosition().toVec().product(m.getScale()).sum(v.getNormal()).rotate(m.getRotation()).difference(v.getPosition().toVec().product(m.getScale()).rotate(m.getRotation())).toVec(),
                                v.getTexPosition(),
                                mat
                            )
                        ))
                        .toArray(Vertex[]::new),
                null, offset, rot, scale);
    }
    
    public SimpleMesh simplify() {
        return new SimpleMesh(
                meshes.stream()
                        .flatMap(m -> m.stream().map(v -> new Vertex(
                                v.getPosition().toVec().product(m.getScale()).rotate(m.getRotation()).sum(m.getPosition()),
                                v.getPosition().toVec().product(m.getScale()).sum(v.getNormal()).rotate(m.getRotation()).difference(v.getPosition().toVec().product(m.getScale()).rotate(m.getRotation())).toVec(),
                                v.getTexPosition(),
                                m.getMaterial() == null ? v.getMaterial() : (v.getMaterial() == null ? m.getMaterial() : m.getMaterial().blend(v.getMaterial(), 0.5f))
                            )
                        ))
                        .toArray(Vertex[]::new),
                null, offset, rot, scale);
    }
}
