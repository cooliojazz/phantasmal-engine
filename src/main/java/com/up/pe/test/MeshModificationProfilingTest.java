package com.up.pe.test;

import com.up.pe.math.Angle3D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.MeshGenerator;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.render.Material;
import java.awt.Color;

/**
 *
 * @author rush
 */
public class MeshModificationProfilingTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        SimpleMesh testSphere = MeshGenerator.sphere(3, new Material(Color.pink, Color.white, Color.black, 0, null), Point3D.zeros(), Angle3D.zeros(), Vector3D.ones());
        
        final int meshc = 1000;
        System.out.println("Testing update of " + testSphere.getVertexCount() * meshc + " vertices per round.");
        int total = 0;
        for (int i = 0; i < 100; i++) {
            long time = System.currentTimeMillis();
            for (int j = 0; j < meshc; j++) {
                tick(testSphere);
            }
            total += System.currentTimeMillis() - time;
            System.out.println(i + ": " + (System.currentTimeMillis() - time) + "ms");
        }
        double avg = total / 100d;
        System.out.println("Average time: " + avg + "ms (" + (int)(avg / testSphere.getVertexCount() / meshc * 1000000) + "ns / vertex)");
    }
    
    
    public static void tick(SimpleMesh m) {
//        m.setPosition(m.getPosition().sum(0.1));
//        m.setRotation(m.getRotation().sum(0.1));
//        m.setScale(m.getScale().sum(0.1));
        m.setSpatial(m.getPosition().sum(0.1), m.getRotation().sum(0.1), m.getScale().sum(0.1));
    }
}
