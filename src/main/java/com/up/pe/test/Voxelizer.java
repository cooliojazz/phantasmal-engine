//package com.up.pe.test;
//
//import com.jogamp.opengl.GL2;
//import com.jogamp.opengl.GLCapabilities;
//import com.jogamp.opengl.GLProfile;
//import com.jogamp.opengl.awt.GLCanvas;
//import com.up.pe.Angle3D;
//import com.up.pe.DisplayManager;
//import com.up.pe.Light;
//import com.up.pe.Line;
//import com.up.pe.Material;
//import com.up.pe.ObjLoader;
//import com.up.pe.Point2D;
//import com.up.pe.Point3D;
//import com.up.pe.Vector3D;
//import com.up.pe.mesh.Mesh;
//import com.up.pe.mesh.MeshGenerator;
//import com.up.pe.mesh.SimpleMesh;
//import java.awt.Color;
//import java.awt.Frame;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.stream.Collectors;
//
///**
// *
// * @author Ricky
// */
//public class Voxelizer {
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        Frame f = new Frame("Phantasmal Engine");
//        f.addWindowListener(new WindowAdapter() {
//
//            @Override
//            public void windowClosing(WindowEvent e) {
//                System.exit(0);
//            }
//            
//        });
//        DisplayManager dm = new DisplayManager() {
//
//            @Override
//            public void preRender(GL2 gl) {
//                
//            }
//
//            @Override
//            public void postRender(GL2 gl) {
//                renderString(getCamera().getX() + ", " + getCamera().getY() + ", " + getCamera().getZ(), Color.red, new Point2D(20, 20));
//                renderString(getCameraAngle().getX() + ", " + getCameraAngle().getY() + ", " + getCameraAngle().getZ(), Color.red, new Point2D(20, 40));
//                renderString(Math.round(getFPS()) + "fps", Color.red, new Point2D(20, 60));
//            }
//        };
//        GLCanvas c = dm.getCanvas();
//        f.add(c);
//        f.setSize(800, 600);
//        f.setVisible(true);
//        dm.addDefaultFlyControls(1, 1);
//        
//        ObjLoader o = null;
//        try {
//            dm.addMeshes(new ObjLoader(new File("meshes/M1911.obj")).create(new Point3D(0, 0, 0), new Angle3D(0, 0, 0), new Vector3D(1, 1, 1)));
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        //Calculate voxelization
//        //Find mesh bounds
//        Point3D min = new Point3D(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
//        Point3D max = new Point3D(Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE);
//        dm.getMeshes().stream().filter(m -> m instanceof SimpleMesh).map(m -> (SimpleMesh)m).forEach(m ->
//            m.stream().map(v -> v.getPosition()).forEach(p -> {
//                if (p.getX() < min.getX()) min.setX(p.getX());
//                if (p.getY() < min.getY()) min.setY(p.getY());
//                if (p.getZ() < min.getZ()) min.setZ(p.getZ());
//                if (p.getX() > max.getX()) max.setX(p.getX());
//                if (p.getY() > max.getY()) max.setY(p.getY());
//                if (p.getZ() > max.getZ()) max.setZ(p.getZ());
//            })
//        );
//        
//        ArrayList<Point3D> voxes = new ArrayList<>();
//        ArrayList<float[]> colors = new ArrayList<>();
//        double res = 1 / 4d;
//        double res2 = res / 2;
//        ArrayList<Long> times = new ArrayList<>();
//        for (double x = (int)min.getX() - 1; x < (int)max.getX() + 1; x += res) {
//            for (double y = (int)min.getY() - 1; y < (int)max.getY() + 1; y += res) {
//                long time = System.currentTimeMillis();
//                for (double z = (int)min.getZ() - 1; z < (int)max.getZ() + 1; z += res) {
//                    for (Mesh m : dm.getMeshes()) {
//                        SimpleMesh sm = (SimpleMesh)m;
//                        if (sm.intersects(new Line(new Point3D(x - res2, y - res2, z - res2), new Point3D(x + res2, y - res2, z - res2))) && !voxes.contains(new Point3D(x, y, z))) {
//                            voxes.add(new Point3D(x, y, z));
//                            colors.add(sm.getMaterial().diffuse);
//                            break;
//                        }
//                        if (sm.intersects(new Line(new Point3D(x - res2, y - res2, z - res2), new Point3D(x - res2, y - res2, z + res2))) && !voxes.contains(new Point3D(x, y, z))) {
//                            voxes.add(new Point3D(x, y, z));
//                            colors.add(sm.getMaterial().diffuse);
//                            break;
//                        }
//                        if (sm.intersects(new Line(new Point3D(x - res2, y - res2, z - res2), new Point3D(x - res2, y + res2, z - res2))) && !voxes.contains(new Point3D(x, y, z))) {
//                            voxes.add(new Point3D(x, y, z));
//                            colors.add(sm.getMaterial().diffuse);
//                            break;
//                        }
//                    }
//                }
//                times.add(System.currentTimeMillis() - time);
////                System.out.println(Math.round(((x - (int)min.getX() + 1) / (double)((int)max.getX() - (int)min.getX() + 2) + (y - (int)min.getY() + 1) / (double)((int)max.getY() - (int)min.getY() + 2) * res / ((int)max.getX() - (int)min.getX() + 2)) * 10000) / 100d + "%  " + times.get(times.size() - 1) + " " + times.stream().collect(Collectors.averagingDouble(l -> (double)l)));
//                System.out.println(Math.round(((x - (int)min.getX() + 1) / (double)((int)max.getX() - (int)min.getX() + 2) + (y - (int)min.getY() + 1) / (double)((int)max.getY() - (int)min.getY() + 2) * res / ((int)max.getX() - (int)min.getX() + 2)) * 10000) / 100d + "%  " + voxes.size() + " cubes  " + times.get(times.size() - 1) + "ms");
//            }
//        }
//        System.out.println(times.stream().collect(Collectors.averagingDouble(l -> (double)l)));
//        
//        dm.clearMeshes();
//        
//        dm.addLight(new Light(new Point3D(-1, 2, -1), Material.white, 100));
////        dm.addLight(new Light(new Point3D(5, 10, 5), Material.white, 100));
//        dm.addLight(new Light(new Point3D(5, 10, 5), new float[] {0.75f, 0.5f, 0.25f, 1f}, 100));
////        dm.addLight(new Light(new Point3D(5, 10, 5), new float[] {0.0f, 0.0f, 0.0f, 1f}, 100));
//        dm.addMesh(MeshGenerator.cube(new Material(Material.white, Material.black, Material.white, 0, null), new Point3D(-1, 2, -1), new Angle3D(0, 0, 0), new Vector3D(.1, .1, .1)));
//        dm.addMesh(MeshGenerator.cube(new Material(Material.green, Material.black, Material.green, 0, null), new Point3D(5, 10, 5), new Angle3D(0, 0, 0), new Vector3D(1, 1, 1)));
//        
//        for (int i = 0; i < voxes.size(); i++) {
//            dm.addMesh(MeshGenerator.cube(new Material(colors.get(i), Material.white, Material.black, 16, null), voxes.get(i), new Angle3D(0, 0, 0), new Vector3D(res, res, res)));
//        }
//        
//        new Thread(() -> {
//            while (true) {
//                c.repaint();
//                try {
//                    Thread.sleep(1);
//                } catch (InterruptedException ex) { }
//            }
//        }).start();
//        
////        //Simple physics. Very simple.
////        new Thread(() -> {
////            while (true) {
////                for (Mesh m : dm.getMeshes()) {
//////                    m.vel.setX(m.vel.getX() + (Math.random() - 0.5) / 100);
////                    m.vel.setY(m.vel.getY() - 0.1);
//////                    m.vel.setY(m.vel.getY() - 0.1 + (Math.random() - 0.5) / 100);
//////                    m.vel.setZ(m.vel.getZ() + (Math.random() - 0.5) / 100);
////                    m.vel = m.vel.mul(0.99);
////                    m.setPosition(m.getPosition().sum(m.vel));
////                    if (m.getPosition().getY() < -10) {
////                        m.getPosition().setY(-10);
////                        m.vel.setY(m.vel.getY() * -1);
////                        m.vel = m.vel.mul(0.75);
////                    }
////                }
////            }
////        }).start();
//    }
//    
//}
