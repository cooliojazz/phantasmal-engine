package com.up.pe.test;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.awt.GLCanvas;
import com.up.pe.input.MouseFreeTrackingCameraAdapter;
import com.up.pe.math.Angle3D;
import com.up.pe.render.DisplayManager;
import com.up.pe.render.light.PointLight;
import com.up.pe.render.Material;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.AnimateMesh;
import com.up.pe.mesh.FrameAnimateMesh;
import com.up.pe.mesh.MeshGenerator;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.render.event.RenderInterceptorAdapter;
import com.up.pe.render.camera.FreeTrackingCamera;
import java.awt.Color;
import javax.swing.JFrame;

/**
 *
 * @author Ricky
 */
public class SimpleAnimateExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(1000, 800);
		DisplayManager dm = new DisplayManager(new RenderInterceptorAdapter() {
				@Override
				public void preRender(DisplayManager dm, GL2 gl) {
					dm.drawAxis(gl, 10);
				}

				@Override
				public void postRender(DisplayManager dm, GL2 gl) {
//					dm.renderString(gl, Math.round(dm.getFPS()) + "fps", Color.red, new Point2D(10, 10));
				}
			});
		GLCanvas cnv = dm.getCanvas();
		f.add(cnv);
		f.setVisible(true);
		FreeTrackingCamera cam = new FreeTrackingCamera(Point3D::zeros);
		dm.setCamera(cam);
		new MouseFreeTrackingCameraAdapter(cam).addToComponent(cnv);
		new Thread(() -> {
			while (true) {
				cnv.repaint();
				try {
					Thread.sleep(1);
				} catch (InterruptedException ex) { }
			}
		}).start();

		dm.addLight(new PointLight(new Point3D(5, 5, 5), new float[] {1.0f, 1.0f, 0.5f, 1.0f}, 100));
		dm.addMesh(MeshGenerator.cube(new Material(new float[] {1.0f, 1.0f, 0.5f, 1.0f}, new float[] {1.0f, 1.0f, 0.5f, 1.0f}, new float[] {1.0f, 1.0f, 0.5f, 1.0f}, 0, null), new Point3D(5, 5, 5), new Angle3D(0, 0, 0), new Vector3D(0.25, 0.25, 0.25)));
		dm.addLight(new PointLight(new Point3D(0, 0, 0), new float[] {0.5f, 0.5f, 1.0f, 1.0f}, 100));
		dm.addMesh(MeshGenerator.cube(new Material(new float[] {0.5f, 0.5f, 1.0f, 1.0f}, new float[] {0.5f, 0.5f, 1.0f, 1.0f}, new float[] {0.5f, 0.5f, 1.0f, 1.0f}, 0, null), new Point3D(0, 0, 0), new Angle3D(0, 0, 0), new Vector3D(0.25, 0.25, 0.25)));

		SimpleMesh f1 = MeshGenerator.sphere(5, new Material(Color.red, Color.white, Color.black, 0.0f, null), new Point3D(10, 0, 0), new Angle3D(0, 0, 0), new Vector3D(0.25, 1, 1));
		SimpleMesh f2 = MeshGenerator.sphere(5, new Material(Color.blue, Color.white, Color.black, 0.5f, null), new Point3D(0, 0, 10), new Angle3D(0, 0, 0), new Vector3D(1, 1, 0.25));
		SimpleMesh f3 = MeshGenerator.sphere(5, new Material(Color.green, Color.white, Color.black, 1.0f, null), new Point3D(0, 10, 0), new Angle3D(0, 0, 0), new Vector3D(1, 0.25, 1));
		FrameAnimateMesh fam1 = new FrameAnimateMesh(new SimpleMesh[] {f1, f2, f3, f1}, 100, false);
		FrameAnimateMesh fam2 = new FrameAnimateMesh(new SimpleMesh[] {f2, f3, f1}, 100, true);
		FrameAnimateMesh fam3 = new FrameAnimateMesh(new SimpleMesh[] {f3, f1, f2}, 100, true);
		dm.addMesh(fam1);
		dm.addMesh(fam2);
		dm.addMesh(fam3);

		new Thread(() -> {
			while (true) {
				fam1.frame();
				fam2.frame();
				fam3.frame();
				try {
					Thread.sleep(10);
				} catch (InterruptedException ex) { }
			}
		}).start();
    }
    
}
