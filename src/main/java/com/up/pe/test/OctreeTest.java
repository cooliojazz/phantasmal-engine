package com.up.pe.test;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.up.pe.collision.AABoundingBox;
import com.up.pe.collision.Collidable;
import com.up.pe.collision.Octree;
import com.up.pe.input.MouseFreeTrackingCameraAdapter;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.MeshGenerator;
import com.up.pe.mesh.SimpleMesh;
import com.up.pe.render.DisplayManager;
import com.up.pe.render.light.PointLight;
import com.up.pe.render.Material;
import com.up.pe.render.event.RenderInterceptor;
import com.up.pe.render.camera.FreeTrackingCamera;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

/**
 *
 * @author rush
 */
public class OctreeTest {
    
    private static Octree ot = new Octree(new AABoundingBox(Point3D.zeros(), new Vector3D(100, 100, 100)));
    
    private static double tps = 0;
    
    public static void main(String[] args) {
        Frame f = new Frame("Phantasmal Engine");
        f.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
            
        });
        DisplayManager dm = new DisplayManager(new RenderInterceptor() {

            @Override
            public void preRender(DisplayManager dm, GL2 gl) {
				dm.drawAxis(gl, 100);
            }

            @Override
            public void postRender(DisplayManager dm, GL2 gl) {
                ot.render(gl);
//                dm.renderString(gl, dm.getCamera().getPosition().getX() + ", " + dm.getCamera().getPosition().getY() + ", " + dm.getCamera().getPosition().getZ(), Color.red, new Point2D(20, 20));
//                dm.renderString(gl, dm.getCamera().getAngle().getX() + ", " + dm.getCamera().getAngle().getY() + ", " + dm.getCamera().getAngle().getZ(), Color.red, new Point2D(20, 40));
//                dm.renderString(gl, Math.round(dm.getFPS()) + "fps", Color.red, new Point2D(20, 60));
//                dm.renderString(gl, Math.round(tps) + "tps", Color.red, new Point2D(20, 80));
                
//                drawgravities.entrySet().stream().forEach(e -> e.getValue().render(gl, e.getKey().getMesh().getPosition()));
            }
			
            @Override
            public void preTransformation(DisplayManager dm, GL2 gl) {
            }
        });
		dm.setRenderDistance(1000);
        GLCapabilities cap = new GLCapabilities(GLProfile.getDefault());
//        cap.setSampleBuffers(true);
        GLCanvas c = dm.getCanvas(cap);
        f.add(c);
        f.setSize(1000, 800);
        f.setVisible(true);
        FreeTrackingCamera camera = new FreeTrackingCamera(Point3D::zeros);
        dm.setCamera(camera);
        new MouseFreeTrackingCameraAdapter(camera).addToComponent(dm.getCanvas());
        
        dm.addLight(new PointLight(new Point3D(-25, 10, 10), Material.white, 100));
        dm.addLight(new PointLight(new Point3D(75, 10, -10), Material.red, 100));
        
        ArrayList<BoundedSphere> meshes = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Color mc = new Color((int)(Math.random() * Integer.MAX_VALUE));
//            AABoundingBox bb = new AABoundingBox(new Point3D((i - 5) * 2, -5, -5), new Vector3D(1.5, 1, 1));
            AABoundingBox bb = new AABoundingBox(new Point3D((Math.random() - 0.5) * 90, (Math.random() - 0.5) * 90, (Math.random() - 0.5) * 90), new Vector3D(Math.random() * 2 + 1, Math.random() * 2 + 1, Math.random() * 2 + 1));
//            SimpleMesh m = MeshGenerator.cube(new Material(mc, Color.WHITE, Color.BLACK, 0.25f, null), bb.getCenter().sum(bb.getExtent().mul(-1)), new Angle3D(0, 0, 0), bb.getExtent().mul(2));
            SimpleMesh m = MeshGenerator.sphere(2, new Material(mc, Color.WHITE, Color.BLACK, 0.25f, null), bb.getCenter(), new Angle3D(0, 0, 0), bb.getExtent());
            meshes.add(new BoundedSphere(m, bb));
        }
//        dm.addMeshes(meshes.stream().map(s -> s.getMesh()).collect(Collectors.toList()));
        ot.build(meshes);
        
        new Thread(() -> {
            while (true) {
                long tpstime = System.nanoTime();
                doGravity(meshes);
//                for (int i = 0; i < meshes.size(); i++) {
//                    BoundedSphere s = meshes.get(i);
//                    SimpleMesh m = s.getMesh();
//                    m.setPosition(m.getPosition().sum(new Point3D(Math.random(), Math.random(), Math.random()).sum(-.5).mul(0.5)));
//                    s.setBounds(new AABoundingBox(m.getPosition(), m.getScale()));
//                }
                ot.build((ArrayList<BoundedSphere>)meshes.clone());
//                doNaiveCollisions(meshes);
                doOctreeCollisions(ot);
                tps = 1000000000d / (System.nanoTime() - tpstime);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) { }
            }
        }, "Physics").start();
        
        new Thread(() -> {
            while (true) {
                c.repaint();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) { }
            }
        }, "Render").start();
        
    }
    
    static HashMap<BoundedSphere, Vector3D> drawgravities = new HashMap<>();
    
    private static void doGravity(ArrayList<BoundedSphere> meshes) {
        HashMap<BoundedSphere, Vector3D> gravities = new HashMap<>(meshes.size());
//        for (int i = 0; i < meshes.size(); i++) {
//            BoundedSphere curm = meshes.get(i);
//            Vector3D curg = Vector3D.zeros();
//            for (int j = 0; j < meshes.size(); j++) {
//                if (i != j) {
//                    Vector3D dir = curm.getMesh().getPosition().vectorTo(meshes.get(j).getMesh().getPosition());
//                    double r = dir.magnitude();
//                    if (r > 0.5) curg = curg.sum(dir.normalize().mul(1 / r / r));
//                }
//            }
//            gravities.put(curm, curg);
//        }
//        for (int i = 0; i < meshes.size(); i++) {
//            BoundedSphere curm = meshes.get(i);
//            curm.getMesh().setPosition(curm.getMesh().getPosition().sum(gravities.get(curm)));
//        }
        drawgravities = gravities;
    }
    
    static class BoundedSphere implements Collidable {
        
        private SimpleMesh sphere;
        private AABoundingBox bounds;

        public BoundedSphere(SimpleMesh sphere, AABoundingBox bounds) {
            this.sphere = sphere;
            this.bounds = bounds;
        }

        @Override
        public AABoundingBox getBounds() {
            return bounds;
        }

//        @Override
//        public SimpleMesh getMesh() {
//            return sphere;
//        }

        public void setBounds(AABoundingBox bounds) {
            this.bounds = bounds;
        }

        @Override
        public String toString() {
            return "{Sphere in " + bounds + "}";
        }
        
    }
    
    private static void doNaiveCollisions(ArrayList<? extends Collidable> meshes) {
//        long nt = System.nanoTime();
//        ArrayList<Pair<Collidable, Collidable>> collisions = new ArrayList<>();
//        for (Collidable m : meshes) {
//            meshes.stream().filter(m2 -> m != m2 && m.getMesh().collides(m2.getMesh()) && !collisions.contains(new Pair<>(m, m2))).map(m2 -> new Pair<>(m, m2)).forEach(collisions::add);
//        }
//        collisions.forEach(System.out::println);
//        System.out.println("Naïve: " + (System.nanoTime() - nt) / 1000000000d);
    }
    
    private static void doOctreeCollisions(Octree o) {
        long nt = System.nanoTime();
        ArrayList<Pair<Collidable, Collidable>> collisions = octreeCollision(o.getRoot(), new ArrayList<>());
        System.out.println("Streamed: " + (System.nanoTime() - nt) / 1000000000d);
        nt = System.nanoTime();
        collisions = streamlessOctreeCollision(o.getRoot(), new ArrayList<>());
        System.out.println("Streamless: " + (System.nanoTime() - nt) / 1000000000d);
        collisions.forEach(System.out::println);
    }
    
    private static ArrayList<Pair<Collidable, Collidable>> octreeCollision(Octree.OctreeNode n, ArrayList<Collidable> cols) {
        ArrayList<Pair<Collidable, Collidable>> collisions = new ArrayList<>();
//        n.getContents().stream().flatMap(s -> n.getContents().stream()
//                .map(s2 -> new Pair<>(s, s2)))
//                .filter(p -> p.getT() != p.getU() && !collisions.contains(p) && p.getT().getMesh().collides(p.getU().getMesh())
//            ).forEach(collisions::add);
//        n.getContents().stream().flatMap(s -> cols.stream()
//                .map(s2 -> new Pair<>(s, s2)))
//                .filter(p -> !collisions.contains(p) && p.getT().getMesh().collides(p.getU().getMesh())
//            ).forEach(collisions::add);
//        cols.addAll(n.getContents());
//        for (Octree.OctreeNode sn : n.getChildren()) {
//            if (sn != null) collisions.addAll(octreeCollision(sn, cols));
//        }
//        cols.removeAll(n.getContents());
        return collisions;
    }
    
    private static ArrayList<Pair<Collidable, Collidable>> streamlessOctreeCollision(Octree.OctreeNode n, ArrayList<Collidable> cols) {
        ArrayList<Pair<Collidable, Collidable>> collisions = new ArrayList<>();
//        for (Collidable c1 : n.getContents()) {
//            for (Collidable c2 : n.getContents()) {
//                if (c1 != c2) {
//                    Pair<Collidable, Collidable> p = new Pair<>(c1, c2);
//                    boolean test1 = c1.getMesh().collides(c2.getMesh());
////                    boolean test2 = c1.getMesh().streamlessCollides(c2.getMesh());
//                    if (!collisions.contains(p) && test1) {
//                        collisions.add(p);
//                    }
//                }
//            }
//            for (Collidable c2 : cols) {
//                Pair<Collidable, Collidable> p = new Pair<>(c1, c2);
//                boolean test1 = c1.getMesh().collides(c2.getMesh());
////                boolean test2 = c1.getMesh().streamlessCollides(c2.getMesh());
//                if (!collisions.contains(p) && test1) {
//                    collisions.add(p);
//                }
//            }
//        }
//        cols.addAll(n.getContents());
//        for (Octree.OctreeNode sn : n.getChildren()) {
//            if (sn != null) collisions.addAll(streamlessOctreeCollision(sn, cols));
//        }
//        cols.removeAll(n.getContents());
        return collisions;
    }
    
    private static void showOctreeChildren(Octree.OctreeNode n) {
        n.getContents().stream().map(c -> n.getBounds() + ": " + c.getBounds()).forEach(System.out::println);
        for (Octree.OctreeNode sn : n.getChildren()) {
            if (sn != null) showOctreeChildren(sn);
        }
    }
    
    
    
    private static class Pair<T, U>  {

        private T t;
        private U u;

        public Pair(T t, U u) {
            this.t = t;
            this.u = u;
        }

        public T getT() {
            return t;
        }

        public U getU() {
            return u;
        }

        public void setT(T t) {
            this.t = t;
        }

        public void setU(U u) {
            this.u = u;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Pair) {
                Pair<T, U> p = ((Pair<T, U>)obj);
                return (t == p.t && u == p.u) || (t == p.u && u == p.t);
            }
            return false;
        }

        @Override
        public String toString() {
            return "(" + t + ", " + u + ")";
        }
        
    }
    
}
