package com.up.pe.test;

import com.up.pe.math.Angle3D;
import com.up.pe.render.Material;
import com.up.pe.math.Point3D;
import com.up.pe.test.SimplexNoise;
import com.up.pe.math.Vector3D;
import com.up.pe.render.Vertex;
import com.up.pe.mesh.Mesh;
import com.up.pe.mesh.SimpleMesh;
import java.awt.Color;

/**
 *
 * @author rjtalbot
 */
public class ProceduralMesh {
    
    final int width = 50;
    final int height = 50;
    double wfac = 2;
    double hfac = 1;
    double[][] heightmap = new double[width][height];
    public Mesh[] meshes = new Mesh[(width - 1) * (height - 1)];
    SimplexNoise n = new SimplexNoise(2, 0.5);
    double aframe;

    public ProceduralMesh(double z) {
        aframe = z;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                heightmap[x][y] = getSimplexPoint(x, y);
            }
        }
    }
    
    private double getSimplexPoint(double x, double z) {
        return n.simplexNoise(z / wfac, x / wfac, aframe) * hfac;
    }
    
    public Vector3D getFaceNormal(double x, double z) {
//        if (x < 1) x = 1;
//        if (x + 1 >= heightmap.length) x = heightmap.length - 2;
//        if (z < 1) z = 1;
//        if (z + 1 >= heightmap[0].length) z = heightmap[0].length - 2;
//        
//        Vector3D n1 = Point3D.normal(new Point3D(x, heightmap[x][z], z), new Point3D(x + 0.5, (heightmap[x][z] + heightmap[x + 1][z] + heightmap[x + 1][z + 1] + heightmap[x][z + 1]) / 4, z + 0.5), new Point3D(x + 1, heightmap[x + 1][z], z));
//        Vector3D n2 = Point3D.normal(new Point3D(x, heightmap[x][z + 1], z + 1), new Point3D(x + 0.5, (heightmap[x][z] + heightmap[x + 1][z] + heightmap[x + 1][z + 1] + heightmap[x][z + 1]) / 4, z + 0.5), new Point3D(x, heightmap[x][z], z));
//        Vector3D n3 = Point3D.normal(new Point3D(x, heightmap[x][z - 1], z - 1), new Point3D(x - 0.5, (heightmap[x - 1][z - 1] + heightmap[x][z - 1] + heightmap[x][z] + heightmap[x - 1][z]) / 4, z - 0.5), new Point3D(x, heightmap[x][z], z));
//        Vector3D n4 = Point3D.normal(new Point3D(x, heightmap[x][z], z), new Point3D(x - 0.5, (heightmap[x - 1][z - 1] + heightmap[x][z - 1] + heightmap[x][z] + heightmap[x - 1][z]) / 4, z - 0.5), new Point3D(x - 1, heightmap[x - 1][z], z));
//        Vector3D n5 = Point3D.normal(new Point3D(x - 1, heightmap[x - 1][z], z), new Point3D(x - 0.5, (heightmap[x - 1][z] + heightmap[x][z] + heightmap[x][z + 1] + heightmap[x - 1][z + 1]) / 4, z + 0.5), new Point3D(x, heightmap[x][z], z));
//        Vector3D n6 = Point3D.normal(new Point3D(x, heightmap[x][z], z), new Point3D(x - 0.5, (heightmap[x - 1][z] + heightmap[x][z] + heightmap[x][z + 1] + heightmap[x - 1][z + 1]) / 4, z + 0.5), new Point3D(x, heightmap[x][z + 1], z + 1));
//        Vector3D n7 = Point3D.normal(new Point3D(x + 1, heightmap[x + 1][z], z), new Point3D(x + 0.5, (heightmap[x][z - 1] + heightmap[x + 1][z - 1] + heightmap[x + 1][z] + heightmap[x][z]) / 4, z - 0.5), new Point3D(x, heightmap[x][z], z));
//        Vector3D n8 = Point3D.normal(new Point3D(x, heightmap[x][z], z), new Point3D(x + 0.5, (heightmap[x][z - 1] + heightmap[x + 1][z - 1] + heightmap[x + 1][z] + heightmap[x][z]) / 4, z - 0.5), new Point3D(x, heightmap[x][z - 1], z - 1));
//        return n1.sum(n2).sum(n3).sum(n4).sum(n5).sum(n6).sum(n7).sum(n8).mul(1 / 8d).normalize();

//        double dn = 1 / Math.pow(10, ProceduralMeshTest.ls.getValue());
//        return Point3D.normal(new Point3D(x + dn, getSimplexPoint(x + dn, z), z), new Point3D(x, getSimplexPoint(x, z), z), new Point3D(x, getSimplexPoint(x, z + dn), z + dn));

        double h = 0.000000001;
        double gx = (getSimplexPoint(x + h, z) - getSimplexPoint(x, z)) / h;
        double gy = 1;
        double gz = (getSimplexPoint(x, z + h) - getSimplexPoint(x, z)) / h;
        
        return new Vector3D(gx, gy, gz);
    }
    
    public Mesh[] createMeshes() {
        Material mat = new Material(new Color(0.5f, 0f, 0.5f).getRGBColorComponents(null), Material.white, Material.black, 0.5f, null);
        for (int x = 0; x < width - 1; x++) {
            for (int z = 0; z < height - 1; z++) {
                double h1 = heightmap[x][z];
                double h2 = heightmap[x + 1][z];
                double h3 = heightmap[x + 1][z + 1];
                double h4 = heightmap[x][z + 1];
                double h5 = (h1 + h2 + h3 + h4) / 4;
//                Vector3D n1 = Point3D.normal(new Point3D(x, h1, z), new Point3D(x + 0.5, h5, z + 0.5), new Point3D(x + 1, h2, z));
//                Vector3D n2 = Point3D.normal(new Point3D(x + 1, h3, z + 1), new Point3D(x + 0.5, h5, z + 0.5), new Point3D(x, h4, z + 1));
//                Vector3D n3 = Point3D.normal(new Point3D(x, h4, z + 1), new Point3D(x + 0.5, h5, z + 0.5), new Point3D(x, h1, z));
//                Vector3D n4 = Point3D.normal(new Point3D(x + 1, h2, z), new Point3D(x + 0.5, h5, z + 0.5), new Point3D(x + 1, h3, z + 1));
//                Vector3D n5 = n1.sum(n2).sum(n3).sum(n4).mul(1 / 4d).normalize();
                Vector3D n5 = getFaceNormal(x + 0.5, z + 0.5);
                
                Vertex[] verts = new Vertex[12];
                verts[0] = new Vertex(new Point3D(x, h1, z), getFaceNormal(x, z), mat);
                verts[1] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                verts[2] = new Vertex(new Point3D(x + 1, h2, z), getFaceNormal(x + 1, z), mat);
                
                verts[3] = new Vertex(new Point3D(x + 1, h3, z + 1), getFaceNormal(x + 1, z + 1), mat);
                verts[4] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                verts[5] = new Vertex(new Point3D(x, h4, z + 1), getFaceNormal(x, z + 1), mat);
                
                verts[6] = new Vertex(new Point3D(x, h4, z + 1), getFaceNormal(x, z + 1), mat);
                verts[7] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                verts[8] = new Vertex(new Point3D(x, h1, z), getFaceNormal(x, z), mat);
                
                verts[9] = new Vertex(new Point3D(x + 1, h2, z), getFaceNormal(x + 1, z), mat);
                verts[10] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                verts[11] = new Vertex(new Point3D(x + 1, h3, z + 1), getFaceNormal(x + 1, z + 1), mat);

                meshes[z * (width - 1) + x] = new SimpleMesh(verts, null, new Point3D(0, 0, 0), new Angle3D(0, 0, 0), new Vector3D(1, 1, 1));
            }
        }
        return meshes;
    }
    
    public void updateMeshes(double f) {
        aframe = f;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                heightmap[x][y] = getSimplexPoint(y, x);
            }
        }
        for (int x = 0; x < width - 1; x++) {
            for (int z = 0; z < height - 1; z++) {
                SimpleMesh curm = ((SimpleMesh)meshes[z * (width - 1) + x]);
                double h1 = heightmap[x][z];
                double h2 = heightmap[x + 1][z];
                double h3 = heightmap[x + 1][z + 1];
                double h4 = heightmap[x][z + 1];
                double h5 = getSimplexPoint(z + 0.5, x + 0.5);
//                Vector3D n1 = Point3D.normal(new Point3D(x, h1, y), new Point3D(x + 0.5, h5, y + 0.5), new Point3D(x + 1, h2, y));
//                Vector3D n2 = Point3D.normal(new Point3D(x + 1, h3, y + 1), new Point3D(x + 0.5, h5, y + 0.5), new Point3D(x, h4, y + 1));
//                Vector3D n3 = Point3D.normal(new Point3D(x, h4, y + 1), new Point3D(x + 0.5, h5, y + 0.5), new Point3D(x, h1, y));
//                Vector3D n4 = Point3D.normal(new Point3D(x + 1, h2, y), new Point3D(x + 0.5, h5, y + 0.5), new Point3D(x + 1, h3, y + 1));
                Vector3D n5 = getFaceNormal(x + 0.5, z + 0.5);
                
                Material mat = new Material(new Color(0.75f, 0.25f, 0.5f), Color.white, Color.black, 0.25f, null);
                
                Vertex[] verts;
                boolean flag = false;
                if (flag) {
                    verts = new Vertex[12];
                    verts[0] = new Vertex(new Point3D(x, h1, z), getFaceNormal(x, z), mat);
                    verts[1] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                    verts[2] = new Vertex(new Point3D(x + 1, h2, z), getFaceNormal(x + 1, z), mat);

                    verts[3] = new Vertex(new Point3D(x + 1, h3, z + 1), getFaceNormal(x + 1, z + 1), mat);
                    verts[4] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                    verts[5] = new Vertex(new Point3D(x, h4, z + 1), getFaceNormal(x, z + 1), mat);

                    verts[6] = new Vertex(new Point3D(x, h4, z + 1), getFaceNormal(x, z + 1), mat);
                    verts[7] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                    verts[8] = new Vertex(new Point3D(x, h1, z), getFaceNormal(x, z), mat);

                    verts[9] = new Vertex(new Point3D(x + 1, h2, z), getFaceNormal(x + 1, z), mat);
                    verts[10] = new Vertex(new Point3D(x + 0.5, h5, z + 0.5), n5, mat);
                    verts[11] = new Vertex(new Point3D(x + 1, h3, z + 1), getFaceNormal(x + 1, z + 1), mat);
                } else {
                    verts = new Vertex[6];
                    verts[0] = new Vertex(new Point3D(x, h1, z), getFaceNormal(x, z), mat);
                    verts[1] = new Vertex(new Point3D(x + 1, h3, z + 1), getFaceNormal(x + 1, z + 1), mat);
                    verts[2] = new Vertex(new Point3D(x + 1, h2, z), getFaceNormal(x + 1, z), mat);

                    verts[3] = new Vertex(new Point3D(x + 1, h3, z + 1), getFaceNormal(x + 1, z + 1), mat);
                    verts[4] = new Vertex(new Point3D(x, h1, z), getFaceNormal(x, z), mat);
                    verts[5] = new Vertex(new Point3D(x, h4, z + 1), getFaceNormal(x, z + 1), mat);
                }
                curm.setVerticies(verts);
            }
        }
    }
}
