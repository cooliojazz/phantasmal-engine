package com.up.pe.test;

import com.up.pe.mesh.SimpleMesh;
import com.up.pe.mesh.Mesh;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.up.pe.math.Angle3D;
import com.up.pe.render.DisplayManager;
import com.up.pe.render.light.PointLight;
import com.up.pe.math.Line;
import com.up.pe.render.Material;
import com.up.pe.input.MultiKeys;
import com.up.pe.input.MouseFreeTrackingCameraAdapter;
import com.up.pe.mesh.loaders.ObjLoader;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.mesh.MeshGenerator;
import com.up.pe.render.event.RenderInterceptorAdapter;
import com.up.pe.render.camera.FreeTrackingCamera;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import javax.swing.JPanel;
import javax.swing.JSlider;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ricky.t
 */
public class ProceduralMeshTest {
    
    static IntBuffer view = IntBuffer.allocate(4);
    static FloatBuffer model = FloatBuffer.allocate(16);
    static FloatBuffer proj = FloatBuffer.allocate(16);
    static int mouse = 0;
    static public JSlider ls = new JSlider(0, 5, 1);
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Frame f = new Frame("Phantasmal Engine");
        f.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

        });
        DisplayManager dm = new DisplayManager(new RenderInterceptorAdapter() {

				@Override
				public void preRender(DisplayManager dm, GL2 gl) {
					gl.glGetIntegerv(GL2.GL_VIEWPORT, view);
					gl.glGetFloatv(GL2.GL_MODELVIEW_MATRIX, model);
					gl.glGetFloatv(GL2.GL_PROJECTION_MATRIX, proj);
					dm.drawAxis(gl, 100);
				}

				@Override
				public void postRender(DisplayManager dm, GL2 gl) {
					gl.glDisable(GL2.GL_CULL_FACE);
				}

			});
        GLCanvas c = dm.getCanvas();
//        dm.addDefaultFlyControls(1, 1);
        FreeTrackingCamera camera = new FreeTrackingCamera(Point3D::zeros);
        dm.setCamera(camera);
        new MouseFreeTrackingCameraAdapter(camera).addToComponent(dm.getCanvas());
        JSlider hs = new JSlider(0, 50, 20);
        JSlider ws = new JSlider(0, 200, 100);
        JPanel main = new JPanel(new BorderLayout());
        f.add(main);
        main.add(c, BorderLayout.CENTER);
        JPanel sliders = new JPanel();
        sliders.setPreferredSize(new Dimension(10000, 20));
        main.add(sliders, BorderLayout.SOUTH);
        sliders.add(hs);
        sliders.add(ws);
        sliders.add(ls);
        f.setSize(800, 600);
        f.setVisible(true);
        
        ProceduralMesh pm = new ProceduralMesh(0);
        List<SimpleMesh>[][] cubes = new List[pm.width][pm.height];
        
        dm.clearMeshes();
        try {
//            ObjLoader o = new ObjLoader(new File("meshes/cube.obj"));
        
            c.addMouseListener(new MouseAdapter() {

//                @Override
//                public void mouseClicked(MouseEvent me) {
//                    Point2D clicked = getMouseIntersect(me.getX(), c.getHeight() - me.getY(), pm);
//                    if (clicked != null) {
//                        if (me.getButton() == MouseEvent.BUTTON1) {
//                            if (cubes[(int)clicked.getX()][(int)clicked.getY()] == null) {
//                                Point3D cpos = new Point3D(clicked.getX() + 0.5, pm.heightmap[(int)clicked.getX()][(int)clicked.getY()], clicked.getY() + 0.5);
//                                List<Mesh> cube = o.create(cpos, new Point3D(0, 0, 0), 1);
//                                for (Mesh m : cube) m.setMaterial(new Material(new float[] {(float)Math.random(), 0f, 0.5f}, Material.white, Material.black, 60, null));
//                                cubes[(int)clicked.getX()][(int)clicked.getY()] = cube;
//                                dm.meshes.addAll(cube);
//                            }
//                        } else {
//                            dm.meshes.removeAll(cubes[(int)clicked.getX()][(int)clicked.getY()]);
//                            cubes[(int)clicked.getX()][(int)clicked.getY()] = null;
//                        }
//                    }
//                }

                @Override
                public void mousePressed(MouseEvent me) {
                    mouse = me.getButton();
                }

                @Override
                public void mouseReleased(MouseEvent me) {
                    mouse = 0;
                }
                
                
            });
            
            c.addMouseMotionListener(new MouseAdapter() {

                @Override
                public void mouseDragged(MouseEvent me) {
                    if (mouse == 1) {
                        new Thread(() -> {
                            Point2D clicked = getMouseIntersect(me.getX(), c.getHeight() - me.getY(), pm);
                            if (clicked != null) {
                                if (mouse == MouseEvent.BUTTON1) {
                                    if (cubes[(int)clicked.getX()][(int)clicked.getY()] == null) {
                                        Point3D cpos = new Point3D(clicked.getX() + 0.5, pm.heightmap[(int)clicked.getX()][(int)clicked.getY()], clicked.getY() + 0.5);
//                                        List<SimpleMesh> cube = o.create(cpos, new Angle3D(0, 0, 0), new Vector3D(1, 1, 1));
                                        List<SimpleMesh> cube = Collections.singletonList(MeshGenerator.cube(new Material(new float[] {(float)Math.random(), 0f, 0.5f}, Material.white, Material.black, 60, null), cpos, Angle3D.zeros(), Vector3D.ones()));
//                                        for (Mesh m : cube) ((SimpleMesh)m).setMaterial(new Material(new float[] {(float)Math.random(), 0f, 0.5f}, Material.white, Material.black, 60, null));
                                        cubes[(int)clicked.getX()][(int)clicked.getY()] = new ArrayList<>(cube);
                                        dm.addMeshes(cube);
                                    }
                                } else {
//                                    dm.meshes.removeAll(cubes[(int)clicked.getX()][(int)clicked.getY()]);
                                    cubes[(int)clicked.getX()][(int)clicked.getY()] = null;
                                }
                            }
                        }).start();
                    }
                }

            });
            new Thread(() -> {
                while (true) {
                    for (int x = 0; x < pm.width; x++){
                        for (int z = 0; z < pm.height; z++) {
                            if (cubes[x][z] != null) {
                                Point3D cpos = new Point3D(x + 0.5, interpol2d(pm.heightmap[(int)x][(int)z], pm.heightmap[(int)x][(int)z + 1], pm.heightmap[(int)x + 1][(int)z], pm.heightmap[(int)x + 1][(int)z + 1], x - Math.floor(x), z - Math.floor(z)) + 0.5, z + 0.5);
                                for (Mesh m : cubes[x][z]) ((SimpleMesh)m).setPosition(cpos);
                            }
                        }
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        dm.addLight(new PointLight(new Point3D(10, 10, 10), Material.white, 50));

        new Thread(() -> {
            while (true) {
                c.repaint();
            }
        }).start();
        

        dm.addMeshes(Arrays.asList(pm.createMeshes()));
        
        double z = 0;
        while (true) {
            z += 0.001;
            pm.hfac = hs.getValue() / 10d;
            pm.wfac = ws.getValue() / 10d;
            pm.updateMeshes(z);
            try {
                Thread.sleep(10);
            } catch (Exception ex) {}
        }
    }
    
    private static Point2D getMouseIntersect(int x, int y, ProceduralMesh pm) {
        GLU glu = new GLU();
        FloatBuffer pos1 = FloatBuffer.allocate(3);
        FloatBuffer pos2 = FloatBuffer.allocate(3);
        glu.gluUnProject(x, y, 1f, model, proj, view, pos1);
        glu.gluUnProject(x, y, 0f, model, proj, view, pos2);
        Line l = new Line(new Point3D(pos1.get(0), pos1.get(1), pos1.get(2)), new Point3D(pos2.get(0), pos2.get(1), pos2.get(2)));
        Point2D ret = null;
        for (int i = 0; i < pm.meshes.length; i++) {
            Mesh m = pm.meshes[i];
            if (((SimpleMesh)m).intersects(l)) {
                ret = new Point2D(i % (pm.width - 1), i / (pm.width - 1));
            }
        }
        return ret;
    }
    
    private static double interpol2d(double a, double b, double c, double d, double x, double y) {
        return interpol(interpol(a, c, x), interpol(b, d, x), y);
    }
    
    private static double interpol(double a, double b, double x) {
	double f = (1 - Math.cos(x * Math.PI)) * .5;
	return a * (1 - f) + b * f;
    }
}
