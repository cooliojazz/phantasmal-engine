package com.up.pe.test;

/**
 *
 * @author rjtalbot
 */
public class SimplexNoise {
    
    int oct;
    double p;

    public SimplexNoise(int oct, double p) {
        this.oct = oct;
        this.p = p;
    }
    
    public double simplexNoise(double x) {
        double total = 0;
        for (int i = 0; i < oct; i++) {
            double freq = Math.pow(2, i);
            total += interNoise(x * freq) * Math.pow(p, i);
        }
        return total;
    }
    
    public double simplexNoise(double x, double y) {
        double total = 0;
        for (int i = 0; i < oct; i++) {
            double freq = Math.pow(2, i);
            total += interNoise(x * freq, y * freq) * Math.pow(p, i);
        }
        return total;
    }
    
    public double simplexNoise(double x, double y, double z) {
        double total = 0;
        for (int i = 0; i < oct; i++) {
            double freq = Math.pow(2, i);
            total += interNoise(x * freq, y * freq, z * freq) * Math.pow(p, i);
        }
        return total;
    }
    
    private double interNoise(double x) {
        int ix = (int)Math.floor(x);
        return interpolate(noise(ix), noise(ix + 1), x - ix);
    }
    
    private double interNoise(double x, double y) {
        int ix = (int)Math.floor(x);
        int iy = (int)Math.floor(y);
        return interpolate(interpolate(noise(ix, iy), noise(ix + 1, iy), x - ix), interpolate(noise(ix, iy + 1), noise(ix + 1, iy + 1), x - ix), y - iy);
    }
    
    private double interNoise(double x, double y, double z) {
        int ix = (int)Math.floor(x);
        int iy = (int)Math.floor(y);
        int iz = (int)Math.floor(z);
        return interpolate(
                interpolate(
                    interpolate(noise(ix, iy, iz), noise(ix + 1, iy, iz), x - ix),
                    interpolate(noise(ix, iy + 1, iz), noise(ix + 1, iy + 1, iz), x - ix),
                    y - iy
                ),
                interpolate(
                    interpolate(noise(ix, iy, iz + 1), noise(ix + 1, iy, iz + 1), x - ix),
                    interpolate(noise(ix, iy + 1, iz + 1), noise(ix + 1, iy + 1, iz + 1), x - ix),
                    y - iy
                ),
                z - iz
            );
    }
    
    private double interpolate(double a, double b, double x) {
	double f = (1 - Math.cos(x * Math.PI)) / 2;
	return a * (1 - f) + b * f;
    }
    
    private double noise(int x) {
        x = (x << 13) ^ x;
        return 1.0 - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0;
    }
    
    //TODO: Not sure if this is correct 2d simplex
    private double noise(int x, int y) {
        int n = x + y * 57;
        n = (n << 13) ^ n;
        return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);    
    }
    
    private double noise(int x, int y, int z) {
        int n = x + y * 57 + z * 113;
        n = (n << 13) ^ n;
        return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);    
    }
}
