package com.up.pe.test;

import com.up.pe.mesh.MeshGenerator;
import com.up.pe.mesh.Mesh;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.up.pe.input.MouseFreeTrackingCameraAdapter;
import com.up.pe.math.Angle3D;
import com.up.pe.math.Matrix4D;
import com.up.pe.render.DisplayManager;
import com.up.pe.render.light.PointLight;
import com.up.pe.render.Material;
import com.up.pe.math.Point2D;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import com.up.pe.render.event.RenderInterceptorAdapter;
import com.up.pe.render.camera.FreeTrackingCamera;
import com.up.pe.render.light.DirectionLight;
import com.up.pe.util.GLCapabilitiesFactory;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 *
 * @author ricky.t
 */
public class Test {

    static boolean mousetrapped = false;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Frame f = new Frame("Phantasmal Engine");
        f.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
            
        });
        DisplayManager dm = new DisplayManager(new RenderInterceptorAdapter() {

				@Override
				public void preRender(DisplayManager dm, GL2 gl) {
					dm.drawAxis(gl, 100);
				}

				@Override
				public void postRender(DisplayManager dm, GL2 gl) {
					dm.renderString(gl, dm.getCamera().getPosition().getX() + ", " + dm.getCamera().getPosition().getY() + ", " + dm.getCamera().getPosition().getZ(), Color.red, new Point2D(20, 20), 15);
//					dm.renderString(gl, dm.getCamera().getAngle().getX() + ", " + dm.getCamera().getAngle().getY() + ", " + dm.getCamera().getAngle().getZ(), Color.red, new Point2D(20, 40), 15);
					dm.renderString(gl, Math.round(dm.getFPS()) + "fps", Color.red, new Point2D(20, 60), 15);
				}
				
			});
//		dm.useVR();
		dm.setRenderDistance(1000);
		FreeTrackingCamera cam = new FreeTrackingCamera(Matrix4D.identity());
		dm.setCamera(cam);
//		dm.setCamera(new FirstPersonCamera(() -> new Point3D(0, 0, -6), () -> new Angle3D(30, 0, 0)));
//        GLCanvas c = dm.getCanvas(new GLCapabilitiesFactory().withDefaultMultisampleCapabilities(4).usingFBO().get());
        GLCanvas c = dm.getCanvas(new GLCapabilitiesFactory().get());
        f.add(c);
        f.setSize(800, 600);
        f.setVisible(true);
        new MouseFreeTrackingCameraAdapter(cam).addToComponent(dm.getCanvas());
        
//        ObjLoader o;
//        try {
//            o = new ObjLoader(new File("meshes/BMW850/BMW850.obj"));
//            dm.addMeshes(o.create(new Point3D(-10, 0, 0), new Angle3D(0, 0, 0), new Vector3D(1, 1, 1)));
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        dm.addLight(new DirectionLight(new Vector3D(0, -1, 5).normalize(), new float[] {1, 0.5f, 0.3f, 1}));
        dm.addLight(new PointLight(new Point3D(0, 10, 0), Material.white, 25));
//        dm.addLight(new PointLight(new Point3D(-500, 1000, 0), new float[] {0.7f, 0.7f, 0.5f, 1}, 100000));
//        dm.addLight(new PointLight(new Point3D(50, 10, 0), Material.white, 100));
//        dm.addLight(new PointLight(new Point3D(100, -10, 0), Material.red, 100));
        ArrayList<Mesh> meshes = new ArrayList<>();
		meshes.add(MeshGenerator.sphere(5, new Material(new float[] {1, 0.5f, 0.3f, 1}, Material.white, new float[] {1, 0.5f, 0.3f, 1}, 0, null), new Vector3D(0, 1, -5).normalize().mul(500), new Angle3D(0, 0, 0), new Vector3D(10)));
		meshes.add(MeshGenerator.sphere(5, new Material(Material.white, Material.white, Material.white, 0, null), new Point3D(0, 10, 0), new Angle3D(0, 0, 0), new Vector3D(0.1)));
        for (int i = 3; i < 100; i++) {
            Color mc = new Color((int)(Math.random() * Integer.MAX_VALUE));
//            Color mc = Color.white;
//            meshes.add(MeshGenerator.cylinder(20, 2, new Material(mc, Color.WHITE, Color.BLACK, 0.25f, null), new Point3D((i - 3) * 2, 0, -5), new Angle3D(0, 0, 0), new Vector3D(0.5, 1, 0.9)));
//            meshes.add(MeshGenerator.cylinder(20, 2, new Material(mc, Color.WHITE, Color.BLACK, 0.75f, null), new Point3D((i - 3) * 2, 0, 5), new Angle3D(0, 0, 0), new Vector3D(0.9, 1, 0.5)));
            meshes.add(MeshGenerator.sphere(5, new Material(mc, Color.WHITE, Color.BLACK, 0.2f, null), new Point3D(-5, 0, -(i - 3) * 2), new Angle3D(0, 0, 0), new Vector3D(0.5, 1, 0.9)));
            meshes.add(MeshGenerator.sphere(5, new Material(mc, Color.WHITE, Color.BLACK, 0.8f, null), new Point3D(5, 0, -(i - 3) * 2), new Angle3D(0, 0, 0), new Vector3D(0.9, 1, 0.5)));
			if (i % 2 == 0) meshes.add(MeshGenerator.cube(new Material(Color.LIGHT_GRAY, Color.WHITE, Color.BLACK, 0.5f, null), new Point3D(-1.5, -3, -(i - 3) * 2 - 0.5), new Angle3D(0, 0, 0), new Vector3D(3, 3, 3)));
        }
        dm.addMeshes(meshes);
        
        new Thread(() -> {
			int dir = 1;
            while (true) {
                c.repaint();
//                for (Mesh m : meshes)
////                    ((SimpleMesh)m).setPosition(((SimpleMesh)m).getPosition().sum(new Point3D(Math.random(), Math.random(), Math.random()).sum(-.5).mul(0.1)));
//                    ((SimpleMesh)m).setRotation(((SimpleMesh)m).getRotation().addY(0.1));
				if (dm.getVRScale() > 10 || dm.getVRScale() < 0.01) dir = -dir;
				dm.setVRScale(dm.getVRScale() * Math.pow(1.001, dir));
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) { }
            }
        }).start();
        
        System.out.println("Rendering...");
        
//        new Thread(() -> {
//            while (true) {
//                c.repaint();
//            }
//        }).start();
//        
//        System.out.println("Rendering...");
////        dm.meshes.clear();
//        double inc = 0.03;
//        double scale = 10;
//        ArrayList<Mesh> ms = new ArrayList<>();
//        double s = 1;
//        while (s > 0.0001) {
//            for (double x = -2.5; x < 1.5; x += inc) {
//                for (double y = -1; y < 1; y += inc) {
//                    for (double z = -1; z < 1; z += inc) {
//                        if (inSet(x, y, z, s)) {
//                            if (!inSet(x + inc, y, z, s) || !inSet(x - inc, y, z, s) || !inSet(x, y + inc, z, s) || !inSet(x, y - inc, z, s) || !inSet(x, y, z + inc, s) || !inSet(x, y, z - inc, s)) {
//    //                        if (inSet(x + inc, y, z) || inSet(x - inc, y, z) || inSet(x, y + inc, z) || inSet(x, y - inc, z) || inSet(x, y, z + inc) || inSet(x, y, z - inc)) {
//                                List<Mesh> cube = o.create(
//                                        new Point3D(x * scale, y * scale, z * scale),
//                                        new Point3D(0, 0, 0), inc * scale,
//                                        new Material(new float[] {0.25f * (float)Math.abs(x), 0.5f * (float)Math.abs(y), 0.5f * (float)Math.abs(z)}, Material.white, Material.black, 64, null)
//                                );
//                                ms.addAll(cube);
////                                dm.addMeshes(cube);
////                                cubes++;
//                            }
//                        }
//                    }
//                }
////                System.out.println((x + 3) / 6 * 100 + "%");
//            }
//            dm.clearMeshes();
//            dm.addMeshes(ms);
//            ms.clear();
//            s /= 1.2;
//        }
//        System.out.println("Finished mandelbulb render\n");
//        System.out.println("Finished Initializing");
    }
    
    final static private double HALF_PI = 0.5 * Math.PI;
    public static boolean inSet(double x, double y, double z, double scale) {
        x = x * scale - 0.78;
        y = y * scale + 0.07;
        z = z * scale + 0.12;
        double tx = 0.0;
        double ty = 0.0;
        double tz = 0.0;
        int i = 0;
        int max = 20;
        while (tx * tx + ty * ty + tz * tz < 4.0 && i < max) {
            double r = tx * tx + ty * ty + tz * tz;
            double yang = Math.atan2(Math.sqrt(tx * tx + ty * ty), tz) * 2 + HALF_PI;
            double zang = Math.atan2(ty, tx) * 2 + Math.PI;
            double xtemp = r * Math.sin(yang) * Math.cos(zang) + x;
            double ytemp = r * Math.sin(yang) * Math.sin(zang) + y;
            double ztemp = r * Math.cos(yang) + z;
            tx = xtemp;
            ty = ytemp;
            tz = ztemp;
            i++;
        }
        return i == max;
    }
    
    
    public static void cleanmain(String[] args) {
        Frame f = new Frame("Phantasmal Engine");
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        DisplayManager dm = new DisplayManager(new RenderInterceptorAdapter() {

				@Override
				public void preRender(DisplayManager dm, GL2 gl) {
					dm.drawAxis(gl, 100);
				}

				@Override
				public void postRender(DisplayManager dm, GL2 gl) {
//	                renderString(gl, getCamera().getX() + ", " + getCamera().getY() + ", " + getCamera().getZ(), Color.red, new Point2D(20, 20));
//	                renderString(gl, getCameraAngle().getX() + ", " + getCameraAngle().getY() + ", " + getCameraAngle().getZ(), Color.red, new Point2D(20, 40));
//	                renderString(gl, Math.round(getFPS()) + "fps", Color.red, new Point2D(20, 60));
				}
			});
        GLCapabilities cap = new GLCapabilities(GLProfile.getDefault());
        cap.setSampleBuffers(true);
        GLCanvas c = dm.getCanvas(cap);
        f.add(c);
        f.setSize(800, 600);
        f.setVisible(true);
		FreeTrackingCamera cam = new FreeTrackingCamera(Matrix4D.identity());
		dm.setCamera(cam);
        new MouseFreeTrackingCameraAdapter(cam).addToComponent(dm.getCanvas());
        
        dm.addLight(new PointLight(new Point3D(25, 10, 0), Material.white, 50));
        dm.addLight(new PointLight(new Point3D(75, 10, 0), Material.red, 50));
        ArrayList<Mesh> meshes = new ArrayList<>();
        for (int i = 3; i < 100; i++) {
            Color mc = new Color((int)(Math.random() * Integer.MAX_VALUE));
            meshes.add(MeshGenerator.sphere(3, new Material(mc, Color.WHITE, Color.BLACK, 0.25f, null), new Point3D((i - 3) * 2, 0, -5), new Angle3D(0, 0, 0), new Vector3D(0.5, 1, 0.9)));
            meshes.add(MeshGenerator.sphere(3, new Material(mc, Color.WHITE, Color.BLACK, 0.75f, null), new Point3D((i - 3) * 2, 0, 5), new Angle3D(0, 0, 0), new Vector3D(0.9, 1, 0.5)));
        }
        dm.addMeshes(meshes);
        
        new Thread(() -> {
            while (true) {
                c.repaint();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) { }
            }
        }).start();
        
        System.out.println("Rendering...");
    }
}

