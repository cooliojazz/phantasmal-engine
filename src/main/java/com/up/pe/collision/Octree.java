package com.up.pe.collision;

import com.jogamp.opengl.GL2;
import com.up.pe.math.Point3D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author rush
 */
public class Octree {
    
    private AABoundingBox bounds;
    private OctreeNode root;

    public Octree(AABoundingBox bounds) {
        this.bounds = bounds;
    }
    
    public void build(List<? extends Collidable> boxes) {
        OctreeNode nroot = new OctreeNode(null, bounds, new ArrayList<>(boxes));
        nroot.subdivide();
        root = nroot;
    }
    
    public void render(GL2 gl) {
        gl.glColor3ui((hashCode() & 0xFF) << 16, ((hashCode() >> 8) & 0xFF) << 16, ((hashCode() >> 16) & 0xFF) << 16);
        gl.glColor3d(0.5, 0.7, 0.9);
        if (root != null) root.render(gl);
    }

    public OctreeNode getRoot() {
        return root;
    }
    
    public OctreeNode findContainer(Point3D p) {
        return root.getSmallestContaining(p);
    }
    
    public HashMap<Collidable, ArrayList<Collidable>> getAllCollisions() {
        return allCollisions(root, new ArrayList<>());
    }
    
    private static HashMap<Collidable, ArrayList<Collidable>> allCollisions(Octree.OctreeNode n, ArrayList<Collidable> cols) {
        HashMap<Collidable, ArrayList<Collidable>> collisions = new HashMap<>();
        for (Collidable c1 : n.getContents()) {
            ArrayList<Collidable> c1cols = collisions.get(c1);
            if (c1cols == null) {
                c1cols = new ArrayList<>();
                collisions.put(c1, c1cols);
            }
            for (Collidable c2 : n.getContents()) {
                if (c1 != c2) {
//                    if (!c1cols.contains(c2) && c1.getMesh().collides(c2.getMesh())) {
                    if (!c1cols.contains(c2) && c1.getBounds().intersects(c2.getBounds())) {
                        c1cols.add(c2);
                    }
                }
            }
            for (Collidable c2 : cols) {
//                if (!c1cols.contains(c2) && c1.getMesh().collides(c2.getMesh())) {
                if (!c1cols.contains(c2) && c1.getBounds().intersects(c2.getBounds())) {
                    c1cols.add(c2);
                }
            }
        }
        cols.addAll(n.getContents());
        for (Octree.OctreeNode sn : n.getChildren()) {
            if (sn != null) {
                HashMap<Collidable, ArrayList<Collidable>> subcols = allCollisions(sn, cols);
                subcols.entrySet().forEach(e -> {
                    if (collisions.containsKey(e.getKey())) {
                        collisions.get(e.getKey()).addAll(e.getValue());
                    } else {
                        collisions.put(e.getKey(), e.getValue());
                    }
                });
            }
        }
        cols.removeAll(n.getContents());
        return collisions;
    }
    
//    public ArrayList<Pair<Collidable, Collidable>> getAllCollisions() {
//        return allCollisions(root, new ArrayList<>());
//    }
//    
//    private static ArrayList<Pair<Collidable, Collidable>> allCollisions(Octree.OctreeNode n, ArrayList<Collidable> cols) {
//        ArrayList<Pair<Collidable, Collidable>> collisions = new ArrayList<>();
//        for (Collidable c1 : n.getContents()) {
//            for (Collidable c2 : n.getContents()) {
//                if (c1 != c2) {
//                    Pair<Collidable, Collidable> p = new Pair<>(c1, c2);
//                    boolean test1 = c1.getMesh().collides(c2.getMesh());
//                    if (!collisions.contains(p) && test1) {
//                        collisions.add(p);
//                    }
//                }
//            }
//            for (Collidable c2 : cols) {
//                Pair<Collidable, Collidable> p = new Pair<>(c1, c2);
//                boolean test1 = c1.getMesh().collides(c2.getMesh());
//                if (!collisions.contains(p) && test1) {
//                    collisions.add(p);
//                }
//            }
//        }
//        cols.addAll(n.getContents());
//        for (Octree.OctreeNode sn : n.getChildren()) {
//            if (sn != null) collisions.addAll(allCollisions(sn, cols));
//        }
//        cols.removeAll(n.getContents());
//        return collisions;
//    }
    
    public static class OctreeNode {
        
        private AABoundingBox bounds;
        private ArrayList<? extends Collidable> objs;
        private OctreeNode parent;
        private OctreeNode[] children = new OctreeNode[8];

        public OctreeNode(OctreeNode parent, AABoundingBox bounds, List<? extends Collidable> boxes) {
            this.parent = parent;
            this.bounds = bounds;
            this.objs = new ArrayList<Collidable>(boxes);
        }
        
        private static interface BBDivider extends Function<AABoundingBox, AABoundingBox> {}
        private static BBDivider[] dividers = new BBDivider[] {
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent()), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulX(-1)), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulZ(-1)), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulX(-1).mulZ(-1)), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulY(-1)), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulY(-1).mulX(-1)), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulY(-1).mulZ(-1)), bb.getCenter()),
            bb -> new AABoundingBox(bb.getCenter().sum(bb.getExtent().mulY(-1).mulX(-1).mulZ(-1)), bb.getCenter()),
        };
        
        public void subdivide() {
            if (objs.size() > 1 && dividers[0].apply(bounds).getExtent().getX() > 1) {
                for (int i = 0; i < 8; i++) {
                    AABoundingBox subbounds = dividers[i].apply(bounds);
                    List<Collidable> subboxes = objs.stream().filter(o -> 
							subbounds.contains(o.getBounds().getCenter()) && !subbounds.intersectsEdges(o.getBounds())
						).collect(Collectors.toList());
                    objs.removeAll(subboxes);
                    children[i] = new OctreeNode(this, subbounds, subboxes);
                    children[i].subdivide();
                }
            }
        }
    
        public boolean isLeaf() {
            return children[0] == null;
        }
        
        /**
         * Draw the bounding box e.g. for debugging
         */
        public void render(GL2 gl) {
            if (!isLeaf()) for (OctreeNode n : children) n.render(gl);
            gl.glColor3ui((hashCode() & 0xFF) << 24, ((hashCode() >> 8) & 0xFF) << 24, ((hashCode() >> 16) & 0xFF) << 24);
            bounds.render(gl);
            for (Collidable box : objs) box.getBounds().render(gl);
        }

        public List<? extends Collidable> getContents() {
            return objs;
        }
        
        public OctreeNode[] getChildren() {
            return children;
        }

        public AABoundingBox getBounds() {
            return bounds;
        }

        @Override
        public String toString() {
            return (isLeaf() ? "Leaf" : "Branch") + " at " + bounds + " containing " + objs.size() + " objects.";
        }
        
        public OctreeNode getSmallestContaining(Point3D p) {
            if (getBounds().contains(p)) {
                for (OctreeNode n : children) {
                    if (n != null && n.getSmallestContaining(p) != null) return getSmallestContaining(p);
                }
                return this;
            }
            return null;
        }
        
//        public void reevaluate(Collidable c) {
//            if (objs.contains(c)) {
//                if (getBounds().contains(c.getBounds())) {
//                    _;
//                }
//            } else {
//                parent.reevaluate(c);
//            }
//        }
        
    }
}
