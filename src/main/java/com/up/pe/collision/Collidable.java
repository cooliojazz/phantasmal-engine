package com.up.pe.collision;

import com.up.pe.mesh.SimpleMesh;

/**
 *
 * @author rush
 */
public interface Collidable {
    
    public AABoundingBox getBounds();
    
//    public SimpleMesh getMesh();
    
}
