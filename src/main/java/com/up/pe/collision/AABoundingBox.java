package com.up.pe.collision;

import com.jogamp.opengl.GL2;
import com.up.pe.math.Point3D;
import com.up.pe.math.Vector3D;
import java.util.Collection;
import java.util.Comparator;

/**
 *
 * @author rush
 */
public class AABoundingBox {
    
    private Point3D min;
    private Point3D max;
    private Point3D center;
    private Vector3D extent;

    public AABoundingBox(Point3D min, Point3D max) {
        this(min.midPoint(max), min.midPoint(max).vectorTo(max));
    }
    
    public AABoundingBox(Point3D center, Vector3D extent) {
        extent = extent.abs();
        this.center = center;
        this.extent = extent;
        min = center.sum(extent.mul(-1));
        max = center.sum(extent);
    }

    public Point3D getCenter() {
        return center;
    }

    public Vector3D getExtent() {
        return extent;
    }

    public Point3D getMin() {
        return min;
    }

    public Point3D getMax() {
        return max;
    }
    
    public Point3D[] getCorners() {
        return new Point3D[] {
            center.sum(extent),
            center.sum(extent.mulX(-1)),
            center.sum(extent.mulY(-1)),
            center.sum(extent.mulZ(-1)),
            center.sum(extent.mulX(-1).mulY(-1)),
            center.sum(extent.mulX(-1).mulZ(-1)),
            center.sum(extent.mulY(-1).mulZ(-1)),
            center.sum(extent.mul(-1))
        };
    }
    
//    public void setCenter(Point3D center) {
//        this.center = center;
//    }
//
//    public void setExtent(Vector3D extent) {
//        this.extent = extent;
//    }
    
    public boolean contains(Point3D p) {
        return min.getX() < p.getX() && max.getX() > p.getX() &&
                min.getY() < p.getY() && max.getY() > p.getY() &&
                min.getZ() < p.getZ() && max.getZ() > p.getZ();
    }
    
    public boolean contains(AABoundingBox bb) {
        return min.getX() <= bb.min.getX() && max.getX() >= bb.max.getX() &&
                min.getY() <= bb.min.getY() && max.getY() >= bb.max.getY() &&
                min.getZ() <= bb.min.getZ() && max.getZ() >= bb.max.getZ();
    }
    
    public boolean intersects(AABoundingBox bb) {
        return min.getX() <= bb.max.getX() && max.getX() >= bb.min.getX() &&
                min.getY() <= bb.max.getY() && max.getY() >= bb.min.getY() &&
                min.getZ() <= bb.max.getZ() && max.getZ() >= bb.min.getZ();
    }
    
    public AABoundingBox intersectionBox(AABoundingBox bb) {
        return new AABoundingBox(getMin().max(bb.getMin()), getMax().min(bb.getMax()));
    }
    
    public boolean intersectsEdges(AABoundingBox bb) {
        return intersects(bb) && !contains(bb) && !bb.contains(this);
    }
    
    /**
     * Draw the bounding box e.g. for debugging
     */
    public void render(GL2 gl) {
//        Point3D emin = center.sum(extent.mul(-extent.magnitude() / 900 - .5));
//        Point3D emax = center.sum(extent.mul(extent.magnitude() / 900 + .5));
        Point3D emin = center.sum(extent.mul(-1.01));
        Point3D emax = center.sum(extent.mul(1.01));
        gl.glLineWidth(2f);
        
//        gl.glBegin(GL2.GL_LINES);
//        gl.glVertex3d(center.getX(), center.getY(), center.getZ());
//        gl.glVertex3d(max.getX(), max.getY(), max.getZ());
//        gl.glEnd();

        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emin.getY(), emin.getZ());
        gl.glVertex3d(emax.getX(), emin.getY(), emin.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emin.getY(), emax.getZ());
        gl.glVertex3d(emax.getX(), emin.getY(), emax.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emax.getY(), emin.getZ());
        gl.glVertex3d(emax.getX(), emax.getY(), emin.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emax.getY(), emax.getZ());
        gl.glVertex3d(emax.getX(), emax.getY(), emax.getZ());
        gl.glEnd();
        
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emin.getY(), emin.getZ());
        gl.glVertex3d(emin.getX(), emax.getY(), emin.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emax.getX(), emin.getY(), emin.getZ());
        gl.glVertex3d(emax.getX(), emax.getY(), emin.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emin.getY(), emax.getZ());
        gl.glVertex3d(emin.getX(), emax.getY(), emax.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emax.getX(), emin.getY(), emax.getZ());
        gl.glVertex3d(emax.getX(), emax.getY(), emax.getZ());
        gl.glEnd();
        
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emin.getY(), emin.getZ());
        gl.glVertex3d(emin.getX(), emin.getY(), emax.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emax.getX(), emin.getY(), emin.getZ());
        gl.glVertex3d(emax.getX(), emin.getY(), emax.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emin.getX(), emax.getY(), emin.getZ());
        gl.glVertex3d(emin.getX(), emax.getY(), emax.getZ());
        gl.glEnd();
        gl.glBegin(GL2.GL_LINES);
        gl.glVertex3d(emax.getX(), emax.getY(), emin.getZ());
        gl.glVertex3d(emax.getX(), emax.getY(), emax.getZ());
        gl.glEnd();
    }

    @Override
    public String toString() {
        return "(·" + getCenter() + ", /" + getExtent() + ")";
    }
    
    public static AABoundingBox encompassing(Collection<AABoundingBox> boxes) {
        Point3D min = new Point3D(boxes.stream().map(b -> b.getMin().getX()).min(Comparator.naturalOrder()).get(), boxes.stream().map(b -> b.getMin().getY()).min(Comparator.naturalOrder()).get(), boxes.stream().map(b -> b.getMin().getZ()).min(Comparator.naturalOrder()).get());
        Point3D max = new Point3D(boxes.stream().map(b -> b.getMax().getX()).max(Comparator.naturalOrder()).get(), boxes.stream().map(b -> b.getMax().getY()).max(Comparator.naturalOrder()).get(), boxes.stream().map(b -> b.getMax().getZ()).max(Comparator.naturalOrder()).get());
        return new AABoundingBox(min, max);
    }
}
